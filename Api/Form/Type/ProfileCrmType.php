<?php
namespace La\UserBundle\Api\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use La\UserBundle\Traits\Form\Crm;


class ProfileCrmType extends AbstractType
{
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Crm\FirstName::add($builder, array("constraints" => array()));
        Crm\LastName::add($builder, array("constraints" => array()));
    }

    public function getName()
    {
        return 'la_user_api_crm_profile';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_protection'   => false,
            'validation_groups' => array('LaEdit'),
        ));
    }

}