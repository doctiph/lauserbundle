<?php

namespace La\UserBundle\Api\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use La\UserBundle\Traits\Form\User;

class ProfileFormType extends AbstractType
{
    public function __construct($class, $crmType)
    {
        $this->class = $class;
        $this->crmType = $crmType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('crm', $this->crmType, array());
    }

    public function getName()
    {
        return 'la_user_api_profile';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_protection'   => false,
            'cascade_validation' => true, // cascade validation for Crm form
            'validation_groups' => array('LaEdit'),
        ));
    }

}