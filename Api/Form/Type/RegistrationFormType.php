<?php

namespace La\UserBundle\Api\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use La\UserBundle\Traits\Form\User;

class RegistrationFormType extends AbstractType
{
    public function __construct($class, $crmType)
    {
        $this->class = $class;
        $this->crmType = $crmType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        User\Username::add($builder);
        User\Email::add($builder);
        User\Password::add($builder);

        $builder->add('crmTmp', $this->crmType, array());
    }

    public function getName()
    {
        return 'la_user_api_registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_protection' => false,
            'cascade_validation' => true, // cascade validation for Crm form
            'validation_groups' => array('LaRegistration'),
        ));
    }

}