<?php

namespace La\UserBundle\Api\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Helper\DialogHelper;
use Symfony\Component\Console\Helper\ProgressHelper;
use Symfony\Component\Console\Output\OutputInterface;
use La\UserBundle\Entity\Token;

class ApiDatabaseManager
{
    protected $em;
    protected $userEntity;

    public function __construct(EntityManager $em, $userEntity)
    {
        $this->em = $em;
        $this->userEntity = $userEntity;
    }

    public function cleanObsolescentTokens(OutputInterface $output, ProgressHelper $progress, DialogHelper $dialog, $age)
    {
        $tokenRepository = $this->em->getRepository('La\UserBundle\Entity\Token');
        $userRepository = $this->em->getRepository($this->userEntity);

        $expiration = $tokenRepository::TOKEN_TTL - $age;
        $expirationDate = new \DateTime();
        $expirationDate->modify(sprintf('+%s days', $expiration));

        $tokens = $tokenRepository->getExpired($expirationDate);
        if ($output->getVerbosity() > 1) {
            $output->writeln(sprintf(PHP_EOL . '<debug>%d tokens may be eligible for deletion. Checking their users.</debug>', count($tokens)));
            $progress->start($output, count($tokens));
        }

        /** @var Token $token */
        foreach ($tokens as $i => $token) {

            $user = $userRepository->findBy(array(
                'id' => $token->getId(),
                'enabled' => 1
            ));

            // Active user. Skip.
            if (!empty($user)) {
                unset($tokens[$i]);
            }

            if ($output->getVerbosity() > 1) {
                $progress->advance();
            }
        }

        if ($output->getVerbosity() > 1) {
            $progress->finish();
            if (count($tokens) > 0) {
                if (!$dialog->askConfirmation(
                    $output,
                    sprintf(PHP_EOL . '<debug>%d tokens are eligible for deletion (Disabled users). Confirm deletion ? (y/n)</debug>' . PHP_EOL, count($tokens)),
                    false
                )
                ) {
                    return;
                }
            }else{
                $output->writeln(PHP_EOL.'<debug>No token eligible for deletion. Exiting.</debug>'.PHP_EOL);
                exit;
            }
        }

        foreach ($tokens as $token) {
            try {
                $tokenRepository->remove($token->getId());
                if ($output->getVerbosity() > 1) {
                    $token->getExpire()->modify(sprintf('-%s days', $tokenRepository::TOKEN_TTL));
                    $output->writeln(sprintf('Deleted : Token for user %s, created on %s.', $token->getId(), $token->getExpire()->format('d/m/Y H:i:s')));
                }
            } catch (\Exception $e) {
                $output->writeln(sprintf('An error occured while deleting token for user %s (%s). Exiting.', $token->getId(), $token->getToken()));
                exit;
            }
        }

        return;
    }

}