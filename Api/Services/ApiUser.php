<?php

namespace La\UserBundle\Api\Services;

use Symfony\Component\EventDispatcher\GenericEvent;

class ApiUser
{

    public function formatEventUser(GenericEvent $event)
    {
        $user = $event->getSubject();

        // user data
        $res = array(
            'id' => $user->getId(),
            'username' => $user->getUserName(),
            'email' => $user->getEmail()
        );

        // crm data
        $crm = $user->getCrm();

        $res['crm'] = array();

        $map = $crm->properties();
        $excluded = array('email', 'user',  '__isInitialized__');
        foreach ($map as $key => $property) {
            if (in_array($key, $excluded)) {
                continue;
            }
            $res['crm'][$key] = $property;
        }

        return $res;
    }

}