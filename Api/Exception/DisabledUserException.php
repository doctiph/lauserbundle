<?php
namespace La\UserBundle\Api\Exception;

use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception\ApiExceptionInterface;


/**
 * Class InvalidUserException
 * @package La\UserBundle\Api\Exception
 */
class DisabledUserException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_LOCKED;
    }

    public function getApiMessage()
    {
        return 'user_account_disabled';
    }

}