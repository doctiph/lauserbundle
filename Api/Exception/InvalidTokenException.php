<?php
namespace La\UserBundle\Api\Exception;

use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception\ApiExceptionInterface;

/**
 * Class InvalidTokenException
 * @package La\UserBundle\Api\Exception
 */
class InvalidTokenException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_BAD_REQUEST;
    }

    public function getApiMessage()
    {
        return 'invalid_token';
    }

}