<?php
namespace La\UserBundle\Api\Exception;

use FOS\RestBundle\Util\Codes;
use La\ApiBundle\Exception\ApiExceptionInterface;


/**
 * Class InvalidCredentialsException
 * @package La\UserBundle\Api\Exception
 */
class InvalidCredentialsException extends \Exception implements ApiExceptionInterface
{

    public function getHttpErrorCode()
    {
        return Codes::HTTP_BAD_REQUEST;
    }

    public function getApiMessage()
    {
        return 'invalid_credentials';
    }

}