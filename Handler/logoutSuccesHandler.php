<?php

namespace La\UserBundle\Handler;

use La\UserBundle\Lib\LaUserUtil;
use La\UserBundle\Lib\RedirectUtil;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

class logoutSuccesHandler implements LogoutSuccessHandlerInterface
{


    public function __construct(LaUserUtil $serviceLaUserUtil, RedirectUtil $serviceRedirectUtil)
    {
        $this->serviceLaUserUtil = $serviceLaUserUtil;
        $this->serviceRedirectUtil = $serviceRedirectUtil;
    }

    /**
     * User has logout
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response|\Symfony\Component\Security\Http\Logout\Response
     */
    public function onLogoutSuccess(Request $request)
    {
        $this->serviceLaUserUtil->unsetAllCookies($request);
        // get redirect_url
        if ($request->query->has('redirect_url')) {
            $redirectUrl = $request->query->get('redirect_url');
        } else {
            $redirectUrl = $this->serviceLaUserUtil->getConf()['redirect_url'];
        }

        return $this->serviceRedirectUtil->responseRedirectTo($redirectUrl);
    }
}