<?php

namespace La\UserBundle\Tests\Validation\Constraints;

use La\UserBundle\Validation\Constraints\LaUserCharacters;

class LaUserCharactersTest extends \PHPUnit_Framework_TestCase {

    public function testMessage()
    {
        $LaUserCharacters = new LaUserCharacters();
        $this->assertEquals(3, count($LaUserCharacters->message));

        $keys = array_keys($LaUserCharacters->message);
        $values = array_values($LaUserCharacters->message);

        $this->assertEquals($keys[0], 'alphanumeric_first_char');
        $this->assertEquals($values[0], 'la_user.registration.username.fchar');
        $this->assertEquals($keys[1], 'invalid_chars');
        $this->assertEquals($values[1], 'la_user.registration.username.invalid');
        $this->assertEquals($keys[2], 'forbidden_username');
        $this->assertEquals($values[2], 'la_user.registration.username.forbidden');

    }
}
