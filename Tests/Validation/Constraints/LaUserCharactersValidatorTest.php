<?php

namespace La\UserBundle\Tests\Validation\Constraints;


use La\UserBundle\Validation\Constraints\LaUserCharacters;
use La\UserBundle\Validation\Constraints\LaUserCharactersValidator;

class LaUserCharactersValidatorTest extends \PHPUnit_Framework_TestCase
{

//            array('Antoine', 'none'),
//            array('$Antoine', 'fchar'),
//            array('A$ntoine', 'invalid'),
//            array('-Antoine', 'fchar'),
//            array('A-ntoine', 'none'),
//            array('admin', 'forbidden'),
//            array('test', 'forbidden'),
//            array('', 'none')

    public function testInvalidCharsValidate()
    {
        $constraint = new LaUserCharacters();
        $value = 'A$ntoine';

        $validator = new LaUserCharactersValidator();
        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();

        $context
            ->expects($this->once())
            ->method('addViolation')
            ->with($this->equalTo('la_user.registration.username.invalid'));

        $validator->initialize($context);
        $validator->validate($value, $constraint);
    }

    public function testInvalidFirstCharValidate()
    {
        $constraint = new LaUserCharacters();
        $value = '$Antoine';

        $validator = new LaUserCharactersValidator();
        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();

        $context
            ->expects($this->once())
            ->method('addViolation')
            ->with($this->equalTo('la_user.registration.username.fchar'));

        $validator->initialize($context);
        $validator->validate($value, $constraint);
    }

    public function testForbiddenUsernameValidate()
    {
        $constraint = new LaUserCharacters();

        $value = 'ADMIN';

        $validator = new LaUserCharactersValidator();
        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();

        $context
            ->expects($this->once())
            ->method('addViolation')
            ->with($this->equalTo('la_user.registration.username.forbidden'));

        $validator->initialize($context);
        $validator->validate($value, $constraint);
    }

    public function testNothingHappensValidate()
    {
        $constraint = new LaUserCharacters();

        $value = 'Antoine';

        $validator = new LaUserCharactersValidator();
        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();

        $context
            ->expects($this->never())
            ->method('addViolation');

        $validator->initialize($context);
        $validator->validate($value, $constraint);
    }

}
