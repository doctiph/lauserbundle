<?php

namespace La\UserBundle\Tests\Form\Type;

use La\UserBundle\Form\Type\ResettingFormType;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\Validator\Validation;

class ResettingFormTypeTest extends TypeTestCase
{
    // Validation not tested here, but Validator needed for "invalid_message" option
    protected function setUp()
    {
        parent::setUp();

        $this->factory = Forms::createFormFactoryBuilder()
            ->addTypeExtension(
                new FormTypeValidatorExtension(
                    $this->getMock('Symfony\Component\Validator\ValidatorInterface')
                )
            )
            ->addTypeGuesser(
                $this->getMockBuilder(
                    'Symfony\Component\Form\Extension\Validator\ValidatorTypeGuesser'
                )
                    ->disableOriginalConstructor()
                    ->getMock()
            )
            ->getFormFactory();

        $this->dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $this->builder = new FormBuilder(null, null, $this->dispatcher, $this->factory);
    }


    /**
     * @dataProvider testData
     */
    public function testSubmitValidData($formData)
    {
        Validation::createValidator();
        $type = new ResettingFormType('La\UserBundle\Entity\User');
        $form = $this->factory->create($type);

//        $object = new TestObject();
//        $object->fromArray($formData);

        // submit the data to the form directly
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());
//        $this->assertEquals($object, $form->getData());

        $view = $form->createView();
        $children = $view->children;

        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }
    }

    public function testData()
    {
        return array(
            array(
                'data' => array(
                    'plainPassword_first' => 'bonjour1234',
                    'plainPassword_second' => 'bonjour1234',
                ),
            ),
            array(
                'data' => array(),
            ),
            array(
                'data' => array(
                    'plainPassword_first' => null,
                    'plainPassword_second' => null,
                ),
            ),
        );
    }
}