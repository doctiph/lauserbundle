<?php

namespace La\UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{

    public function testLogin()
    {
        $client = static::createClient(array(), array(
            'HTTP_HOST' => 'www.toto.fr' // pour getDomain
        ));
        $crawler = $client->request('GET', '/login');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $submitButton = $crawler->filter('button[id=_submit]');
        $this->assertCount(1, $submitButton);

        $form = $submitButton->form();
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());

        $this->assertTrue('http://www.toto.fr/login_check' === $form->getUri());


//        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }

    public function testLoginPost()
    {

        $client = static::createClient(array(), array(
            'HTTP_HOST' => 'www.toto.fr' // pour getDomain
        ));

        $crawler = $client->request('GET', '/login');
        $submitButton = $crawler->filter('button[id=_submit]');
        $form = $submitButton->form();

        $form->setValues(array(
            "_username" => "ladanalytics@gmail.com",
            "_password" => "pasteque98;"
        ));

        $client->submit($form);

        $crawler = $client->followRedirect();

        $this->assertTrue($client->getResponse()->isRedirection());

        $this->assertTrue($crawler->filter(
            'html:contains("Redirecting to /login")'
        )->count() > 0);

        // reloging
        $crawler = $client->request('GET', '/login');
        $client->followRedirect();

//        $this->assertTrue($client->getResponse()->isRedirection());

        ob_flush();
       //echo $client->getResponse()->getContent();
        ob_start();


    }
}