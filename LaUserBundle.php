<?php

namespace La\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LaUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
