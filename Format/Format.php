<?php

namespace La\UserBundle\Format;

use Doctrine\ORM\EntityManager;
use La\UserBundle\Entity\User;
use La\UserBundle\Lib\LaUserUtil;

class Format
{

    public function __construct(EntityManager $em, LaUserUtil $serviceUser, $conf)
    {
        $this->em = $em;
        $this->serviceUser = $serviceUser;
        $this->conf = $conf;
    }

    /**
     * Format and send User data
     *
     * @param User $user
     * @param string $format
     * @return mixed
     */
    public function formatAndSendUser(User $user, $format)
    {
        if (is_null($user)) {
            return $this->formatClassFactory($format)->send($this->formatClassFactory($format)->formatError());
        }
        $data = $this->getData($user);
        $formattedData = $this->formatClassFactory($format)->formatUser($data);
        return $this->formatClassFactory($format)->send($formattedData);
    }

    /**
     * Format and send error
     *
     * @param string $format
     * @return mixed
     */
    public function formatAndSendError($format)
    {
        return $this->formatClassFactory($format)->send($this->formatClassFactory($format)->formatError());
    }

    /**
     * Format and send data from token
     *
     * @param string $token
     * @param string $format
     * @return mixed
     */
    public function getFormatedDataFromToken($token, $format)
    {
        try {
            $user = $this->serviceUser->getUserFromToken($token);
            if (is_null($user)) {
                return $this->formatAndSendError($format);
            }
            return $this->formatAndSendUser($user, $format);
        } catch (\Exception $e) {
            return $this->formatAndSendError($format);
        }
    }

    /**
     * Format and send data from id
     *
     * @param string $idUser
     * @param string $format
     * @return mixed
     */
    public function getFormatedDataFromId($idUser, $format)
    {
        try {
            $user = $this->serviceUser->getUserFromId($idUser);
            if (is_null($user)) {
                return $this->formatAndSendError($format);
            }
            return $this->formatAndSendUser($user, $format);
        } catch (\Exception $e) {
            return $this->formatAndSendError($format);
        }
    }

    /**
     * Format and send data from email
     *
     * @param string $email
     * @param string $format
     * @return mixed
     */
    public function getFormatedDataFromEmail($email, $format)
    {
        try {
            $user = $this->serviceUser->getUserFromEmail($email);
            if (is_null($user)) {
                return $this->formatAndSendError($format);
            }
            return $this->formatAndSendUser($user, $format);
        } catch (\Exception $e) {
            return $this->formatAndSendError($format);
        }
    }

    /**
     * Format and send data from username
     *
     * @param string $username
     * @param string $format
     * @return mixed
     */

    public function getFormatedDataFromUsername($username, $format)
    {
        try {
            $user = $this->serviceUser->getUserFromUsername($username);
            if (is_null($user)) {
                return $this->formatAndSendError($format);
            }
            return $this->formatAndSendUser($user, $format);
        } catch (\Exception $e) {
            return $this->formatAndSendError($format);
        }
    }


    /**
     * Get format class
     *
     * @param $format
     * @return string
     */
    protected function formatClassFactory($format)
    {
        $class = sprintf('%s\\Format%s', __NAMESPACE__, ucfirst($format));
        return new $class();
    }

    /**
     * @return array
     *
     * Get format class
     */
    protected function formatAdditionnalClassesFactory()
    {
        $result = [];
        foreach ($this->conf['additionnal_data'] as $namespace) {
            $result[] = sprintf('%s', $namespace);
        }
        return $result;
    }

    public function getError($key)
    {
        switch ($key) {
            case 'invalid_user':
                return array('user' => array('error' => 'Invalid User'));
                break;
            case 'anonymous_user':
                return array('user' => array('error' => 'Anonymous User'));
                break;
            default:
                return array('error' => 'unknown');
        }
    }

    public function getData(User $user)
    {
        if (is_null($user)) {
            throw new \Exception('Invalid User');
        }
        $crm = $user->getCrm();
        if (is_null($crm)) {
            throw new \Exception('Invalid User Crm');
        }

        $res = array(
            'user' => array(
                'id' => $user->getId(),
                'remoteId' => $user->getId(), // For eZPublish
                'username' => $user->getDisplayUserName(),
                'email' => $user->getEmail(),
            )
        );

        $res['crm'] = array();

        $map = $crm->properties();
        $excluded = array('__isInitialized__', 'email', 'user');
        foreach ($map as $key => $property) {
            if (in_array($key, $excluded)) {
                continue;
            }
            $res['crm'][$key] = $property;
        }

        foreach ($this->formatAdditionnalClassesFactory() as $additionnal) {
            $extra = new $additionnal();
            $res = $res + $extra->getData($user);
        }

        return $res;

    }


}