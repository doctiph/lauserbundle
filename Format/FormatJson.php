<?php

namespace La\UserBundle\Format;

use La\UserBundle\Entity\User;
use La\UserBundle\Format\FormatInterface;

class FormatJson implements FormatInterface
{
    /**
     * Format json on error
     *
     * @return string
     */
    public function formatError()
    {
        return json_encode(null);
    }

    /**
     * Format json data
     *
     * @param array $data
     * @return mixed|string
     * @throws \Exception
     */
    public function formatUser(array $data)
    {

        if (is_null($data)) {
            throw new \Exception('Invalid Data');
        }
        return json_encode($data);
    }

    /**
     * Send json and headers
     *
     * @param $data
     * @return mixed|void
     */
    public function send($data)
    {
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        exit($data);
    }

}