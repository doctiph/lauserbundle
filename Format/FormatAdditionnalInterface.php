<?php

namespace La\UserBundle\Format;

use La\UserBundle\Entity\User;

interface FormatAdditionnalInterface
{

    /**
     * Add User data
     *
     * @param User $user
     * @return mixed
     */
    static function getData(User $user);


}