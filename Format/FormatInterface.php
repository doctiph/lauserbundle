<?php

namespace La\UserBundle\Format;


interface FormatInterface
{
    /**
     * Format error
     *
     * @return mixed
     */
    function formatError();

    /**
     * Format data user
     *
     * @param array $data
     * @return mixed
     */
    function formatUser(array $data);

    /**
     * Send data and headers
     * @param $data
     * @return mixed
     */
    function send($data);

}