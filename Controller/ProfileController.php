<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace La\UserBundle\Controller;

use FOS\UserBundle\Controller\ProfileController as BaseProfileController;
use La\UserBundle\Entity\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use La\UserBundle\Lib\RedirectUtil;
use La\UserBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends BaseProfileController
{

    /**
     * Edit the user
     */
    public function editAction(Request $request)
    {
        /**
         * @var RedirectUtil $redirectutils
         */
        $redirectutils = $this->container->get('la_user.lib.redirectutils');
        $redirectutils->setRedirectAction($request);

        if ($request->isMethod('POST')) {
            // On edit, change "modified" date of crm
            $user = $this->container->get('security.context')->getToken()->getUser();

            if ($user instanceof User) {
                $modified = new \DateTime();
                $user->setModified($modified);
                $crm = $user->getCrm();
                if (!is_null($crm)) {
                    $crm->setModified($modified);
                }
            }
        }

        return parent::editAction($request);
    }

    /**
     * Delete the user
     */
    public function deleteAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        if ($request->isMethod('POST')) {
            $redirectutils = $this->container->get('la_user.lib.redirectutils');
            $redirectutils->setRedirectAction($request);

            $user = $this->container->get('security.context')->getToken()->getUser();
            if ($user instanceof User) {
                $userUtils = $this->container->get('la_user.lib.lauserutils');
                try {
                    $userUtils->deleteUser($user->getEmailCanonical());
                    $userUtils->logoutIfNeeded($request);
                    $redirectutils->getReponseRedirect($request);
                } catch (\Exception $e) {
                    return $this->container->get('templating')->renderResponse('LaUserBundle:Profile:delete.html.' . $this->container->getParameter('fos_user.template.engine'), array('error' => true));
                }
            }
        }

        return $this->container->get('templating')->renderResponse('LaUserBundle:Profile:delete.html.' . $this->container->getParameter('fos_user.template.engine'));
    }
}
