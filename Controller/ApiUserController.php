<?php

namespace La\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\GenericEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use La\UserBundle\LaUserEvents;
use La\UserBundle\Entity\User;
use La\UserBundle\Api\Exception as ApiUserException;
use La\ApiBundle\Controller\AbstractApiController;
use La\ApiBundle\Event\ApiResponseEvent;

class ApiUserController extends AbstractApiController
{

    const KEY = 'user';

    /**
     * @Rest\View()
     * @Rest\Get("user/{token}")
     */
    public function getAction(Request $request, $token)
    {
        try {
            $this->checkSecurity();

            $user = $this->checkUserToken($token);
            $userData = $this->getUserData($user);

            return $this->send($userData, Codes::HTTP_OK);

        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Post("user/login")
     */
    public function loginAction(Request $request)
    {
        try {
            $this->checkSecurity();

            $credentials = $this->getCredentialsFromRequest($request);
            $user = $credentials['user'];
            $password = $credentials['password'];

            if (!$user instanceof User) {
                throw new ApiUserException\InvalidCredentialsException();
            }

            if (is_null($this->container->get('la_user.lib.lauserutils')->getUserLoggedToken($user))) {
                if (!$user->isEnabled() || $user->isExpired() || $user->isLocked()) {
                    throw new ApiUserException\DisabledUserException();
                }
            }

            $factory = $this->container->get('security.encoder_factory');
            $encoder = $factory->getEncoder($user);

            $passwordValid = $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());


            if ($passwordValid) {
                try {
                    $userData = $this->loginUser($user);
                } catch (\Exception $e) {
                    throw new ApiUserException\DisabledUserException();
                }
                return $this->send($userData, Codes::HTTP_OK);
            } else {
                throw new ApiUserException\InvalidCredentialsException();
            }
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Post("user/logout/{token}")
     */
    public function logoutAction(Request $request, $token)
    {
        try {
            $this->checkSecurity();

            $user = $this->checkUserToken($token);

            if (!$user instanceof User) {
                throw new ApiUserException\InvalidTokenException();
            }

            if ($this->get('la_user.lib.lauserutils')->removeToken($token)) {
                // @todo renvoyer null
                return $this->send(array("success" => true), Codes::HTTP_OK);
            }
            throw new ApiUserException\InvalidTokenException();

        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    // Register
    /**
     * @Rest\View()
     * @Rest\Post("user")
     */
    public function registerAction(Request $request)
    {
        try {
            $this->checkSecurity();

            // Dispatch before register to allow special registration behaviour.
            $event = $this->dispatch(LaUserEvents::API_BEFORE_REGISTRATION, $request);

            // If event propagation was stopped, a user was found. Format it and return it.
            if ($event->isPropagationStopped()) {
                $user = $event->getUser();

                if (!is_null($user)) {
                    $userData = $this->loginUser($user);
                    return $this->send($userData, Codes::HTTP_OK);
                }
                throw new ApiUserException\InvalidUserException();
            }

            // Regular registration behaviour
            $client = $request->getUser();

            $api = $this->container->getParameter("la_user")['api'];

            $form = $this->createForm(
                $this->container->get($api['clients'][$client]['form_types']['registration'])
            );

            $errors = $this->getErrors($form, $request);

            if (!$errors) {
                $user = $form->getData();


                $this->get('la_user.lib.lauserutils')->setCrmTmpData($user, 'api_' . $client);

                if ($request->query->has('redirect_url')) {
                    // set session for redirect in events
                    $this->get('la_user.lib.redirectutils')->addRedirectUrl($request->query->get('redirect_url'));
                }

                $this->container->get('event_dispatcher')->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, new FormEvent($form, $request));

                if ($api['clients'][$client]['quick_register']) {

                    // Avoid profiler issues with 201 code returned
                    if ($this->container->has('profiler'))
                    {
                        $this->container->get('profiler')->disable();
                    }

                    $this->get('la_user.lib.lauserutils')->activateUserCrm($user);
                    // Enable it to login user, persist it and get data.
                    $user->setLastLogin(new \DateTime());
                    $user->setEnabled(true);
                    $this->get("fos_user.user_manager")->updateUser($user);
                    $userData = $this->loginUser($user);
                    // Then re-disable it and send confirmation mail.
                    $user->setEnabled(false);
                    $this->get("fos_user.user_manager")->updateUser($user);
                    return $this->send($userData, Codes::HTTP_CREATED);
                }
                $this->get("fos_user.user_manager")->updateUser($user);
                return $this->send(array("success" => true), Codes::HTTP_ACCEPTED);

            } else {
                return $this->sendError(array('form' => $errors));
            }

        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    // Edit
    /**
     * @Rest\View()
     * @Rest\Put("user/{token}")
     */
    public function editAction(Request $request, $token)
    {
        try {
            $this->checkSecurity();

            $user = $this->checkUserToken($token);
            if (!$user instanceof User) {
                throw new ApiUserException\InvalidTokenException();
            }

            $client = $request->getUser();

            $api = $this->container->getParameter("la_user")['api'];
            $form = $this->createForm(
                $this->container->get($api['clients'][$client]['form_types']['profile']),
                $user
            );

            $form = $this->container->get('la_user.lib.form')->filterFormFromRequest($form, $request);

            if (is_null($user->getCrm())) { // @todo edition d'un compte sans crm : compte non validé : exception spéciale
                throw new ApiUserException\InvalidCrmException();
            }

            $errors = $this->getErrors($form, $request);
            if (!$errors) {

                $user->setModified(new \DateTime());
                $user->getCrm()->setModified(new \DateTime());

                $this->get("fos_user.user_manager")->updateUser($form->getData());
                $userData = $this->getUserData($user);
                return $this->send($userData, Codes::HTTP_OK);
            } else {
                return $this->sendError(array('form' => $errors));
            }


        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    //Change Email
    /**
     * @Rest\View()
     * @Rest\Patch("user/{token}/email")
     */
    public function changeEmailAction(Request $request, $token)
    {
        try {
            $this->checkSecurity();

            $user = $this->checkUserToken($token);

            if (!$user instanceof User) {
                throw new ApiUserException\InvalidTokenException();
            }

            $form = $this->container->get('form.factory')->createNamed(
                'la_user_change_email',
                $this->container->get('la_user.change_email.form.type'),
                null,
                array('validation_groups' => "LaEdit")
            );

            $oldEmail = $user->getEmail();
            $form->setData($user);
            $form->submit($request);

            if ($form->isValid()) {

                $user->setEmailRequested($form->getData()->getEmail());
                $user->setEmailRequestedAt(new \DateTime());
                $user->setEmailRequestedToken($this->container->get('fos_user.util.token_generator')->generateToken());
                $user->setEmail($oldEmail);
                $this->container->get('fos_user.user_manager')->updateUser($user);

                $this->container->get('la_user.mailer')->sendChangeEmailMessage($form->getData());

                return $this->send(array("success" => true), Codes::HTTP_OK);
            } else {
                throw new ApiUserException\InvalidUserException();
            }
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    // Change Password
    /**
     * @Rest\View()
     * @Rest\Patch("user/{token}/password")
     */
    public function changePasswordAction(Request $request, $token)
    {
        try {
            $this->checkSecurity();

            $user = $this->checkUserToken($token);
            if (!$user instanceof User) {
                throw new ApiUserException\InvalidTokenException();
            }
            $this->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('la_user.firewall_name'),
                $user
            );

            $form = $this->createForm('la_user_change_password');
            $form->setData($user);

            $errors = $this->getErrors($form, $request);

            if (!$errors) {

                $user->setModified(new \DateTime());
                $user->getCrm()->setModified(new \DateTime());
                $this->get("fos_user.user_manager")->updateUser($form->getData());
                $userData = $this->getUserData($user, $token);
                return $this->send($userData, Codes::HTTP_OK);

            } else {
                return $this->sendError(array('form' => $errors));
            }


        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    /**
     * @Rest\View()
     * @Rest\Delete("user/{email}", requirements={"email"=".+"})
     */
    public function deleteAction(Request $request, $email)
    {
        try {
            $this->checkSecurity();

            $userUtils = $this->container->get('la_user.lib.lauserutils');
            $userUtils->deleteUser($email);

            return $this->send(sprintf("'%s' account successfuly deleted", $email), 204);
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }

    //Reset Password
    /**
     * @Rest\View()
     * @Rest\Post("user/reset/{email}", requirements={"email"=".+"})
     */
    public function resetAction(Request $request, $email)
    {
        try {
            $this->checkSecurity();

            //Duplicate request and change it to be forwardable to resetting controller.
            $forwardRequest = $request->duplicate(null, array('username' => $email));
            $this->forward('FOSUserBundle:Resetting:sendEmail', array('request' => $forwardRequest));

            if ($this->get('session')->getFlashBag()->has('la_reset_password_success')) {
                return $this->send(array("success" => true), Codes::HTTP_OK);
            } else {
                throw new ApiUserException\InvalidUserException();
            }
        } catch (\Exception $e) {
            return $this->sendError($e);
        }
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected function loginUser(User $user)
    {
        $loginManager = $this->get('fos_user.security.login_manager');
        $loginManager->loginUser($this->container->getParameter('la_user.firewall_name'), $user);
        $token = $this->get('la_user.lib.lauserutils')->createAndStoreToken($user);

        $userData = $this->getUserData($user, $token);

        if ($this->container->get('kernel')->getEnvironment() === 'dev') {
            $userData = array_merge($userData, array('dev_encoded_token' => $this->get('la_user.lib.lauserutils')->encodeToken($token)));
        }
        return $userData;
    }

    protected function getUserData(User $user, $token = null)
    {

        $userData = array();
        $event = new GenericEvent($user, array('data' => $userData));

        $format = $this->container->get('event_dispatcher')->dispatch(LaUserEvents::API_FORMAT_USER_REQUEST, $event);
        $userData = $format['data'];

        if (is_null($token)) {
            return $userData;
        }
        return array_merge($userData, array('token' => $token));

    }


    /**
     * @param Request $request
     * @return array
     * @throws \La\UserBundle\Api\Exception\InvalidCredentialsException
     */
    protected function getCredentialsFromRequest(Request $request)
    {
        $encodedCredentials = $request->get('credentials');

        if (is_null($encodedCredentials)) {
            throw new ApiUserException\InvalidCredentialsException();
        }

        // Decode credentials
        $credentials = $this->decode($encodedCredentials);
        if (count($credentials) != 2) {
            throw new ApiUserException\InvalidCredentialsException();
        }

        try {
            // Determine user :
            if (preg_match('/^[a-zA-Z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$/', $credentials[0]) === 1) {
                $user = $this->get('la_user.lib.lauserutils')->getUserFromEmail($credentials[0]);
            } else {
                $user = $this->get('la_user.lib.lauserutils')->getUserFromUsername($credentials[0]);
            }

            // Password check
            if (is_null($credentials[1])) {
                throw new ApiUserException\InvalidCredentialsException();
            }

        } catch (\Exception $e) {
            throw new ApiUserException\InvalidCredentialsException();
        }

        return array("user" => $user, "password" => $credentials[1]);
    }


    /**
     * @param $var
     * @param string $delimiter
     * @return array
     */
    protected function decode($var, $delimiter = ":")
    {
        $decodedVar = base64_decode($var);
        $decodedArray = explode($delimiter, $decodedVar);

        return $decodedArray;
    }

    protected function dispatch($eventName, $request)
    {
        $dispatcher = $this->container->get('event_dispatcher');
        $event = new ApiResponseEvent($request);

        $dispatcher->dispatch($eventName, $event);

        return $event;
    }

    /**
     * @param $token
     * @return User
     * @throws \La\UserBundle\Api\Exception\InvalidTokenException
     */
    protected function checkUserToken($token)
    {
        try {
            if (is_null($token)) {
                throw new ApiUserException\InvalidTokenException();
            }
            $user = $this->get('la_user.lib.lauserutils')->getUserFromToken($token);
        } catch (\Exception $e) {
            throw new ApiUserException\InvalidTokenException();
        }

        return $this->checkUser($user);
    }

    /**
     * @param $user
     * @return User
     * @throws \La\UserBundle\Api\Exception\InvalidCrmException
     * @throws \La\UserBundle\Api\Exception\InvalidUserException
     * @throws \La\UserBundle\Api\Exception\AnonymousUserException
     */
    protected function checkUser($user)
    {
        if (!$user instanceof User) {
            throw new ApiUserException\AnonymousUserException();
        }

        if (is_null($user)) {
            throw new ApiUserException\InvalidUserException();
        }

        $crm = $user->getCrm();
        if (is_null($crm)) {
            throw new ApiUserException\InvalidCrmException();
        }
        return $user;
    }

}