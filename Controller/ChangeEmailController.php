<?php

namespace La\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use La\UserBundle\LaUserEvents;

class ChangeEmailController extends ContainerAware
{

    /**
     * Change user email
     */
    public function changeEmailAction(Request $request)
    {
        // validation Ajax
        if ($request->isXmlHttpRequest()) {
            $form = $this->container->get('form.factory')->createNamed(
                'la_user_change_email',
                $this->container->get('la_user.change_email.form.type'),
                null,
                array('validation_groups' => "LaEdit")
            );
            $validateService = $this->container->get('la_user.lib.lauservalidateutils');
            return $validateService->validAjax($form, $request);
        }

        // On edit, change "modified" date of user
        $user = $this->container->get('security.context')->getToken()->getUser();

        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $dispatcher = $this->container->get('event_dispatcher');
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(LaUserEvents::CHANGE_EMAIL_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->container->get('form.factory')->createNamed(
            'la_user_change_email',
            $this->container->get('la_user.change_email.form.type'),
            null,
            array('validation_groups' => "LaEdit")
        );

        $oldEmail = $user->getEmail();
        $form->setData($user);

        if ($request->isMethod('POST')) {
            $form->submit($request);

            if ($form->isValid()) {

                $session = $this->container->get('session');
                $session->getFlashBag()->clear();
                $session->getFlashBag()->add('la_change_email_success', 'la_user.change_email.success');

                $user->setEmailRequested($form->getData()->getEmail());
                $user->setEmailRequestedAt(new \DateTime());
                $user->setEmailRequestedToken($this->container->get('fos_user.util.token_generator')->generateToken());
                $user->setEmail($oldEmail);
                $this->container->get('fos_user.user_manager')->updateUser($user);

                $this->container->get('la_user.mailer')->sendChangeEmailMessage($form->getData());

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(LaUserEvents::CHANGE_EMAIL_SUCCESS, $event);


                if (null === $response = $event->getResponse()) {
                    $response = $this->container->get('templating')->renderResponse(
                        'LaUserBundle:ChangeEmail:changeEmail.html.twig',
                        array('form' => $form->createView())
                    );
                }
                return $response;
            }
        }

        return $this->container->get('templating')->renderResponse(
            'LaUserBundle:ChangeEmail:changeEmail.html.twig',
            array('form' => $form->createView())
        );
    }

    public function confirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->findUserBy(array("emailRequestedToken" => $token));
        if (null === $user) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:ChangeEmail:invalidToken.html.twig', array());
        }

        $newEmail = $user->getEmailRequested();
        if (null === $newEmail) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:ChangeEmail:invalidToken.html.twig', array());
        }

        $user->setEmail($newEmail);
        $crm = $user->getCrm();
        $crm->setEmail($user->getEmail());

        $modified = new \DateTime();
        $user->setModified($modified);
        $crm->setModified($modified);

        $user->setCrm($crm);

        $user->setEmailRequested(null);
        $user->setEmailRequestedAt(null);
        $user->setEmailRequestedToken(null);

        $userManager->updateUser($user);

        $session = $this->container->get('session');
        $session->getFlashBag()->clear();
        $session->getFlashBag()->add('la_change_email_complete', 'la_user.change_email.complete');

        $url = $this->container->get('router')->generate('fos_user_profile_show');
        $response = new RedirectResponse($url);
        $dispatcher->dispatch(LaUserEvents::CHANGE_EMAIL_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

}
