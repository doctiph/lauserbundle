<?php

namespace La\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserController extends ContainerAware
{

    /**
     * Returns user from id
     *
     * @param $id
     * @param $format
     * @return mixed
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function idAction($id, $format)
    {
        if (false === $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->container->get('la_user.format.format')->getFormatedDataFromId($id, $format);

    }

    /**
     * Returns user from email
     *
     * @param $email
     * @param $format
     * @return mixed
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function emailAction($email, $format)
    {
        if (false === $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->container->get('la_user.format.format')->getFormatedDataFromEmail($email, $format);

    }

    /**
     * Returns user from username
     *
     * @param $username
     * @param $format
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function usernameAction($username, $format)
    {
        if (false === $this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->container->get('la_user.format.format')->getFormatedDataFromUsername($username, $format);

    }

}
