<?php

namespace La\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends ContainerAware
{


    public function indexAction()
    {
        return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));
    }
}
