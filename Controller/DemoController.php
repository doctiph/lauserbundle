<?php

namespace La\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DemoController extends ContainerAware
{


    public function indexAction()
    {
        return $this->container->get('templating')->renderResponse(
            'LaUserBundle:Demo:index.html.twig');
    }

}
