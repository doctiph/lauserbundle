<?php

namespace La\UserBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;

class RedirectController extends ContainerAware
{

    public function redirectAction(Request $request)
    {
        $redirectUrl = $request->query->get('redirect_url');
        return $this->container->get('templating')->renderResponse('LaUserBundle:Redirect:redirect.html.twig', array('redirect_url' => $redirectUrl));
    }

}
