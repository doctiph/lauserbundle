<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace La\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use La\AdminBundle\Controller\AdminController as BaseAdminController;

/**
 * Controller managing the user profile
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class AdminController extends BaseAdminController
{

    protected $currentMenu = 'la_user_admin.nav.users.default';


    public function statsAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN')) {
            throw new AccessDeniedException();
        }

        return $this->render('LaUserBundle:Admin:stats.html.twig', array(
            'layout' => "LaStatsBundle:Display:admin_layout.html.twig",
            'type' => 'user',
            'current_menu' => 'la_user_admin.nav.stats.default',
            'current_sub_menu' => 'la_user_admin.nav.stats.users',
        ));
    }

    public function exportAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_USER')) {
            throw new AccessDeniedException();
        }

        $emails = null;

        $form = $this->createFormBuilder()
            ->add('emails', 'textarea', array('label' => 'Emails'))
            ->add('search', 'submit', array('label' => 'Exporter'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            if (isset($data['emails'])) {
                $emails = $data['emails'];
                $emails = explode(PHP_EOL, $emails);
            }
        }

        if (!is_null($emails)) {
            /** @var $crmManager \La\UserBundle\Services\CrmManager */
            $crmManager = $this->container->get('la_crm.manager');
            $list = array();
            $head = array();
            foreach ($emails as $email) {
                $email = trim($email);
                $crm = $crmManager->getByEmail($email);
                if (!is_null($crm)) {
                    if (!count($head)) {
                        $excluded = array('__isInitialized__', 'id', 'user', 'newsletters');
                        $map = $crm->properties();
                        foreach ($map as $key => $property) {
                            if (in_array($key, $excluded)) {
                                continue;
                            }
                            $head[] = $key;
                        }
                        $head[] = 'id_user';
                        $head[] = 'username';
                        $list[] = $head;
                    }
                    $data = array();
                    reset($head);
                    foreach ($head as $key) {
                        if ($key == 'id_user' || $key == 'username') {
                            continue;
                        }
                        $method = 'get' . strtoupper($key);
                        $d = $crm->$method();
                        if ($d instanceof \DateTime) {
                            $d = $d->format('d/m/Y');
                        } elseif (is_object($d)) {
                            die(var_dump($d));
                        }
                        $data[] = $d;
                    }
                    $data[] = $crm->getUser()->getId();
                    $data[] = $crm->getUser()->getUserName();
                    $list[] = $data;
                }
            }

            $now = new \DateTime();
            $filename = sprintf('export_users_%s.csv', $now->format('Y-m-d_H-i-s'));
            if (count($list)) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment;filename=' . $filename);

                $fp = fopen('php://output', 'w');

                foreach ($list as $fields) {
                    fputcsv($fp, $fields);
                }

//            die(var_dump($list));

                fclose($fp);
                exit;
            } else {
                $this->alert('warning', sprintf('Aucune donnée trouvée'));
            }
        }

        return $this->render('LaUserBundle:Admin:crm_export.html.twig', array(
            'form' => $form->createView(),
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_user_admin.nav.users.export'
        ));

    }

    public function crmAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_USER')) {
            throw new AccessDeniedException();
        }

        $email = null;

        $form = $this->createFormBuilder()
            ->add('crm', 'text', array('label' => 'Email'))
            ->add('search', 'submit', array('label' => 'Cherher'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            if (isset($data['crm'])) {
                $email = $data['crm'];
            }
        } else if ($request->query->has('email')) {
            $email = $request->query->get('email');
        }

        if (!is_null($email)) {

            /** @var $crmManager \La\UserBundle\Services\CrmManager */
            $crmManager = $this->container->get('la_crm.manager');
            $crm = $crmManager->getByEmail($email);
            if (!is_null($crm)) {
                $this->history('email', $email);

                $res = array();
                $res['id'] = $crm->getId();
                $res['email'] = $crm->getEmail();
                $res['created'] = $crm->getCreated()->format('d/m/Y H:i:s');
                $res['modified'] = $crm->getModified()->format('d/m/Y H:i:s');
                $res['lastActivity'] = $crm->getLastActivity()->format('d/m/Y H:i:s');
                $res['firstName'] = $crm->getFirstName();
                $res['lastname'] = $crm->getLastName();
                $res['origin'] = $crm->getOrigin();

                $map = $crm->properties();
                $excluded = array(
                    'id', 'email', 'created', 'modified', 'lastActivity', 'newsletters',
                    'user', '__isInitialized__', '_entityPersister', '_identifier'
                );

                foreach ($map as $key => $property) {
                    if (in_array($key, $excluded)) {
                        continue;
                    }
                    if ($key === 'gender') {
                        $prefix = 'la_user_admin.crm.';
                        $property = $prefix . 'gender.' . $property;
                    }
                    if (is_null($property) || is_scalar($property)) {
                        $res[$key] = $property;
                    }
                }

                return $this->render('LaUserBundle:Admin:crm.html.twig', array(
                    'crm' => $res,
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_user_admin.nav.users.crm'
                ));
            } else {
                $this->alert('danger', 'L\'email spécifié n\'est pas dans la base Crm');

                return $this->render('LaUserBundle:Admin:crm_search.html.twig', array(
                    'form' => $form->createView(),
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_user_admin.nav.users.crm'
                ));
            }
        } else {
            return $this->render('LaUserBundle:Admin:crm_search.html.twig', array(
                'form' => $form->createView(),
                'current_menu' => $this->currentMenu,
                'current_sub_menu' => 'la_user_admin.nav.users.crm'
            ));
        }

    }

    public function unconfirmedAction()
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_USER_ACTIVATE')) {
            throw new AccessDeniedException();
        }

        $userLib = $this->get('la_user.lib.lauserutils');
        $users = $userLib->getUnconfirmedUsers();
        $userManager = $this->get('fos_user.user_manager');
        $res = [];

        $problems = [];
        foreach ($users as $u) {
            $user = $userManager->findUserByEmail($u['email']);
            if (is_null($user)) {
                $problems[] = $u['email'];
            } else {
                $res[] = array(
                    'created' => $user->getCreated(),
                    'username' => $user->getUsernameCanonical(),
                    'email' => $user->getEmailCanonical()
                );
            }
        }

        if (count($problems)) {
            $this->alert('danger', sprintf('Une erreur est survenue avec le(s) user(s) : %s. Merci de contacter un admin.', implode(', ', $problems)));
        }

        return $this->render('LaUserBundle:Admin:unconfirmed.html.twig', array(
            'users' => $res,
            'current_menu' => $this->currentMenu,
            'current_sub_menu' => 'la_user_admin.nav.users.unconfirmed'
        ));
    }

    public function enableUserAction(Request $request, $username)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_USER_ACTIVATE')) {
            throw new AccessDeniedException();
        }

        try {
            $userManager = $this->get('fos_user.user_manager');
            $user = $userManager->findUserByUsername($username);
            if (is_null($user)) {
                throw new Exception('Le user spécifié n\'est pas dans la bae User');
            }

            $userActivator = $this->get('la_user.activate_user.command');
            $userActivator->activateUser($user, $username, null);

            $this->alert('success', sprintf('Le user %s a été activé', $username));
        } catch (\Exception $e) {
            $this->alert('danger', sprintf('Une erreur est survenue : %s', $e->getMessage()));
        }
        if ($request->query->has('from') && $request->query->get('from') === 'unconfirmed') {
            return new RedirectResponse($this->get('router')->generate('la_user_admin_user_unconfirmed'));
        } else {
            return new RedirectResponse($this->get('router')->generate('la_user_admin_user', array('user' => $username)));
        }
    }

    /**
     * Edit the user
     */
    public function userAction(Request $request)
    {

        if (false === $this->get('security.context')->isGranted('ROLE_LA_ADMIN_LA_USER')) {
            throw new AccessDeniedException();
        }

        $emailOrUsername = null;

        $form = $this->createFormBuilder()
            ->add('user', 'text', array('label' => 'Email ou Username'))
            ->add('search', 'submit', array('label' => 'Cherher'))
            ->getForm();

        $form->handleRequest($request);

        if ($request->isMethod('POST')) {
            $data = $form->getData();
            if (isset($data['user'])) {
                $emailOrUsername = $data['user'];
            }
        } else if ($request->query->has('email')) {
            $emailOrUsername = $request->query->get('email');
        } else if ($request->query->has('user')) {
            $emailOrUsername = $request->query->get('user');
        }

        if (!is_null($emailOrUsername)) {


            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $userManager = $this->container->get('fos_user.user_manager');
            $user = $userManager->findUserByUsernameOrEmail($emailOrUsername);

            $prefix = 'la_user_admin.user.';
            if (!is_null($user)) {


                $res = array();
                $res['id'] = $user->getId();
                $res['username'] = $user->getUsernameCanonical();
                $res['email'] = $user->getEmailCanonical();
                $res['created'] = $user->getCreated()->format('d/m/Y H:i:s');
                $res['modified'] = $user->getModified()->format('d/m/Y H:i:s');
                $res['enabled'] = true;

                if (!$user->isEnabled()) {
                    $res['enabled'] = false;
                } else {

                }

                $this->history('email', $res['email']);
                $this->history('user', $res['username']);

                return $this->render('LaUserBundle:Admin:user.html.twig', array(
                    'user' => $res,
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_user_admin.nav.users.user'
                ));
            } else {
                $this->alert('danger', 'L\'email ou le username spécifié n\'est pas dans la base');

                return $this->render('LaUserBundle:Admin:user_search.html.twig', array(
                    'form' => $form->createView(),
                    'current_menu' => $this->currentMenu,
                    'current_sub_menu' => 'la_user_admin.nav.users.user'
                ));
            }
        } else {
            return $this->render('LaUserBundle:Admin:user_search.html.twig', array(
                'form' => $form->createView(),
                'current_menu' => $this->currentMenu,
                'current_sub_menu' => 'la_user_admin.nav.users.user'
            ));
        }
    }

}
