<?php

namespace La\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseSecurityController;
use Symfony\Component\HttpFoundation\Request;
use La\UserBundle\Lib\RedirectUtil;
use La\UserBundle\Lib\LaUserUtil;

class SecurityController extends BaseSecurityController
{
    public function tokenAction($token, $format)
    {
        $formatService = $this->container->get('la_user.format.format');

        return $formatService->getFormatedDataFromToken($token, $format);

    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        /**
         * @var LaUserUtil $lauserutils
         */

        $lauserutils = $this->container->get('la_user.lib.lauserutils');
        /**
         * @var RedirectUtil $redirectutils
         */
        $redirectutils = $this->container->get('la_user.lib.redirectutils');
        // No token cookie
        $lauserutils->logoutIfNeeded($request);

        // User is logged
        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $redirectutils->getReponseRedirect($request);
        } else {
            $redirectutils->setRedirectAction($request);
        }
        // parent
        return parent::loginAction($request);
    }
}
