<?php

namespace La\UserBundle\Controller;

use La\UserBundle\Event\CrmEvent;
use La\UserBundle\LaUserEvents;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;

class TrackController extends ContainerAware
{
    const COOKIE_EXPIRE = 30;

    /**
     * Track user by cookie
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tagAction(Request $request)
    {
        $service = $this->container->get('la_user.lib.lauserutils');
        $domain = $service->getDomain($request->getHttpHost());
        $response = $this->container->get('templating')->renderResponse('LaUserBundle:Track:tag.js.twig',
            array(
                'domain' => $domain,
                'cookie_name' => $service::COOKIE_LA_TRACKING_COOKIE_NAME,
                'cookie_token' => $service::COOKIE_LA_TOKEN,
                'cookie_expire' => static::COOKIE_EXPIRE,
            )
        );

        $response->headers->set('Content-Type', 'application/javascript');
        $response->setPublic();
        $response->setMaxAge(30 * 365 * 24 * 60 * 60);
        return $response;
    }

    /**
     * Create a cookie for testing
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction()
    {
        $response = $this->container->get('templating')->renderResponse('LaUserBundle:Track:create.html.twig');
        $response->setPublic();
        $response->setMaxAge(static::COOKIE_EXPIRE * 60);
        return $response;
    }

    /**
     * ping user
     *
     * @param Request $request
     * @return Response
     */
    public function imageAction(Request $request)
    {
        $service = $this->container->get('la_user.lib.lauserutils');
        $cookieKey = $service::COOKIE_LA_TOKEN;
        if ($request->cookies->has($cookieKey)) {
            $token = $request->cookies->get($cookieKey);
            try {
                $service = $this->container->get('la_user.lib.lauserutils');
                if (is_string($token)) {
                    $user = $service->getUserFromToken($service->encodeToken($token));
                    $crm = $user->getCrm();

                    if (!is_null($crm)) {
                        // lastactivity sur CRM
                        $crmManager = $this->container->get('la_crm.manager');
                        $event = new CrmEvent($user->getCrm());
                        $this->container->get('event_dispatcher')->dispatch(LaUserEvents::LAST_ACTIVITY_UPDATE, $event);
                        $crmManager->update($crm);
                        $this->container->get('la_user.visit.manager')->track($user->getId(), $token, $_SERVER);
                    } else {
                        $this->container->get('la_user.visit.manager')->track(0, $token, $_SERVER, false);
                    }
                }
            } catch (\Exception $e) {
                $this->container->get('la_user.visit.manager')->track(0, $token, $_SERVER, false);
            }
        }
        //  send image
        $headers = array('Content-Type' => 'image/png');
        $response = new Response($this->getBlankImageData(), 200, $headers);
        // cache
        $response->setPublic();
        $response->setMaxAge(static::COOKIE_EXPIRE * 60);

        return $response;
    }

    protected function getBlankImageData()
    {
        // GD : Image transparente 1x1 png
        $img = imagecreatetruecolor(1, 1);
        imagesavealpha($img, true);
        $color = imagecolorallocatealpha($img, 0x00, 0x00, 0x00, 127);
        imagefill($img, 0, 0, $color);
        ob_start();
        imagepng($img);
        $str = ob_get_contents();
        ob_end_clean();
        imagedestroy($img);
        return $str;
    }

}
