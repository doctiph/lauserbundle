<?php

namespace La\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseRegistrationController;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class RegistrationController extends BaseRegistrationController
{
    public function registerAction(Request $request)
    {
        if ($request->query->has('redirect_url')) {
            $this->container->get('la_user.lib.redirectutils')->addRedirectUrl($request->query->get('redirect_url'));
        }
        // validation Ajax
        if ($request->isXmlHttpRequest()) {
            $formFactory = $this->container->get('fos_user.registration.form.factory');
            $form = $formFactory->createForm();
            $validateService = $this->container->get('la_user.lib.lauservalidateutils');
            return $validateService->validAjax($form, $request);
        }

        return parent::registerAction($request);
    }

    public function confirmAction(Request $request, $token)
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);

        // Override to prevent exception when token is not found : instead render a twig.
        if (null === $user) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:invalidToken.html.' . $this->getEngine(), array());
        } else {
            return parent::confirmAction($request, $token);
        }
    }

    public function confirmedAction()
    {
        $request = $this->container->get('request');
        if ($request->query->has('redirect_url')) {
            $this->container->get('la_user.lib.redirectutils')->addRedirectUrl($request->query->get('redirect_url'));
        }
        return parent::confirmedAction();
    }

}
