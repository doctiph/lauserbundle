<?php

namespace La\UserBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\ResettingController as ParentResettingController;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use La\UserBundle\Entity\User;

class ResettingController extends ParentResettingController
{
    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    protected $redirectRoute = 'fos_user_resetting_request'; //fos_user_security_login

    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction(Request $request)
    {
        $email = $request->request->get('username');
        $session = $this->container->get('session');
        $session->getFlashBag()->clear();

        // check if valid Email
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $session->getFlashBag()->add('la_reset_password_error', 'la_user.reset_password.invalid');
        }

        /** @var $user UserInterface */
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);

        //array('invalid_username' => $email)
        if (null === $user) {
            $session->getFlashBag()->add('la_reset_password_error', 'la_user.reset_password.notfound');

            $response = new RedirectResponse($this->container->get('router')->generate($this->redirectRoute));
            return $response;
        }


        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $session->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));

        $this->container->get('la_user.mailer')->sendResettingEmailMessage($user);

        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        $session->getFlashBag()->add('la_reset_password_success', 'la_user.reset_password.check_email');

        $response = new RedirectResponse($this->container->get('router')->generate($this->redirectRoute));
        return $response;
    }

    public function resetAction(Request $request, $token)
    {

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);

        // If user decided to reset its password before registration confirmation : consider clicking reseting as email confirmation
        if($user instanceof User && null === $user->getCrm() && null != $user->getCrmTmp()){
            $this->container->get('event_dispatcher')->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, new GetResponseUserEvent($user, $request));
            $this->container->get('event_dispatcher')->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, new Response()));
            $user->setEnabled(true);
            $userManager->updateUser($user);
        }
        // Override to prevent exception when token is not found : instead render a twig.
        if (null === $user || ($user instanceof User && null === $user->getCrm() && null === $user->getCrmTmp())) {
            return $this->container->get('templating')->renderResponse('FOSUserBundle:Resetting:invalidToken.html.' . $this->getEngine(), array());
        }


        /**
         * @var RedirectUtil $redirectutils
         */
        $redirectutils = $this->container->get('la_user.lib.redirectutils');

        if ($request->isXmlHttpRequest()) {
            $formFactory = $this->container->get('fos_user.resetting.form.factory');
            $form = $formFactory->createForm();
            $validateService = $this->container->get('la_user.lib.lauservalidateutils');
            return $validateService->validAjax($form, $request);
        }

        if (!$request->isMethod('POST')) {
            $redirectutils->setRedirectAction($request);
        }
        return parent::resetAction($request, $token);
    }

}