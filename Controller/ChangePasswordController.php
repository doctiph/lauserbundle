<?php

namespace La\UserBundle\Controller;


use FOS\UserBundle\Controller\ChangePasswordController as BaseChangePasswordController;
use Symfony\Component\HttpFoundation\Request;
use La\UserBundle\Entity\User;

class ChangePasswordController extends BaseChangePasswordController
{
    public function changePasswordAction(Request $request)
    {
        // validation Ajax
        if ($request->isXmlHttpRequest()) {
            $formFactory = $this->container->get('fos_user.change_password.form.factory');
            $form = $formFactory->createForm();
            $validateService = $this->container->get('la_user.lib.lauservalidateutils');
            return $validateService->validAjax($form, $request);
        }

        // On edit, change "modified" date of user
        $user = $this->container->get('security.context')->getToken()->getUser();
        if($user instanceof User)
        {
            $modified = new \DateTime();
            $user->setModified($modified);
        }

        return parent::changePasswordAction($request);
    }
}
