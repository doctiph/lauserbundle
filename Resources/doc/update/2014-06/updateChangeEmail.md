# UPDATE CHANGE EMAIL

# Mettre à jour la table la_user__users

php app/console doctrine:schema:update --dump-sql
Puis lancez les commandes SQL ainsi générées dans PhpMyAdmin

# Template à ajouter :

Tous les templates dans "vendors/la/userbundle/La/UserBundle/Resources/views/ChangeEmail" sont surchargeables.
Référez vous à "vendors/la/userbundle/La/UserBundle/Resources/doc/customization.md"

# Template à modifier :

## src/La/[SiteName]UserBundle/Resources/views/Profile/edit_content.html.twig :

Rajoutez :

    {% if form.changeemail is defined %}
        <label class="control-label">Votre email :</label>
        {{ form_row(form.changeemail) }}
    {% endif %}

# Translations à rajouter :

## src/La/[SiteName]UserBundle/Resources/translations/forms.fr.yml :
- la_user.form.change_email.label : Label du bouton vers la page de changement d'email dans le profil
- la_user.form.change_email.return : Label du bouton pour revenir au profil depuis la page de changement d'email

## src/La/[SiteName]UserBundle/Resources/translations/validators.fr.yml :
- la_user.profile.change_email.already_used : Erreur lorsque le nouvel email est déjà utilisé
- la_user.profile.change_email.identical : Erreur lorsque le nouvel email est identique à l'ancien

## src/La/[SiteName]UserBundle/Resources/translations/messages.fr.yml :
- la_user.change_email.success : Message lorsque un email de confirmation a été envoyé
- la_user.change_email.complete : Message lorsque l'email a été changé avec succès