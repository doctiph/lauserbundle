# UPDATE LAST ACTIVITY

# Mettre à jour le SQL

    php app/console doctrine:schema:update --dump-sql

Puis lancer les commandes SQL ainsi générées dans PhpMyAdmin
Enfin, mettre à jour les users existants avec :

    UPDATE `lauser__crm` SET `lastActivity` = GREATEST(`lastActivity`, `modified`);