# Field Paths

## ROUTE API DE LA DOC


Tous les champs sont préfixés par le nom du formulaire :

* Profil : *la_user_api_profile*
* Inscription : *la_user_api_registration*

## Liste exhaustive des champs par défaut :

Pour récupérer les champs nécessaires à une méthode, il suffit d'accéder à /api/user en OPTIONS cela fournit la liste suivante :

    curl -v -X OPTIONS test.profile.programme-television.org/api/user :

    "POST": {
        "description": "Register a user",
        "parameters": {
            "username": {
                "field": "la_user_api_registration[username]",
                "required": true
            },
            "email": {
                "field": "la_user_api_registration[email]",
                "required": true
            },
            "password": {
                "first": {
                    "field": "la_user_api_registration[plainPassword][first]",
                    "required": true
                },
                "repeat": {
                    "field": "la_user_api_registration[plainPassword][second]",
                    "required": true
                }
            },
            "crmTmp": {
                "gcu": {
                    "field": "la_user_api_registration[crmTmp][gcu]",
                    "required": true
                }
            }
        }
    },
    "PUT": {
        "description": "Edit a user",
        "parameters": {
            "crm": {
                "firstName": {
                    "field": "la_user_api_profile[crm][firstName]",
                    "required": true
                },
                "lastName": {
                    "field": "la_user_api_profile[crm][lastName]",
                    "required": true
                },
                "gender": {
                    "field": "la_user_api_profile[crm][gender]",
                    "required": false
                },
                "birthdate": {
                    "field": "la_user_api_profile[crm][birthDate]",
                    "required": false
                },
                "address": {
                    "field": "la_user_api_profile[crm][address1]",
                    "required": false
                },
                "zipcode": {
                    "field": "la_user_api_profile[crm][zipCode]",
                    "required": false
                },
                "city": {
                    "field": "la_user_api_profile[crm][city]",
                    "required": false
                }
            }
        }
    }
