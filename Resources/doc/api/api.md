# REST API USER Documentation

# Authentification de l'application

Toutes les interactions nécessitent que l'application soit authentifiée en HTTP sur l'API
Ainsi, on passera les identifiants de l'application (ex:'mobile') et le mot de passe de l'application avec chaque requête.

    Exemple: curl -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' (...)


# Le token de session

Lorsqu'une requète de login est envoyée avec succès à l'API, elle renvoie (en plus des informations de l'utilisateur) un token de session.
Ce token est enregistré encrypté en base avec la méthode suivante :

    $encoded_token = sha1( sha1($sso_key) . '-' . sha1($token) );

Où $sso_key est une clef secrète défini dans les params du site (voir app/config/parameters.yml)

Le token réencodé avec l'algorithme ci-dessus devra être passé dans toutes les requètes concernant l'utilisateur authentifié.


# LOGIN

## POST /api/user/login

On log un user en passant un base64 encode de son username et de son mot de passe par sécurité.

Exemple :

    On a:
        - username: ladanalytics@gmail.com
        - password: Pasteque98;

    On enverra à l'api : base64_encode(ladanalytics@gmail.com:Pasteque98;)
    Soit : bGFkYW5hbHl0aWNzQGdtYWlsLmNvbTpQYXN0ZXF1ZTk4Ow==

D'où :

    curl -v -X POST -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' -d 'credentials=bGFkYW5hbHl0aWNzQGdtYWlsLmNvbTpQYXN0ZXF1ZTk4Ow==' http://test.profile.programme-television.org/api/user/login

Log et retourne les informations du user.

Le paramètre __-x__ du curl contient la méthode HTTP appelée.
Le paramètre __-d__ du curl passe les identifiants de l'utilisateur à authentifier. Il a pour syntaxe : base64_encode([username-ou-email]:[password])
Enfin le dernier paramètre est l'url que l'on curl.

Au succès l'api renvoie un JSON avec un [token] qui sera utilisé par l'api.

    {
        "user":{
            "id":10000006,
            "username":"toto1234",
            "email":"ladanalytics@gmail.com",
            "crm":{
                "id":10000006,
                "created":"2013-10-22T14:55:28+0200",
                "modified":"2013-10-22T14:55:28+0200",
                "firstName":"michel",
                "lastName":"dupont",
                "gender":2
            },
            "token":"KwqLnuSHs2NwJVPPywUP0qKl3lvTycGO-FKcRkGU6Nk"
        }
    }

en cas d'erreur le json comporte un code et un message d'erreur:

    {
        "user":{
            "error_code":30,
            "error_message":"invalid_credentials"
        }
    }

# LOGOUT

## POST api/user/logout

On logout un user en envoyant son token encodé en POST

    curl -v -X POST -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' -d 'token=ead0e8d0ffdbd7bc4ac0ea14ec06065d8c9a84a6' http://test.profile.programme-television.org/api/user/logout

# PROFIL

## Lecture du profil : GET /api/user/[encoded_token]

Renvoie les infos du user identifié par son token encodé

## Edition du profil : PUT /api/user/[encoded_token]

Modification des informations de l'user identifié par le token encodé.
Les champs sont ici fournis à titre d'exemple, cf. [[api_default_fields_paths.md]] pour plus d'info

    curl -v -X PUT -d -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' 'la_user_api_profile[crm][firstName]=michel&la_user_api_profile[crm][lastName]=dupont' http://test.profile.programme-television.org/api/users/449c5ac23aa0631396acf42aa907ddfb59c9140f



# INSCRIPTION

## POST /api/user/

Enregistrement d'un user.
Les champs sont ici fournis à titre d'exemple, cf. [[api_default_fields_paths.md]] pour plus d'info

    curl -v -X POST -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' -d 'la_user_api_registration[email]=abcd@gmail.com&la_user_api_registration[username]=toto123&la_user_api_registration[plainPassword][first]=arnaudlagardere&la_user_api_registration[plainPassword][second]=arnaudlagardere&la_user_api_registration[crmTmp][firstName]=arnaud&la_user_api_registration[crmTmp][lastName]=anonyme' http://test.profile.programme-television.org/api/user

# CONNEXION SOCIALE

La connexion via réseau social est identique sémantiquement à un register. On passera simplement en plus les paramètres suivants :
    - id ( obligatoire, id distant de l'utilisateur. ex: facebookid:1023452056 ou googleid:452145215214 )
    - access_token ( optionnel, oAuth token. ex: facebook_access_token:blabla123 ou google_access_token:blabla123 )

Ce qui donne, par exemple :
## POST /api/user

    curl -v -X POST -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' -d 'facebookid:1023452056&facebook_access_token:blabla123&la_user_api_registration[email]=abcd@gmail.com&la_user_api_registration[crmTmp][firstName]=arnaud&la_user_api_registration[crmTmp][lastName]=anonyme' http://test.profile.programme-television.org/api/user

# MOT DE PASSE PERDU

## POST /api/user/reset

Un seul paramètre à passer : l'email du compte dont on a perdu le mot de passe :

    curl -v -X POST -u 'mobile:56fb3198403591855b7a91955685dd29ea72f674' -d 'email:ladanalytics@gmail.com' http://test.profile.programme-television.org/api/user/reset


# Voir aussi

    http://restcookbook.com/HTTP%20Methods/put-vs-post/