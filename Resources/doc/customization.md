# Customization

Tout ou partie du LaUserBundle peut être surchargé pour s'adapter aux besoins du site.

## Translations
Tous les textes des formulaires, mails, erreurs sont fournis par défaut dans
src/La/UserBundle/Resources/translations/*.fr.yml

- forms.fr.yml : Labels des forms.
- validators.fr.yml : Messages d'erreur transmis par le validator.
- messages.fr.yml: Messages divers

ATTENTION
=========
Les textes commencent tous par le préixe [LA] qu'il faudra surcharger dans le bundle local :

- src/La/ElleUserBundle/Resources/translations/forms.fr.yml
- src/La/ElleUserBundle/Resources/translations/validators.fr.yml
- src/La/ElleUserBundle/Resources/translations/messages.fr.yml

## Templates
Tous les templates situés dans src/La/UserBundle/Resources/views/* sont overridable en les recréant dans
src/La/ElleUserBundle/Resources/views/* :

### Change Password
- La/UserBundle/Resources/views/ChangePassword/changePassword.html.twig [optionnel]

    Layout du Change Password

- La/UserBundle/Resources/views/ChangePassword/changePassword_content.html.twig

    Form du Change Password

### Change Email

- La/UserBundle/Resources/views/ChangeEmail/changeEmail.html.twig [optionnel]

    Layout du Change Email

- La/UserBundle/Resources/views/ChangeEmail/changeEmail_content.html.twig

    Form du Change Email

- La/UserBundle/Resources/views/ChangeEmail/email.txt.twig

    Email envoyé pour valider le nouvel email.

- La/UserBundle/Resources/views/ChangeEmail/invalidToken.html.twig

    Page affichée lorsque le token est invalide

### Template de Formulaires
- La/UserBundle/Resources/views/Form/Bootstrap/form_div_layout.html.twig [optionnel]

    Theme twig pour les forms du site. Par défaut bootstrap form est utilisé

### Edit
- La/UserBundle/Resources/views/Profile/edit.html.twig [optionnel]

    Layout de l'edit.

- La/UserBundle/Resources/views/Profile/edit_content.html.twig [optionnel]

    Form de l'édit

## Register
- La/UserBundle/Resources/views/Registration/checkEmail.html.twig

    Page affichée à l'envoie du mail de confirmation d'inscription.

- La/UserBundle/Resources/views/Registration/confirmed.html.twig

    Page de validation de l'inscription après le click sur le lien envoyé par mail

- La/UserBundle/Resources/views/Registration/email.txt.twig

    Email envoyé à l'inscription pour valider l'email.

- La/UserBundle/Resources/views/Registration/register.html.twig [optionnel]

    Form d'inscription.

- La/UserBundle/Resources/views/Registration/welcome.txt.twig

    Email envoyé après validation de l'inscription.

- La/UserBundle/Resources/views/Registration/invalidToken.html.twig

    Page affichée lorsque le token est invalide


## Resetting
- La/UserBundle/Resources/views/Resetting/request.html.twig [optionnel]

    Layout de la page de confirmation d'envoie du mail de mot de passe perdu.

- La/UserBundle/Resources/views/Resetting/checkEmail.html.twig [optionnel]

    Page précisant de vérifier sa boîte mail.

- La/UserBundle/Resources/views/Resetting/email.txt.twig

    Email envoyé demandant de cliquer sur un lien.

- La/UserBundle/Resources/views/Resetting/passwordAlreadyRequested.html.twig [optionnel]

    Page d'erreur si le mot de passe a déjà été demandé

- La/UserBundle/Resources/views/Resetting/request_content.html.twig

    Page de confirmation d'envoie du mail de mot de passe perdu.

- La/UserBundle/Resources/views/Resetting/reset.html.twig [optionnel]

    Layout du Form de ressetting (une fois cliqué sur le le lien du mail)

- La/UserBundle/Resources/views/Resetting/reset_content.html.twig

    Form de ressetting (une fois cliqué sur le le lien du mail)

- La/UserBundle/Resources/views/Resetting/invalidToken.html.twig

    Page affichée lorsque le token est invalide

## Login
- La/UserBundle/Resources/views/Security/login.html.twig

    Page/Form de login




