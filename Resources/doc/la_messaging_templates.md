# Templates de mail


## confirmation

### Description

Apres l'inscription, envoi d'un mail de confirmation

### Variables

- username : Username de l'utilisateur
- confirmationUrl    : Url de confirmation. Elle pointe vers www et non sur profile.


## welcome

### Description

Apres confirmation de l'inscription, envoi d'un mail de bienvenue

### Variables

- username : Username de l'utilisateur


## reset_password

### Description

Si l'utilisateur a oublié son mot de passe, on lui envoie un mail de confirmation

### Variables

- username : Username de l'utilisateur
- resettingUrl : Url de reset. Elle pointe vers www et non sur profile.


## reminder

### Description

Lorsqu'un utilisateur n'a pas confirmé son inscription (24/48h) on lui renvoie un mail de confirmation

### Variables

- username : Username de l'utilisateur
- confirmationUrl     : Url de confirmation. Elle pointe vers www et non sur profile.



