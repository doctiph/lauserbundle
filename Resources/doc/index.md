LaUserBundle
============

# Prerequis

Ce bundle requiert la/apibundle et la/messagingbundle pour fonctionner.

# Installation

### Nommage
Pour l'exemple de la documentation nous utiliserons le site www.elle.fr avec l'identification sur le domaine identification.elle.fr

## Rajouter le bundle dans composer.json :

    "repositories": [
#....
        {
            "type": "git",
            "url": "git@git01.in.ladtech.fr:LaUserBundle"
        }
    ],
#....
    "require": {
#....
        "friendsofsymfony/user-bundle": "~2.0@dev"
        "la/userbundle": "dev-master"
    },

# RegisterBundle
Enregistrer les bundles dans app/AppKernel.php si ce n'est pas déjà fait.

    public function registerBundles()
    {
        $bundles = array(
            // ....
            new FOS\UserBundle\FOSUserBundle(),
            new La\UserBundle\LaUserBundle(),
            // ....
        );
    }

# Import des fichiers de configurations communs.

### app/config/config.yml

    imports:
        # ....
        - { resource: @LaUserBundle/Resources/config/config.yml }

### app/config/routing.yml

    fos_user_profile:
        resource: "@LaUserBundle/Resources/config/routing/profile.yml"
        prefix: /profile

    fos_user_security:
        resource: "@FOSUserBundle/Resources/config/routing/security.xml"
        prefix: /login

    fos_user_register:
        resource: "@FOSUserBundle/Resources/config/routing/registration.xml"
        prefix: /register

    fos_user_resetting:
        resource: "@FOSUserBundle/Resources/config/routing/resetting.xml"
        prefix: /resetting

    fos_user_change_password:
        resource: "@FOSUserBundle/Resources/config/routing/change_password.xml"
        prefix: /profile

    la_user:
        resource: "@LaUserBundle/Resources/config/routing/routing.yml"
        prefix:   /

### app/config/routing_dev.yml :

    demo:
        resource: "@LaUserBundle/Resources/config/routing/demo.yml"
        prefix:   /


# Configuration


## Paramétrage de FOSUserBundle :

### app/config/config.yml :

    fos_user:
        user_class: La\[Elle]UserBundle\Entity\User
        from_email:
            address:        noreply@elle.fr
            sender_name:    elle.fr


## Paramétrage de LaUserBundle :

# Parameters

D'abord, vérifiez que vous possédez bien les fichiers suivants. Créez ceux qui vous manquent :
- /app/config/parameters.yml
- /app/config/parameters.yml.dist
- /app/config/parameters_dev.yml
- /app/config/parameters_demo.yml
- /app/config/parameters_pp.yml
- /app/config/parameters_prod.yml
- /app/config/parameters_test.yml

Il faut déclarer les parameters suivants dans parameters.yml :

    parameters:
        locale: fr
        secret: cc1b521c0720aa6c20c9a7d1a9becd8e7ae8e9ca
        sso_key: 08b095bd947cd653e7c34b7d74db85f6b27fc363
        test_user: [La valeur de votre choix]
        test_user_password: [La valeur de votre choix]
        test_user_email: [La valeur de votre choix]

Reportez les dans parameters.yml.dist :

    parameters:
        locale: fr
        secret: cc1b521c0720aa6c20c9a7d1a9becd8e7ae8e9ca
        sso_key: 08b095bd947cd653e7c34b7d74db85f6b27fc363
        test_user: [La valeur de votre choix]
        test_user_password: [La valeur de votre choix]
        test_user_email: [La valeur de votre choix]

Dans tous les autres fichiers parameters_<environnement>.yml, déclarez les parameters suivants :

    parameters:
        database_driver: pdo_mysql
        database_host: <host selon l'environnement>
        database_port: null
        database_name: <db selon l'environnement>
        database_user: <user selon l'environnement>
        database_password: <password selon l'environnement>

        mailer_transport: smtp
        mailer_host: <host selon l'environnement>
        mailer_user: <user selon l'environnement>
        mailer_password: <password selon l'environnement>

Attention ! Rappelez-vous que l'environnement de démo est celui en place sur la plateforme de test,
alors que l'environnement de test représente l'environnement sous lequel tournent les tests unitaires !

# Config

D'abord, vérifiez que vous possédez bien les fichiers suivants. Créez ceux qui vous manquent :
- /app/config/config.yml
- /app/config/config_dev.yml
- /app/config/config_demo.yml
- /app/config/config_pp.yml
- /app/config/config_prod.yml
- /app/config/config_test.yml

Au debut de chaque config_<environnement>.yml rajoutez l'import des parameters propres à l'environnement :

    imports:
        - { resource: config.yml }
        - { resource: parameters_<environnement>.yml }

Exemple dans config_pp.yml :

    imports:
        - { resource: config.yml }
        - { resource: parameters_pp.yml }

Puis rajoutez dans les fichiers suivants :

### app/config/config.yml

    framework:
        # ....
        translator:      { fallback: fr }

    doctrine:
        # ....
        orm:
            # ....
            resolve_target_entities:
                 La\UserBundle\Entity\CrmInterface: La\[Elle]UserBundle\Entity\Crm    #Crm propre au site

### app/config/la_config.yml

    la_user:
        sso_key:                            %sso_key%   # Required: clef d'identification utilisé par ezublish (voir code ez)
        communityfactory:                   ~   # Création des cookies CF, true par défaut
        secure_profile_edit:                false   # Remettre son mot de passe pour valider l'édition du compte
        html_redirect:                      true    # La redirection post login se fait en html (js redirect) ou php
        redirect_url:                       'http://www.[elle.fr]'    # Required: fallback de toutes les redirections
        crm_class:                          La\[Elle]UserBundle\Entity\Crm    # class de l'entité Crm
        test_user:                          %test_user% # Required  # Username du user de test
        test_user_password:                 %test_user_password%    # Password du user de test
        test_user_email:                    %test_user_email%       # Email du user de test
        firewall_name:                      secured_area  # Firewall principal (cf security.yml)
        form_types: ~                       # classes des form_types (voir après)

### app/config/config_dev.yml:

    la_user:
        redirect_url: 'http://dev.[identification.elle.fr]/demo'

    # Swiftmailer Configuration
    swiftmailer:
        spool:
            type: file
            path: "%kernel.root_dir%/spool"
        delivery_address: toto@toto.com

Modifiez :

    web_profiler:
        ...
        intercept_redirects: false

En :

    web_profiler:
        ...
        intercept_redirects: true

### app/config/config_demo.yml

   web_profiler:
       toolbar: true
       intercept_redirects: true

### app/AppKernel.php

Modifiez :

    if (in_array($this->getEnvironment(), array('dev', 'test'))) {

En :

    if (in_array($this->getEnvironment(), array('dev', 'demo', 'test'))) {

# UserBundle du site

Créer un bundle pour le site :

    $ php app/console generate:bundle --namespace=La/[Elle]UserBundle
    $ Configuration format (yml, xml, php, or annotation): yml
    $ Do you want to generate the whole directory structure [no]? yes

Enregistrer le bundles dans app/AppKernel.php si ce n'est pas fait automatiquement

    public function registerBundles()
    {
        $bundles = array(
            // ....
            new La\[Elle]UserBundle\La[Elle]UserBundle(),
            // ....
        );
    }

# Héritage du Bundle LaUserBundle

Dans la class La\[Elle]UserBundle\La[Elle]UserBundle créer la méthode :

    public function getParent()
    {
        return 'LaUserBundle';
    }

# Mise à jour des paramètres

### app/config/config.yml


    assetic:
        # ....
        bundles:        [La[Elle]UserBundle, LaUserBundle]


### app/config/routing.yml supprimer les lignes générées au début du fichier :

    la_[elle]_user:
        resource: "@La[Elle]UserBundle/Resources/config/routing.yml"
        prefix:   /


## Si ce n'est pas déjà fait, nettoyer toute référence à Acme, le bundle de démo

### src/ supprimer le dossier Acme :

    rm -rf src/Acme

### app/AppKernel.php supprimer la ligne

    $bundles[] = new Acme\DemoBundle\AcmeDemoBundle();

### app/config/routing_dev.yml supprimer les lignes

    _acme_demo:
        resource: "@AcmeDemoBundle/Resources/config/routing.yml"



# Les entités

## L'entité Crm
Créer une entité Crm dans le  bundle du site src/La/[Elle]UserBundle/Entity/
_(Fichier copiable depuis La/UserBundle/Resources/install/Entity/Crm.php)_

### src/La/[Elle]UserBundle/Entity/Crm.php

    <?php

    namespace La[Elle]UserBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use Symfony\Component\Validator\Constraints as Assert;
    use La\UserBundle\Entity\Crm as CrmAbstract;
    use La\UserBundle\Traits\Entity as Property;

    /**
     * @ORM\Table(
     *      name="lauser__crm",
     *      indexes={
     *          @ORM\Index(name="email_idx", columns={"email"})
     *      }
     * )
     * @ORM\Entity(repositoryClass="La\UserBundle\Entity\CrmRepository")
     */
    class Crm extends CrmAbstract
    {
        use Property\Id;
    }

### Les propriétés du Crm

Elles sont définies dans des traits (La\UserBundle\Traits\Entity\*)  et sont appelées selon les besoins
__ex__:

    <?php
    // ....
    class Crm extends CrmAbstract
    {
        use Property\Id;
        // ....
        use Property\Crm\Country;
        use Property\Crm\Address1;
        use Property\Crm\Address2;
        use Property\Crm\ZipCode;

        // ....
    }

Le nom, le prénom et l'origine sont définis dans la class parent La\UserBundle\Entity\Crm.


## L'entité User

Créer la surcharge de la class abstraite La\UserBundle\Entity\User
_(Fichier copiable depuis La/UserBundle/Resources/install/Entity/User.php)_

### src/La/[Elle]UserBundle/Entity/User.php

    <?php

    namespace La\[Elle]UserBundle\Entity;

    use Doctrine\ORM\Mapping as ORM;
    use La\UserBundle\Entity\User as BaseUser;
    use La\UserBundle\Traits\Entity as Property;

    /**
     * @ORM\Entity
     * @ORM\Table(
     *      name="lauser__users",
     *      indexes={
     *          @ORM\Index(name="confirmation_token_idx", columns={"confirmation_token"})
     *      }
     * )
     */
    class User extends BaseUser
    {

        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue(strategy="AUTO")
         */
        protected $id;

        use Property\Id;

    }

"use Property\Id" inclura les setter/getter

## Security/Firewall

### app/config/security.yml

    security:
        encoders:
            Symfony\Component\Security\Core\User\User: plaintext
            La\ApiBundle\Entity\Client: plaintext
            La\UserBundle\Entity\User: sha512

        role_hierarchy:
            ROLE_ADMIN:       ROLE_USER
            ROLE_SUPER_ADMIN: [ROLE_USER, ROLE_ADMIN, ROLE_ALLOWED_TO_SWITCH]

        providers:
            api_client:
                id: security.user.provider.api_client
            chain_provider:
                chain:
                    providers: [fos_userbundle]
            fos_userbundle:
                id: fos_user.user_provider.username_email
            in_memory:
                memory: ~

        firewalls:
            apidoc:
                pattern: ^/api/doc/
                http_basic:
                    realm: "Secured Api Area"
                    provider: api_client
            api:
                pattern: ^/api/
                http_basic:
                    realm: "Secured Api Area"
                    provider: api_client
                stateless:  true
            sso:
                pattern: ^/sso/
                anonymous: true
                stateless:  true
            user:
                pattern: ^/user/
                http_basic:
                    realm: "Secured User Area"
                    provider: in_memory
                logout:
                    success_handler: la_user.logout.handler
                anonymous:    true
                stateless:  true
            secured_area:
                pattern:    ^/
                form_login:
                    provider: fos_userbundle
                    csrf_provider: form.csrf_provider
                    login_path: /login
                    check_path: /login_check
                    target_path_parameter: redirect_url
                anonymous: true
                logout:
                    success_handler: la_user.logout.handler
            dev:
                pattern:  ^/(_(profiler|wdt)|css|images|js)/
                security: false

        access_control:
            - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
            - { path: ^/user$, role: ROLE_ADMIN }
            - { path: ^/api$, role: ROLE_API_CLIENT }


## Forms
Créer les forms dans src/La/[Elle]UserBundle/Form/Type/

## RegistrationFormType

### src/La/[Elle]UserBundle/Form/Type/RegistrationFormType.php
(La/UserBundle/Resources/install/Form/Type/RegistrationFormType.php)

    <?php

    namespace La\[Elle]UserBundle\Form\Type;

    use La\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
    use La\UserBundle\Traits\Form;
    use Symfony\Component\Form\FormBuilderInterface;

    class RegistrationFormType extends BaseRegistrationFormType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            parent::buildForm($builder, $options);
        }

    }

Si on veut un CGU non mappé par exemple on ajoutera le trait :

    class RegistrationFormType extends BaseRegistrationFormType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            // ....
            Form\Registration\Gcu::add($builder);
        }

    }

Ajouter la classe dans config.yml :

    la_user:
        # ....
        form_types:
            registration : La\[Elle]UserBundle\Form\Type\RegistrationFormType


### src/La/[Elle]UserBundle/Form/Type/RegistrationCrmType.php

    <?php
    namespace La\[Elle]UserBundle\Form\Type;

    use La\UserBundle\Form\Type\CrmType as BaseCrmType;
    use La\UserBundle\Traits\Form\Crm;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    class RegistrationCrmType extends BaseCrmType
    {

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            parent:: buildForm($builder, $options);
        }

        public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'La\[Elle]UserBundle\Entity\Crm',
            ));
        }

    }

Ajouter la classe dans config.yml :

    la_user:
        # ....
        form_types:
            # ....
            crm_registration : La\[Elle]UserBundle\Form\Type\RegistrationCrmType

### src/La/[Elle]UserBundle/Form/Type/ProfileFormType.php

    <?php
    namespace La\[Elle]UserBundle\Form\Type;

    use La\UserBundle\Form\Type\ProfileFormType as baseProfileFormType;
    use La\UserBundle\Traits\Form;
    use Symfony\Component\Form\FormBuilderInterface;

    class ProfileFormType extends baseProfileFormType
    {

        public function __construct($class, $conf, $crmType, $securityContext, $userManager)
        {
            parent::__construct($class);
            $this->crmType = $crmType;
            $this->conf = $conf;
            $this->securityContext = $securityContext;
            $this->userManager = $userManager;
        }

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            // User
            $token = $this->securityContext->getToken();
            $user = $token->getUser();

            Form\User\Username::add($builder, array(
                'disabled' => true,
                'read_only' => true,
                'data' => $this->userManager->getDisplayUsername($user),
                // on ne modifie pas le username
                'constraints' => array(),
            ));

            Form\User\Email::add($builder, array(
                'disabled' => true,
                'read_only' => true,
                // on ne modifie pas l'email
                'constraints' => array(),
            ));

            $builder
            ->add('changepw', 'button', array(
                'attr' => array(
                    'class' => 'link',
                    'href' => 'fos_user_change_password'),
                'label' => 'la_user.form.change_password.label'));

            $builder
                ->add('changeemail', 'button', array(
                    'attr' => array(
                        'class' => 'link',
                        'href' => 'la_user_profile_change_email'),
                    'label' => 'la_user.form.change_email.label'));

            // Crm
            $builder
                ->add('crm', $this->crmType, array(
                    'label' => null,
                ));

            Form\Submit::add($builder, array('label' => 'la_user.form.submit.profile'));
        }
    }


Ajouter la classe dans config.yml :

    la_user:
        # ....
        form_types:
            # ....
            profile : La\[Elle]UserBundle\Form\Type\ProfileFormType

### src/La/[Elle]UserBundle/Form/Type/ProfileCrmType.php

    <?php
    namespace La\[Elle]UserBundle\Form\Type;

    use La\UserBundle\Form\Type\CrmType as BaseCrmType;
    use La\UserBundle\Traits\Form\Crm;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\OptionsResolver\OptionsResolverInterface;

    class ProfileCrmType extends BaseCrmType
    {

        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            parent:: buildForm($builder, $options);
        }

        public function setDefaultOptions(OptionsResolverInterface $resolver)
        {
            $resolver->setDefaults(array(
                'data_class' => 'La\[Elle]UserBundle\Entity\Crm',
            ));
        }

    }

Il est possible d'ajouter les champs du Crm :

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent:: buildForm($builder, $options);

        Crm\Country::add($builder, array('required' => false));
        Crm\Address1::add($builder, array('required' => false));
        Crm\Address2::add($builder, array('required' => false));
        Crm\ZipCode::add($builder, array('required' => false));
        Crm\City::add($builder, array('required' => false));
        // ....



Ajouter la classe dans config.yml :

    la_user:
        # ....
        form_types:
            # ....
            crm_profile : La\[Elle]UserBundle\Form\Type\ProfileCrmType


## Initialisation

**Attention : Avant de lancer ces commandes, vérifiez que votre base de données est bien paramétrée dans app/config/parameters.yml !**

### Configurer Doctrine
Dans app/config/config.yml ajouter

    doctrine:
        dbal:
            # ....
            mapping_types:
                enum: string    # Doctrine ne connaît pas enum a priori

### Création des tables dans la base :

    php app/console doctrine:schema:update --dump-sql

Puis, Lancer les requêtes dans phpMyAdmin


### Création d'un User de test

    php app/console user:createtestuser (--superadmin)

# Création des assets

## Créer un lien symbolique des assets (css, js, images...) dans le dossier web/

    php app/console assets:install --symlink --relative

# Personnalisation

@see customization.md

---------------------------------------------------------------------------------------------------------------------------------

# API

## Documentation

Toute la documentation concernant l'API User se trouve sur la route suivante :

    /api/doc/user

## Ajout de form types spécifiques aux clients api

Commencez par créer vos form types spécifiques dans /src/[Elle]UserBundle/Api/<nomduclient>/Form/Type/
Par exemple, pour le client "mobile", je créerai mes form types dans /src/[Elle]UserBundle/Api/Mobile/Form/Type/

Les fichiers à créer sont identiques aux form type web, c'est à dire :
- RegistrationFormType
- RegistrationCrmType
- ProfileFormType
- ProfileCrmType

Déclarez les ensuite dans /src/[Elle]UserBundle/Resources/config/services.xml , là encore comme pour les form types web.
Par exemple, pour le client "mobile", j'aurai :

### /src/[Elle]UserBundle/Resources/config/services.xml

      <service id="la_user.api_registration.form.type.mobile" class="La\[Elle]UserBundle\Api\Mobile\Form\Type\RegistrationFormType">
        ...

Inspirez-vous des déclarations de services web.

Ensuite précisez dans la configuration lse form types le client va utiliser :

### config.yml

    api:
        clients:
            <nomduclient>:
                form_types:
                    registration :          la_user.api_registration.form.type.<nomduclient>
                    crm_registration :      la_user.api_crm_registration.form.type.<nomduclient>
                    profile :               la_user.api_profile.form.type.<nomduclient>
                    crm_profile :           la_user.api_crm_profile.form.type.<nomduclient>

Par exemple, pour le client mobile j'aurai :

    api:
        clients:
            mobile:
                form_types:
                    registration :          la_user.api_registration.form.type.mobile

Ou alors, prenez les formtypes api par défaut :

    api:
        clients:
            <nomduclient>:
                form_types : ~