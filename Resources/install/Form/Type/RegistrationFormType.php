<?php

namespace La\SITENAMEUserBundle\Form\Type;

use La\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use La\UserBundle\Traits\Form;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends BaseRegistrationFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

}