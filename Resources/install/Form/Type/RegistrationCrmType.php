<?php

namespace La\SITENAMEUserBundle\Form\Type;

use La\UserBundle\Form\Type\CrmType as BaseCrmType;
use La\UserBundle\Traits\Form\Crm;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegistrationCrmType extends BaseCrmType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent:: buildForm($builder, $options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'La\SITENAMEUserBundle\Entity\Crm',
        ));
    }

}