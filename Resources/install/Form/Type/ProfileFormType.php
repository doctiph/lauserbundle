<?php
namespace La\SITENAMEUserBundle\Form\Type;

use La\UserBundle\Form\Type\ProfileFormType as baseProfileFormType;
use La\UserBundle\Traits\Form;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends baseProfileFormType
{

    public function __construct($class, $conf, $crmType, $securityContext, $userManager)
    {
        parent::__construct($class);
        $this->crmType = $crmType;
        $this->conf = $conf;
        $this->securityContext = $securityContext;
        $this->userManager = $userManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // User
        $token = $this->securityContext->getToken();
        $user = $token->getUser();

        Form\User\Username::add($builder, array(
            'disabled' => true,
            'read_only' => true,
            'data' => $this->userManager->getDisplayUsername($user),
            // on ne modifie pas le username
            'constraints' => array(),
        ));

        Form\User\Email::add($builder, array(
            'disabled' => true,
            'read_only' => true,
            // on ne modifie pas l'email
            'constraints' => array(),
        ));

        $builder
            ->add('changepw', 'button', array(
                'attr' => array(
                    'class' => 'link',
                    'href' => 'fos_user_change_password'),
                'label' => 'la_user.form.change_password.label'));

        $builder
            ->add('changeemail', 'button', array(
                'attr' => array(
                    'class' => 'link',
                    'href' => 'la_user_profile_change_email'),
                'label' => 'la_user.form.change_email.label'));
        // Crm
        $builder
            ->add('crm', $this->crmType, array(
                'label' => null,
            ));

        Form\Submit::add($builder, array('label' => 'la_user.form.submit.profile'));
    }
}