<?php

namespace La\SITENAMEUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use La\UserBundle\Entity\User as BaseUser;
use La\UserBundle\Traits\Entity as Property;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="lauser__users",
 *      indexes={
 * @ORM\Index(name="confirmation_token_idx", columns={"confirmation_token"})
 *      }
 * )
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    use Property\Id;

}