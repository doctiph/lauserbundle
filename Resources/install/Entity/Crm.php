<?php

namespace La\SITENAMEUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use La\UserBundle\Entity\Crm as CrmAbstract;
use La\UserBundle\Traits\Entity as Property;

/**
 * @ORM\Table(
 *      name="lauser__crm",
 *      indexes={
 * @ORM\Index(name="email_idx", columns={"email"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="La\UserBundle\Entity\CrmRepository")
 */
class Crm extends CrmAbstract
{
    use Property\Id;
}