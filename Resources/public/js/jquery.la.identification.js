/**
 * IDENTIFICATION
 *
 */


(function ($) {

    $.la = $.la || {};

    $.extend(true, $.la,
        {
            modules: {
                la: {
                    modules: {
                        identification: {
                            active: true
                        }
                    }
                }
            },
            identification: {
                prefixUrl: "",
                prefixHost: "",
                getUrl: function () {
                    throw "URL is not defined.";
                },
                getPrefixDomain: function () {

                },
                closeModal: function (modale_id) {
                    if (null !== typeof modale_id) {
                        $('#' + modale_id).modal('hide');
                    }
                },
                displayModal: function (modale_id) {
                    if (null !== typeof modale_id) {
                        $('#' + modale_id).modal('show');
                    }

                }
            }
        });
})(jQuery);