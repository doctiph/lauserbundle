/**
 *
 * jQuery.lauser.validate()
 *
 */

(function ($) {

    $.lauser = $.lauser || {};

    $.extend(true, $.lauser,
        {
            validate: function (form, page, callback, callbackError) {
                $.ajax({
                        url: form.attr('action'),
                        type: form.attr('method'),
                        data: form.serialize(),
                        success: function (res) {
                            var onErrorFields;

                            res = JSON.parse(res);
                            $(".error span.help-inline", form).remove();
                            $(".error", form).removeClass('error');

                            if (page === null) {
                                page = form;
                            }

                            $('input,select', page).
                                each(function () {
                                    var name = typeof $(this).attr('name') === "string" ? $(this).attr('name') : "",
                                        propertyPath = name,
                                        children,
                                        i;

                                    if ($(this).attr('disabled') === 'disabled') {
                                        return;
                                    }

                                    propertyPath = propertyPath.replace(form.attr('name'), "");
                                    children = propertyPath.match(/\[[a-zA-Z0-9\-_\.]+\]/g);

                                    if (children === null) {
                                        return;
                                    }

                                    for (i = 0; i < children.length; i += 1) {
                                        propertyPath = propertyPath.replace(children[i], 'children'.concat(children[i], '.'));
                                    }
                                    propertyPath = propertyPath.concat('data');

                                    if (res.hasOwnProperty(propertyPath)) {
                                        $(this).closest(".control-group").addClass('error');
                                        if ($('span.help-inline', $(this).closest(".controls")).length === 0) {
                                            $(this).closest(".controls").append(
                                                '<span class="help-inline">' + res[propertyPath].message + '</span>'
                                            );
                                        }
                                    }
//                                    $(this).focusout(function () {
//                                        $(this).next().fadeOut(100);
//                                    });
                                });

                            onErrorFields = $(".error", form);
                            if (onErrorFields.length === 0) {
                                callback();
                            } else {
                                $('input', onErrorFields.first()).focus();
                                if (typeof callbackError !== null) {
                                    callbackError();
                                }
                            }
                        }}
                );
                return false;
            },

            bindSubmit: function (form, callbackError) {
                var submit = jQuery('button[type=submit]', form);
                submit.click(function () {
                    jQuery.lauser.validate(form, null, function () {
                        form.submit();
                    }, function () {
                        if (typeof callbackError !== null) {
                            callbackError();
                        }
                    });
                    return false;
                })
            }
        }
    );
})(jQuery);