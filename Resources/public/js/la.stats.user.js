/*global document, window, console, XMLHttpRequest, JSON, US, jQuery, Highcharts, alert */

US.monthlyArray = [];    // 0:fb / 1:g+ / 2:sub / 3 : users
US.monthlyNLArray = [];
US.monthlyNLUnsubArray = [];
US.totalsDataSet = {};    // 0:fb / 1:g+ / 2:sub / 3 : users
US.yearlyArray = [];    // 0:fb / 1:g+ / 2:sub
US.yearlyNLArray = [];
US.yearlyNLUnsubArray = [];

US.monthChartsData = {
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { day: '%e/%m' },
        labels: { step: 1 }
    },
    series: [
        {
            name: 'Emails (sans Compte)',
            visible: true,
            data: [],
            stack: 'users'
        },
        {
            name: 'Comptes clients',
            visible: true,
            data: [],
            stack: 'users'
        },
        {
            name: 'Facebook',
            visible: true,
            data: [],
            stack: 'social'
        },
        {
            name: 'Google+',
            visible: true,
            data: [],
            stack: 'social'
        }
    ]
};

US.monthlyNLChartsData = {
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { day: '%e/%m' },
        labels: {
            minorTickLength: 10
        }
    },
    series: []
};

US.monthlyNLUnsubChartsData = {
    xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: { day: '%e/%m' },
        labels: {
            minorTickLength: 10
        }
    },
    series: []
};

US.yearChartsData = {
    xAxis: {
        categories: []
    },
    series: [
        {
            name: 'Emails (sans Compte)',
            visible: true,
            data: [],
            stack: 'users'
        },
        {
            name: 'Comptes clients',
            visible: true,
            data: [],
            stack: 'users'
        },
        {
            name: 'Facebook',
            visible: true,
            data: [],
            stack: 'social'
        },
        {
            name: 'Google+',
            visible: true,
            data: [],
            stack: 'social'
        }
    ]
};

US.yearlyNLChartsData = {
    xAxis: {
        categories: []
    },
    series: []
};

US.yearlyNLUnsubChartsData = {
    xAxis: {
        categories: []
    },
    series: []
};

US.drawChartsFromData = function drawChartsFromData(chartsData) {
    "use strict";
    var length = chartsData.length,
        i,
        currentDataSet = null;

    if (chartsData && length > 0) {

        //extract datasets to treat them separately : month, totals, yearly
        for (i = 0; i < length; i += 1) {

            currentDataSet = chartsData[i];
            switch (currentDataSet.label) {

                //  MONTHLY
                case 'monthlyCrm':
                    US.monthlyArray[0] = currentDataSet;
                    break;
                case 'monthlyUsers':
                    US.monthlyArray[1] = currentDataSet;
                    break;
                case 'monthlyFacebookUsers':
                    US.monthlyArray[2] = currentDataSet;
                    break;
                case 'monthlyGoogleUsers':
                    US.monthlyArray[3] = currentDataSet;
                    break;

                // TOTALS
                case 'totalCrm':
                    US.totalsDataSet.totalCrm = currentDataSet.stats;
                    break;
                case 'totalFacebookUsers':
                    US.totalsDataSet.totalFacebookUsers = currentDataSet.stats;
                    break;
                case 'totalGoogleUsers':
                    US.totalsDataSet.totalGoogleUsers = currentDataSet.stats;
                    break;
                case 'totalSubscriptions':
                    US.totalsDataSet.totalSubscriptions = currentDataSet.stats;
                    break;
                case 'totalUsers':
                    US.totalsDataSet.totalUsers = currentDataSet.stats;
                    break;
                case 'totalUniqueSubscriber':
                    US.totalsDataSet.totalUniqueSubscriber = currentDataSet.stats;
                    break;


                //  YEARLY
                case 'yearlyCrm':
                    US.yearlyArray[0] = currentDataSet;
                    break;
                case 'yearlyUsers':
                    US.yearlyArray[1] = currentDataSet;
                    break;
                case 'yearlyFacebookUsers':
                    US.yearlyArray[2] = currentDataSet;
                    break;
                case 'yearlyGoogleUsers':
                    US.yearlyArray[3] = currentDataSet;
                    break;

                //Subscriptions
                case 'monthlySubscriptions':
                    US.monthlyNLArray = currentDataSet;
                    break;
                case 'yearlySubscriptions':
                    US.yearlyNLArray = currentDataSet;
                    break;

                // Unsubscriptions
                case 'monthlyUnsubscriptions':
                    US.monthlyNLUnsubArray = currentDataSet;
                    break;
                case 'yearlyUnsubscriptions':
                    US.yearlyNLUnsubArray = currentDataSet;
                    break;
            }

        }

        for (i in US.monthlyArray[0].stats) {
            if (US.monthlyArray[0].stats.hasOwnProperty(i)) {
                US.monthlyArray[0].stats[i] = US.monthlyArray[0].stats[i] - US.monthlyArray[1].stats[i];
            }
        }
        for (i in US.yearlyArray[0].stats) {
            if (US.yearlyArray[0].stats.hasOwnProperty(i)) {
                US.yearlyArray[0].stats[i] = US.yearlyArray[0].stats[i] - US.yearlyArray[1].stats[i];
            }
        }


        US.formatBarsStats(US.monthlyArray, US.monthChartsData, true);
        US.formatBarsStats(US.yearlyArray, US.yearChartsData, false);

        US.formatLineStats(US.monthlyNLArray, US.monthlyNLChartsData, true);
        US.formatLineStats(US.yearlyNLArray, US.yearlyNLChartsData, false);

        US.formatLineStats(US.monthlyNLUnsubArray, US.monthlyNLUnsubChartsData, true);
        US.formatLineStats(US.yearlyNLUnsubArray, US.yearlyNLUnsubChartsData, false);

        US.buildTotalsPage(US.totalsDataSet);

        jQuery(function () {
            jQuery(document).trigger("chartDataReady");
        });

    } else {

        //  trigger error event -> fallback
        jQuery(function () {
            jQuery(document).trigger("chartDataError");
        });

    }

};

US.buildTotalsPage = function buildTotalsPage(baseData) {
    "use strict";
    var i,
        nlSubscriptions = baseData.totalSubscriptions,
        len = nlSubscriptions.length,
        htmlArray = [];

    //  total crm
    htmlArray.push('<div class="totalBlock" id="totalBlockCRM"><span class="title">Total Emails</span><span class="subtitle">(Comptes clients compris)</span><span class="number">');
    htmlArray.push(US.formatThousands(baseData.totalCrm));
    htmlArray.push('</span></div>');
    //  total users
    htmlArray.push('<div class="totalBlock" id="totalBlockUsers"><span class="title">Total comptes clients</span><span class="subtitle">(Facebook et Google+ compris)</span><span class="number">');
    htmlArray.push(US.formatThousands(baseData.totalUsers));
    htmlArray.push('</span></div>');
    //  total FB
    htmlArray.push('<div class="totalBlock" id="totalBlockFB"><span class="title">Total Facebook</span><span class="subtitle">(Au moins une connexion)</span><span class="number">');
    htmlArray.push(US.formatThousands(baseData.totalFacebookUsers));
    htmlArray.push('</span></div>');
    //  total GPlus
    htmlArray.push('<div class="totalBlock" id="totalBlockGPlus"><span class="title">Total Google+</span><span class="subtitle">(Au moins une connexion)</span><span class="number">');
    htmlArray.push(US.formatThousands(baseData.totalGoogleUsers));
    htmlArray.push('</span></div>');

    //  liste NewsLetters
    htmlArray.push('<span class="titleNewsLetters">Inscrits Newsletters et emailings</span><ul>');

    // Total Newsletters uniques
    htmlArray.push('<li><span class="nlName">Total inscrits uniques Newsletters et emailings</span>');
    htmlArray.push('<span class="nlNumber">');
    htmlArray.push(US.formatThousands(baseData.totalUniqueSubscriber));
    htmlArray.push('</span></li></ul><ul>');

    for (i = 0; i < len; i += 1) {
        htmlArray.push('<li><span class="nlName">');
        htmlArray.push(nlSubscriptions[i].name);
        htmlArray.push('</span><span class="nlNumber">');
        htmlArray.push(US.formatThousands(nlSubscriptions[i].value));
        htmlArray.push('</span></li>');
    }

    htmlArray.push('</ul>');

    jQuery('#totalsPage').html(htmlArray.join(''));
};

US.onChartDataReady = function onChartDataReady(chartInstanceSetting) {
    "use strict";

    US.monthlyStatsChart = jQuery('#monthlyStatsContainer').highcharts({
        chart: { type: 'column' },
        title: { text: 'Nouveaux utilisateurs : 30 derniers jours' },
        //  jours du mois
        xAxis: US.monthChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'utilisateurs' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.monthChartsData.series
    });

    US.yearlyStatsChart = jQuery('#yearlyStatsContainer').highcharts({
        chart: { type: 'column' },
        title: { text: 'Nouveaux Utilisateurs : 12 dernier mois' },
        //  jours du mois
        xAxis: US.yearChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'nouveaux utilisateurs' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.yearChartsData.series
    });

    US.monthlyNLStatsChart = jQuery('#monthlyNewsLetterContainer').highcharts({
        title: { text: 'Nouveaux abonnés NewsLetters : <br/>30 derniers jours' },
        //  jours du mois
        xAxis: US.monthlyNLChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'abonnés' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.monthlyNLChartsData.series
    });

    US.yearlyNLStatsChart = jQuery('#yearlyNewsLetterContainer').highcharts({
        /*chart: { type: 'column' },*/
        title: { text: 'Nouveaux abonnés NewsLetters : <br/>12 derniers mois' },
        //  jours du mois
        xAxis: US.yearlyNLChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'abonnés' }
        },

        legend: {
            align: 'center',
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<span style="font-size:14px;text-align:center;width:100%;">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.yearlyNLChartsData.series
    });

    US.monthlyNLUnsubStatsChart = jQuery('#monthlyNewsLetterUnsubContainer').highcharts({
        title: { text: 'Nouveaux désabonnés NewsLetters : <br/>30 derniers jours' },
        //  jours du mois
        xAxis: US.monthlyNLUnsubChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'désabonnés' }
        },
        legend: chartInstanceSetting.legend,
        tooltip: chartInstanceSetting.tooltip,
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.monthlyNLUnsubChartsData.series
    });

    US.yearlyNLUnsubStatsChart = jQuery('#yearlyNewsLetterUnsubContainer').highcharts({
        /*chart: { type: 'column' },*/
        title: { text: 'Nouveaux désabonnés NewsLetters : <br/>12 derniers mois' },
        //  jours du mois
        xAxis: US.yearlyNLUnsubChartsData.xAxis,
        yAxis: {
            min: 0,
            title: { text: 'désabonnés' }
        },

        legend: {
            align: 'center',
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: true
        },
        tooltip: {
            headerFormat: '<span style="font-size:14px;text-align:center;width:100%;">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },
        series: US.yearlyNLUnsubChartsData.series
    });
}
