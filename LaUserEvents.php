<?php

namespace La\UserBundle;

/**
 * Contains all La-specific events thrown in the LaUserBundle
 */
final class LaUserEvents
{
    /**
     * The LAST_ACTIVITY_UPDATE event occurs whenever the user interacts with the profile.
     * Activity is a bit different from modified, since user trigger activity even when not modifying anything.
     *
     * The event listener method receives a La\UserBundle\Event\CrmEvent instance.
     */
    const LAST_ACTIVITY_UPDATE = 'la_user.crm.last_activity.update';

    /**
     * The CHANGE_EMAIL_INITIALIZE event occurs whenever the user access the form to change his email.
     *
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const CHANGE_EMAIL_INITIALIZE = 'la_user.change_email.edit.initialize';

    /**
     * The CHANGE_EMAIL_SUCCESS event occurs whenever the user successfully asked for a new email.
     *
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const CHANGE_EMAIL_SUCCESS = 'la_user.change_email.edit.success';

    /**
     * The CHANGE_EMAIL_COMPLETED event occurs whenever the user has confirmed his new email.
     *
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const CHANGE_EMAIL_COMPLETED = 'la_user.change_email.edit.completed';


    /**
     * The ON_REGISTRATION_CRM_ACTIVATION event occurs whenever a crm is activated.
     *
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const ON_REGISTRATION_CRM_ACTIVATION = "la.user.on_crm_activate";


    /**
     * The ON_REGISTRATION_CRM_MERGE event occurs whenever a crm is merged in another existant one.
     *
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     */
    const ON_REGISTRATION_CRM_MERGE = "la.user.on_crm_merge";

    /**
     * The API_FORMAT_USER_REQUEST event occurs when a user must be formatted on api request.
     *
     * The event listener method receives a Symfony\Component\EventDispatcher\GenericEvent instance.
     */
    const API_FORMAT_USER_REQUEST = "api.format.user";


    /**
     * The API_BEFORE_REGISTRATION event occurs before a registration through the api, to allow specific registration behaviours.
     * The propagation must be stopped if you catch this event, or it will trigger the regular registration behaviour.
     *
     * The event listener method receives a La\ApiBundle\Event\ApiResponseEvent
     */
    const API_BEFORE_REGISTRATION = "api.before.register";

    /**
     * The USER_DISPLAY_USERNAME_COMPUTE event occurs when a display username is computed.
     *
     * The event listener method receives a La\UserBundle\Event\UserEvent
     */
    const USER_DISPLAY_USERNAME_COMPUTE = "la.user.entity.display_username";


    /**
     * The USER_DELETE_REQUEST event occurs when a user deletion is requested
     *
     * The event listener method receives a La\UserBundle\Event\UserEvent
     */
    const USER_DELETE_REQUEST = "la.user.delete.request";

}
