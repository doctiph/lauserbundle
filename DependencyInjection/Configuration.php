<?php

namespace La\UserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/Doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('la_user');

        $rootNode->
            children()
                ->scalarNode('sso_key')
                    ->isRequired()
                ->end()
                ->scalarNode('encrypt_text_key')
                    ->isRequired()
                ->end()
                ->booleanNode('communityfactory')
                    ->defaultTrue()
                ->end()
                ->booleanNode('secure_profile_edit')
                    ->defaultTrue()
                ->end()
                ->booleanNode('html_redirect')
                    ->defaultFalse()
                ->end()
                ->scalarNode('redirect_url')
                    ->isRequired()
                ->end()
                ->scalarNode('crm_class')
                    ->isRequired()
                ->end()
                ->arrayNode('additionnal_data')
                    ->prototype('scalar')->end()
                ->end()
                ->scalarNode('test_user')
                    ->isRequired()
                ->end()
                ->scalarNode('test_user_password')
                    ->isRequired()
                ->end()
                ->scalarNode('test_user_email')
                    ->isRequired()
                ->end()
                ->scalarNode('firewall_name')
                    ->isRequired()
                ->end()

                ->arrayNode('form_types')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('registration')
                            ->defaultValue('La\UserBundle\Form\Type\RegistrationFormType')
                        ->end()
                        ->scalarNode('profile')
                            ->defaultValue('La\UserBundle\Form\Type\ProfileFormType')
                        ->end()
                        ->scalarNode('crm_registration')
                            ->defaultValue('La\UserBundle\Form\Type\RegistrationCrmType')
                        ->end()
                        ->scalarNode('crm_profile')
                            ->defaultValue('La\UserBundle\Form\Type\ProfileCrmType')
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('api')
                    ->children()
                        ->arrayNode('clients')
                        ->performNoDeepMerging()    // @todo Trouver meilleure solution pour surcharger cette config en test ou dev
                        ->prototype('array')
                            ->children()
                                ->booleanNode('quick_register')
                                    ->defaultValue(false)
                                ->end()
                                ->arrayNode('form_types')
                                    ->addDefaultsIfNotSet()
                                        ->children()
                                            ->scalarNode('registration')
                                                ->defaultValue('la_user.api_registration.form.type')
                                            ->end()
                                            ->scalarNode('crm_registration')
                                                ->defaultValue('la_user.api_crm_registration.form.type')
                                            ->end()
                                            ->scalarNode('profile')
                                                ->defaultValue('la_user.api_profile.form.type')
                                            ->end()
                                            ->scalarNode('crm_profile')
                                                ->defaultValue('la_user.api_crm_profile.form.type')
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                     ->end()
                ->end()
            ->end()
        ->end()
        ;


        return $treeBuilder;
    }
}