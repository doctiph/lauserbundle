<?php

namespace La\UserBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/Doc/current/cookbook/bundles/extension.html}
 */
class LaUserExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);


        $container->setParameter('la_user', $config);


        /*
         * Services parameters
         */

        $container->setParameter('la_user.registration.form.type.class', $config['form_types']['registration']);
        $container->setParameter('la_user.profile.form.type.class', $config['form_types']['profile']);
        $container->setParameter('la_user.crm.registration.form.type.class', $config['form_types']['crm_registration']);
        $container->setParameter('la_user.crm.profile.form.type.class', $config['form_types']['crm_profile']);
        $container->setParameter('la_user.firewall_name', $config['firewall_name']);

        $container->setParameter('la_user.crm.class', $config['crm_class']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));

        $container->setParameter('la_user.api.doc.root.dir', __DIR__ . '/../Resources/doc/api/resources');


        $loader->load('services/services.xml');
        $loader->load('services/command.xml');
        $loader->load('services/form.xml');
        $loader->load('services/listener.xml');
        $loader->load('services/validate.xml');
        $loader->load('services/api.xml');
        $loader->load('services/admin.xml');
    }
}