<?php
namespace La\UserBundle\Lib;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class LaUserValidateUtil
{
    protected $validator;
    protected $formFactory;
    protected $templating;

    public function __construct($validator, $formFactory, $templating)
    {
        $this->validator = $validator;
        $this->formFactory = $formFactory;
        $this->templating = $templating;
    }

    /**
     * Validate form whith XHR
     *
     * @param $form
     * @param Request $request
     * @return Response
     */
    public function validAjax($form, Request $request)
    {
        $errors = array();
        if ('POST' === $request->getMethod()) {

            $form->submit($request);
            // invalid form ?!
            if (!$form->isValid()) {
                $errorList = $this->validator->validate($form);

                foreach ($errorList as $error) {
                    $propertyPath = $error->getPropertyPath();
                    $invalidValue = $error->getInvalidValue();

                    if (preg_match_all('#data\.(.*)$#', $propertyPath, $matches)) {
                        $propertyPath = str_replace($matches[0][0], 'children[' . $matches[1][0] . ']', $propertyPath);
                    }

                    if (is_array($invalidValue)) {
                        foreach ($invalidValue as $key => $val) {
                            $errors[$propertyPath . '.children[' . $key . '].data'] = array(
                                'message' => $error->getMessage(),
                                'invalidValue' => $val
                            );
                        }
                    } else {
                        if(!preg_match('#.data$#', $propertyPath)){
                            $propertyPath .= '.data';
                        }
                        $errors[$propertyPath] = array(
                            'message' => $error->getMessage(),
                            'invalidValue' => $error->getInvalidValue(),
                        );
                    }
                }
            }
        }
        header('Content-type: application/json');

        return new Response(json_encode($errors));

    }
}