<?php
namespace La\UserBundle\Lib;

use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use La\UserBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Event\UserEvent as FOSUserEvent;
use FOS\UserBundle\Model\UserManager;
use FOS\UserBundle\Util\TokenGenerator;
use La\UserBundle\LaUserEvents;
use La\UserBundle\Event\UserEvent;
use La\UserBundle\Event\CrmEvent;
use La\UserBundle\Entity\Token;
use La\UserBundle\Entity\UserInterface;
use La\UserBundle\Entity\CrmInterface;
use La\Lib\Encrypt\Model\Cipher;

class LaUserUtil
{

    const COOKIE_LA_ID = 'lauser_id';
    const COOKIE_LA_USERNAME = 'lauser_username';
    const COOKIE_LA_DISPLAY_USERNAME = 'lauser_display_username';
    const COOKIE_LA_FIRST_NAME = 'lauser_first_name';
    const COOKIE_LA_LAST_NAME = 'lauser_last_name';
    const COOKIE_LA_TOKEN = 'lauser_token';
    const COOKIE_LA_ENCRYPTED = 'lauser_encrypted';

    const COOKIE_ENCRYPT_KEY = 'ZujdBUOwssa';
    const COOKIE_MCRYPT_IV = 'hj@!diu8';

    const COOKIE_CF_USER_ID = 'CF_user_id';
    const COOKIE_CF_USER_PASS = 'CF_user_pass';
    const COOKIE_CF_USER_LOGIN = 'CF_user_login';

    const COOKIE_MD_ID = 'md_id';
    const COOKIE_MD_PASS = 'md_passs';
    const COOKIE_MD_USER = 'md_user';

    const COOKIE_LA_TRACKING_COOKIE_NAME = 'lauser_utv';
    const LIFETIME_PERSISTENT_COOKIE = 3; // years

    protected $em;
    protected $conf;
    protected $userManager;
    protected $crmClass;
    protected $session;
    protected $securityContext;
    protected $cookies;


    public function __construct(EntityManager $em, array  $conf, UserManager $userManager, $crmClass, $session, $templating, SecurityContextInterface $securityContext, EventDispatcherInterface $dispatcher)
    {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->conf = $conf;
        $this->userManager = $userManager;
        $this->crmClass = $crmClass;
        $this->session = $session;
        $this->templating = $templating;
        $this->securityContext = $securityContext;

        $this->cookies = array(
            static::COOKIE_CF_USER_ID,
            static::COOKIE_CF_USER_LOGIN,
            static::COOKIE_CF_USER_PASS,
            static::COOKIE_MD_ID,
            static::COOKIE_MD_PASS,
            static::COOKIE_MD_USER,
            static::COOKIE_LA_ID,
            static::COOKIE_LA_USERNAME,
            static::COOKIE_LA_DISPLAY_USERNAME,
            static::COOKIE_LA_FIRST_NAME,
            static::COOKIE_LA_LAST_NAME,
            static::COOKIE_LA_TOKEN,
            static::COOKIE_LA_TRACKING_COOKIE_NAME,
            static::COOKIE_LA_ENCRYPTED
        );
    }

    /**
     * Get name of token cookie
     *
     * @return string
     */
    public function getTokenCookieName()
    {
        return static::COOKIE_LA_TOKEN;
    }

    /**
     * Get global conf
     *
     * @return array
     */
    public function getConf()
    {
        return $this->conf;
    }


    /**
     * Send on onSecurityInteractiveLogin
     *
     * @param Event $event
     */
    public function createCookiesFromEvent(Event $event)
    {
        if ($event instanceof FOSUserEvent) {
            $user = $event->getUser();
        } else {
            $user = $event->getAuthenticationToken()->getUser();
        }
        $this->createCookies($user, $event->getRequest());
    }

    /**
     * Create all cookies for current domain
     *
     * @param UserInterface $user
     * @param Request $request
     * @throws \Exception
     */
    public function createCookies(UserInterface $user, Request $request)
    {
        $this->unsetAllCookies($request);

        $crm = $user->getCrm();
        if (!is_object($crm)) {
            throw new \Exception(sprintf("Fatal : No Crm Object for User %d.", $user->getId()));
        }

        $domain = $this->getDomain($request->getHttpHost());

        try {
            $cookies = array();
            $cookies[] = new Cookie(static::COOKIE_LA_ID, $user->getId(), 0, '/', $domain, false, false);
            $cookies[] = new Cookie(static::COOKIE_LA_USERNAME, $user->getUsername(), 0, '/', $domain, false, false);
            $cookies[] = new Cookie(static::COOKIE_LA_DISPLAY_USERNAME, $this->getDisplayUsername($user), 0, '/', $domain, false, false);
            $cookies[] = new Cookie(static::COOKIE_LA_FIRST_NAME, $crm->getFirstName(), 0, '/', $domain, false, false);
            $cookies[] = new Cookie(static::COOKIE_LA_LAST_NAME, $crm->getLastName(), 0, '/', $domain, false, false);
            $cookies[] = new Cookie(static::COOKIE_LA_ENCRYPTED, $this->getEncryptedCookie($user), 0, '/', $domain, false, false);
        } catch (\Exception $e) {
            throw new \Exception("Error while generating token for " . $user->getId() . " : " . $e->getMessage());
        }

        $this->setCookies($cookies);

        // other cookies
        $this->setRemoteTokenUser($user, $request);

        if ($this->conf['communityfactory']) {
            $this->setCfCookies($user, $request);
        }

    }

    protected function getEncryptedCookie(UserInterface $user)
    {
        $cookieData = json_encode(
            array(
                'email' => $user->getEmailCanonical(),
                'login' => $user->getUsernameCanonical(),
                'id' => $user->getId(),
                'firstName' => $user->getCrm()->getFirstName(),
                'lastName' => $user->getCrm()->getLastName()
            ));
        return Cipher::encrypt($cookieData, $this->conf['encrypt_text_key']);
    }

    /**
     * @param UserInterface $user
     * @return null|Token
     */
    public function getUserLoggedToken(UserInterface $user)
    {
        $repository = $this->em->getRepository('La\UserBundle\Entity\Token');
        $entity = $repository->findOneById($user->getId());
        if (!is_null($entity)) {
            return $entity->getToken();
        } else {
            return null;
        }
    }

    /**
     * @param UserInterface $user
     * @return string
     */
    public function createAndStoreToken(UserInterface $user)
    {
        $tokenGenerator = new TokenGenerator();
        $token = $tokenGenerator->generateToken();

        $repository = $this->em->getRepository('La\UserBundle\Entity\Token');
        $repository->cleanUp();
        $repository->remove($user->getId());

//        $entity = $repository->findOneById($user->getId());
//
//        // If token doesnt exist yet
//        if (is_null($entity) || empty($entity)) {

        $entity = new Token();
        $entity->setToken($this->encodeToken($token));
        $entity->setId($user->getId());

//        }

        // Dans tous les cas, mettre à jour la date d'expiration.
        $repository->extendAndStore($entity);

        return $token;
    }

    /**
     * Create temporary token for external SSO
     *
     * @param UserInterface $user
     * @param Request $request
     */
    public function setRemoteTokenUser(UserInterface $user, Request $request)
    {
        $token = $this->createAndStoreToken($user);

        $domain = $this->getDomain($request->getHttpHost());
        $date = new \DateTime();
        $date->modify(sprintf('+%d year', static::LIFETIME_PERSISTENT_COOKIE));
        $cookies[] = new Cookie(static::COOKIE_LA_TOKEN, $token, $date, '/', $domain, false, false);
        $this->setCookies($cookies);
    }

    /**
     * Create cookies for Community Factory
     *
     * @param UserInterface $user
     * @param Request $request
     */
    public function setCfCookies(UserInterface $user, Request $request)
    {
        $password = $user->getPassword();
        $salt = $user->getSalt();
        $id = $user->getId();
        if ($id > $user::AUTO_INCREMENT_ID) {
            $login = $user->getUsername();
        } else {
            $login = $user->getUsernameCanonical();
        }

        $unserialisedSalt = @unserialize(base64_decode($salt));
        if (!is_null($unserialisedSalt)) {
            if (5 === $unserialisedSalt['password_hash_type']) {
                $password = md5($password);
            }
        }

        $domain = $this->getDomain($request->getHttpHost());


        $cookies[] = new Cookie(static::COOKIE_CF_USER_ID, $id, time() + 3600 * 24, '/', $domain, false, false);
        $cookies[] = new Cookie(static::COOKIE_CF_USER_PASS, $password, time() + 3600 * 24, '/', $domain, false, false);
        $cookies[] = new Cookie(static::COOKIE_CF_USER_LOGIN, $login, time() + 3600 * 24, '/', $domain, false, false);


        $cookies[] = new Cookie(static::COOKIE_MD_ID, $id, 0, '/', $domain, false, false);
        $cookies[] = new Cookie(static::COOKIE_MD_PASS, $password, 0, '/', $domain, false, false);
        $cookies[] = new Cookie(static::COOKIE_MD_USER, $login, 0, '/', $domain, false, false);

        $this->setCookies($cookies);

    }

    /**
     * Remove all user cookies
     *
     * @param Request $request
     */
    public function unsetAllCookies(Request $request)
    {
        foreach ($this->cookies as $cookie) {
            setcookie($cookie, '', time() - 3600 * 25, "/", $this->getDomain($request->getHttpHost()));
        }

    }

    /**
     * Set all user cookies
     *
     * @param array $cookies
     */
    protected static function setCookies(array $cookies)
    {
        foreach ($cookies as $cookie) {
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
        }
    }

    /**
     * Get current domain from hostname
     *
     * @param string $host
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function getDomain($host)
    {
        $split = explode('.', $host);
        if (count($split) < 2) {
            throw new \InvalidArgumentException('Invalid Host');
        }
        $len = count($split);
        return sprintf('%s.%s', $split[$len - 2], $split[$len - 1]);
    }

    /**
     * Encode public token
     *
     * @param $token
     * @return string
     */
    public function encodeToken($token)
    {
        return sha1(sha1($this->conf['sso_key']) . '-' . sha1($token));
    }

    /**
     * remove given token
     *
     * @param $token
     * @return bool
     */
    public function removeToken($token)
    {
        try {
            $repository = $this->em->getRepository('La\UserBundle\Entity\Token');
            $this->em->remove($repository->find($token));
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }

    }

    /**
     * Retrieve user from token
     *
     * @param $token
     * @return object
     * @throws \Exception
     */
    public function getUserFromToken($token)
    {
        $tokenRepository = $this->em->getRepository('La\UserBundle\Entity\Token');
        $tokenRepository->cleanup();
        // le token est encodé coté ez avec la meme methode encodeToken()
        $entity = $tokenRepository->findByToken($token);

        if (count($entity) === 1) {
            $idUser = $entity[0]->getId();
            $user = $this->getUserFromId($idUser);

            $tokenRepository->extendAndStore($entity);

            return $user;
        } else {
            throw new \Exception('Invalid User');
        }
    }

    /**
     * Retrieve user from user Id
     *
     * @param $id
     * @return UserInterface
     * @throws \Exception
     */
    public function getUserFromId($id)
    {
        try {
            $user = $this->userManager->findUserBy(array("id" => $id));
            return $user;
        } catch (\Exception $e) {
            throw new \Exception('Invalid User');
        }
    }

    /**
     * Retrieve user from user Email
     *
     * @param $email
     * @return mixed
     * @throws \Exception
     */
    public function getUserFromEmail($email)
    {
        try {
            $user = $this->userManager->findUserByEmail($email);
            if ($user) {
                return $user;
            }
            throw new \Exception('Invalid User');
        } catch (\Exception $e) {
            throw new \Exception('Invalid User');
        }
    }

    /**
     * Retrieve user from user Email
     *
     * @param $email
     * @return mixed
     * @throws \Exception
     */
    public function getCrmFromEmail($email)
    {
        $crmRepository = $this->em->getRepository($this->conf['crm_class']);
        try {
            $crm = $crmRepository->findByEmail($email);
            if (count($crm)) {
                return $crm[0];
            }
            throw new \Exception('Invalid Crm');
        } catch (\Exception $e) {
            throw new \Exception('Invalid Crm');
        }
    }

    /**
     * Retrieve user from user canonical Username
     *
     * @param $user
     * @return mixed
     * @throws \Exception
     */
    public function getUserFromUsername($user)
    {
        try {
            $users = $this->userManager->findUserByUsername($user);
            if ($users) {
                return $users;
            }
            throw new \Exception('Invalid User');
        } catch (\Exception $e) {
            throw new \Exception('Invalid User');
        }
    }

    /**
     * send logout process
     */
    public function logoutIfNeeded(Request $request)
    {

        if ($this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') && !$this->tokenCookieExists()) {
            $this->unsetAllCookies($request);
            $user = $this->securityContext->gettoken()->getuser();
            if ($user instanceof UserInterface) {
                $this->session->invalidate();
                $this->securityContext->settoken(null);
                unset($user);
            }
        }
        //$this->unsetAllCookies($request);

    }

    /**
     * Ez remove token cookie on logout
     *
     * @return bool
     */
    protected function  tokenCookieExists()
    {
        return isset($_COOKIE[static::COOKIE_LA_TOKEN]);
    }

    /**
     * @param $target
     * @param $source
     * @param $fieldsToMerge
     * @return mixed
     * @throws \Exception
     */
    public function merge($target, $source, $fieldsToMerge)
    {

        $reflection = new \ReflectionClass($target);

        foreach ($fieldsToMerge as $property => $property_content) {

            if (!$reflection->hasProperty($property)) {
                throw new \Exception("Error while updating : " . $property . " is not a valid property of " . get_class($source) . ".");
            }

            $reflectionProperty = $reflection->getProperty($property);
            $reflectionProperty->setAccessible(true);

            // Check if the array is a child object and has to be merged through a recursive call. If not, continue : invalid field to merge.
            if (is_array($property_content)) {
                if (is_object($reflectionProperty->getValue($source))) {
                    $value = $this->merge(
                        $reflectionProperty->getValue($target),
                        $reflectionProperty->getValue($source),
                        $property_content
                    );
                } else {
                    continue;
                }
            } else { // Else : regular property
                $value = $reflectionProperty->getValue($source);
            }

            if ($reflectionProperty->getValue($target) != $value && !is_null($value)) { // Only merge if value is different and not null
                $reflectionProperty->setValue($target, $value);
            }
        }

        return $target;
    }

    /**
     * @param UserInterface $user
     * @return string
     */
    public function getDisplayUsername(UserInterface $user)
    {
        $event = new UserEvent($user);

        $this->dispatcher->dispatch(LaUserEvents::USER_DISPLAY_USERNAME_COMPUTE, $event);
        if ($event->isPropagationStopped()) {
            $displayUsername = $event->getDisplayUsername();
        } else {
            $displayUsername = $user->getUsername();
        }

        return $displayUsername;
    }

    /**
     * Activate a user : unserialize its crm & merge if an existing crm matching its email is found in db
     *
     * @param $user
     */
    public function activateUserCrm(UserInterface $user)
    {
        if (!is_null($user->getCrmTmp())) {
            // 1. Unserialize temporary Crm
            /** @var CrmInterface $crm */
            $crm = @unserialize($user->getCrmTmp());

            $event = new CrmEvent($crm);
            $this->dispatcher->dispatch(LaUserEvents::ON_REGISTRATION_CRM_ACTIVATION, $event);

            // 2. Merge new Crm with (potentially) existing Crm.
            $crmRepository = $this->em->getRepository($this->crmClass);
            $existingCrm = $crmRepository->findByEmail($user->getEmailCanonical());
            if (count($existingCrm) > 0) {
                $crm = $existingCrm[0];
                $crmRepository->updateExistingCrm($crm, $event->getCrm());

                $event = new CrmEvent($crm);
                $this->dispatcher->dispatch(LaUserEvents::ON_REGISTRATION_CRM_MERGE, $event);
            }

            // 3. Save Crm into User
            $user->setCrmTmp(null);
            $user->setCrm($crm);
        }
    }

    /**
     * Set data in serialized Crm : Most importantly email, and if available origin.
     *
     * @param UserInterface $user
     * @param null $origin
     */
    public function setCrmTmpData(UserInterface $user, $origin = null)
    {
        $crmTmp = @unserialize($user->getCrmTmp());
        if (is_object($crmTmp)) {
            $crmTmp->setEmail($user->getEmailCanonical());
            $crmTmp->setOrigin($origin);

            $user->setCrmTmp($crmTmp);
        }
    }

    /**
     * Factory computing origin from redirect url. Returns null if parse_url return false.
     *
     * @return string | null
     */
    public function siteOriginFactory()
    {
        $url = $this->conf['redirect_url'];
        $origin = parse_url($url, PHP_URL_HOST);

        return $origin ? : null;
    }

    /**
     * @param $offset
     * @param null $limit
     * @return array
     */
    public function getUnconfirmedUsers($offset = 0, $limit = null)
    {
        $limitDate = false;
        if (!is_null($limit)) {
            if ($limit < $offset) {
                exit($limit . " is smaller than " . $offset . " and shouldn't be." . PHP_EOL);
            }
            $limitDate = new \DateTime();
            $limitDate->modify(sprintf("-%d hours", $limit));
            $limitDate = "AND created >= '" . $limitDate->format("Y-m-d H:i:s") . "'";
        }

        $offsetDate = new \DateTime();
        $offsetDate->modify(sprintf("-%d hours", $offset));
        $offsetDate = "AND created <= '" . $offsetDate->format("Y-m-d H:i:s") . "'";

        $query = sprintf("SELECT email
                          FROM lauser__users
                          WHERE crmTmp IS NOT NULL
                          AND confirmation_token IS NOT NULL
                          AND enabled = 0 %s %s
                          ORDER BY created DESC", $offsetDate, $limitDate ? $limitDate : "");

        return $this->em->getConnection()->fetchAll($query);
    }

    /**
     * To delete an existing user, set his status to expired.
     * A cron handle deletion of all 'expired' users > 24h, calling $this->deleteExpiredUser()
     * This is necessary to export the new state of the user before we actually delete it.
     *
     * @param $email
     * @throws \Exception
     * @return mixed
     */
    public function deleteUser($email)
    {
        /** @var \La\UserBundle\Entity\User $user */
        $user = $this->getUserFromEmail($email);
        $user->setModified(new \DateTime());
        $user->setExpired(true);
        $user->setExpiresAt(new \DateTime());

        $event = new UserEvent($user);
        $this->dispatcher->dispatch(LaUserEvents::USER_DELETE_REQUEST, $event);

        $this->userManager->updateUser($user);

        if ($event->isPropagationStopped()) {
            throw new \Exception(sprintf("An error occured while deleting user with email: %s", $email));
        } else {
            return $user;
        }
    }

    /**
     * Delete an expired user. No physical deletion to not lose any data.
     * Instead, change its email and username
     *
     * @param string $email Email of user you wish to delete
     * @return bool
     * @todo log exceptions & co.
     */
    public function deleteExpiredUser($email)
    {
        try {
            /** @var \La\UserBundle\Entity\User $user */
            $user = $this->getUserFromEmail($email);

            // Check if user is eligible for deletion. If not expired,
            if ($user->isExpired()) {

                // change email/username to allow user to recreate his account after deletion
                $disabledId = uniqid();
                $disabledEmail = "deleted" . $disabledId . "@" . $email;
                $disabledUserName = "deleted" . $disabledId . "@" . $user->getUsername();

                $tokenRepo = $this->em->getRepository("LaUserBundle:Token");
                $tokens = $tokenRepo->findById($user->getId()); // Magic call findById
                foreach ($tokens as $token) {
                    $this->em->remove($token); // LOG : sprintf("-> Token %s (User %s / Expiration %s) deleted.", $token->getToken(), $token->getId(), $token->getExpire()->format("Y-m-d H-i-s"));
                }

                $user->setEmail($disabledEmail);
                $user->setUsername($disabledUserName);
                $user->setModified(new \Datetime());
                // No longer expired, it's now disabled so that we don't delete it <again>. we keep expiresAt date so we know when it has been deleted
                $user->setExpired(false);
                $user->setEnabled(false);

                if (is_null($user->getCrm())) {
                    $user->setCrmTmp(null);
                } else {
                    $crm = $user->getCrm();
                    $crm->setEmail($disabledEmail);
                }

                $this->userManager->updateUser($user);

                return true;
            } else {
                throw new \Exception(sprintf("Tried to delete the following user but it's not expired: %s", $email));
            }
        } catch (\Exception $e) {
//            die($e->getMessage()); // log exceptions
            return false;
        }
    }

    /**
     * get ExpiredUsers older than 24 hours ago
     *
     * @return array
     */
    public function getExpiredUsers()
    {
        $yesterday = new \DateTime();
        $yesterday->modify("-24 hours");

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('email', 'email');
        $q = $this->em->createNativeQuery(' SELECT u.email FROM lauser__users u WHERE u.expired = 1 AND u.expires_at <= :yesterday ', $rsm)
            ->setParameter('yesterday', $yesterday->format('Y-m-d H:i:s'));

        $res = $q->getResult();
        $emails = [];
        // clean results so that we return an array of emails, or an empty array
        if (is_array($res) && !empty($res)) {
            foreach ($res as $dirtyEmail) {
                if(isset($dirtyEmail['email'])){
                    $emails[] = $dirtyEmail['email'];
                }
            }
        }
        return $emails;
    }

}