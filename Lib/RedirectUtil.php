<?php
namespace La\UserBundle\Lib;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectUtil
{

    public function __construct(array  $conf, $session, $templating)
    {
        $this->conf = $conf;
        $this->session = $session;
        $this->templating = $templating;
    }

    /**
     * Add redirect url to session
     * @param $redirectUrl
     */
    public function addRedirectUrl($redirectUrl)
    {
        $this->session->set('la_user.redirect_urls', $redirectUrl);
    }

    /**
     * Add javascript callback to session
     *
     * @param null $callback
     */
    public function addCallback($callback = null)
    {
        $this->session->set('la_user.callbacks', $callback);
    }

    /**
     * Add controller context to session
     *
     * @param $context
     */
    public function addContext($context)
    {
        $this->session->set('la_user.context', $context);
    }

    /**
     * Get redirect url from session
     *
     * @return bool|string
     */
    public function getRedirectUrl()
    {
        $redirectUrl = $this->session->get('la_user.redirect_urls');
        if (!empty($redirectUrl)) {
            $this->session->remove('la_user.redirect_url');
            return $redirectUrl;
        }
        return false;
    }

    /**
     * Get javascript callback from session
     *
     * @return bool
     */
    public function getCallback()
    {
        $callback = $this->session->get('la_user.callbacks');
        if (!empty($callback)) {
            $this->session->remove('la_user.callbacks');
            return $callback;
        }
        return false;
    }

//
//    /**
//     * Send response redirect
//     *
//     * @param string $redirect_url
//     * @param bool $forceRedirect
//     */
//    public function redirectTo($redirect_url, $forceRedirect = false)
//    {
//        $this->responseRedirectTo($redirect_url, $forceRedirect)->send();
//        exit();
//    }

    /**
     * Send response callback
     *
     * @param $callback
     * @param bool $forceRedirect
     */
    public function callback($callback, $forceRedirect = false)
    {
        $this->responseCallback($callback, $forceRedirect)->send();
        exit();
    }


    /**
     * Send response redirect
     *
     * @param $request
     */
    public function getReponseRedirect($request)
    {
        // redirect if logged
        if ($request->query->has('callback')) {
            // get parameter redirect_url
            $data = null;
            if ($request->query->has('data')) {
                $data = $request->query->get('data');
            }
            $this->responseCallback(array($request->query->get('callback'), $data))->send();
        } elseif ($request->query->has('redirect_url')) {
            // get parameter redirect_url
            $this->responseRedirectTo($request->query->get('redirect_url'))->send();
        } else {
            // default
            $this->responseRedirectTo($this->conf['redirect_url'])->send();
        }
        exit();
    }

    /**
     * Set redirect action
     *
     * @param $request
     */
    public function setRedirectAction(Request $request)
    {
        $referer = parse_url($request->headers->get('referer'))['path'];
        $self = $request->server->get('REQUEST_URI', '');
        $self = explode('?', $self)[0];
        if ($self === $referer) {
            return;
        }
        if ('POST' === strtoupper($request->getMethod())) {
            return;
        }

        $this->unsetRedirectionData();

        if ($request->query->has('callback')) {
            // get parameter redirect_url
            $data = null;
            if ($request->query->has('data')) {
                $data = urldecode($request->query->get('data'));
            }
            $this->addCallback(array(urldecode($request->query->get('callback')), urldecode($data)));
        } elseif ($request->query->has('redirect_url')) {
            // set session for redirect in events
            $this->addRedirectUrl($request->query->get('redirect_url'));
        } else {
            // default
            // SI pas de redirect spécifié : on ne redirige pas !
        }

        if ($request->query->has('context')) {
            $this->addContext(urldecode($request->query->get('context')));

        } else {
            // TODO: default context en param
            $this->addContext('nomodal');
        }

    }

    /**
     * unset redirect actions
     */
    public function unsetRedirectionData()
    {
        $this->session->remove('la_user.redirect_urls');
        $this->session->remove('la_user.callbacks');
        $this->session->remove('la_user.context');
    }

    /**
     * Get redirect response
     * @param $redirect_url
     * @param bool $forceRedirect
     * @return RedirectResponse|Response
     */
    public function responseRedirectTo($redirect_url, $forceRedirect = false)
    {
        $this->unsetRedirectionData();
        if ($this->conf['html_redirect'] && !$forceRedirect) {
            $content = $this->templating->render('LaUserBundle:Redirect:redirect.html.twig', array('redirect_url' => $redirect_url));
            $response = new Response($content);
        } else {
            $response = new RedirectResponse($redirect_url);
        }
        return $response;
    }

    /**
     * Get javascript callback response
     * @param $callback
     * @param bool $forceRedirect
     * @return Response
     */
    public function responseCallback($callback, $forceRedirect = false)
    {
        $this->unsetRedirectionData();

        $content = $this->templating->render('LaUserBundle:Redirect:redirect.html.twig', array('callback' => $callback[0], 'data' => $callback[1]));
        return new Response($content);
    }
}