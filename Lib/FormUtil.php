<?php
namespace La\UserBundle\Lib;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormUtil
{

    /**
     * Filter a form to leave in it only fields posted in HTTP Request.
     *
     * @param FormInterface $form
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    public function filterFormFromRequest(FormInterface $form, Request $request)
    {
        $fieldsToKeep = $request->request->get($form->getName());
        if(is_null($fieldsToKeep)){
            $fieldsToKeep = [];
        }

        $this->filterFields($form, $fieldsToKeep);

        return $form;
    }

    /**
     * Filter $form from fields not set in $fieldsToKeep
     *
     * @param FormInterface $form
     * @param array $fieldsToKeep
     */
    protected function filterFields(FormInterface &$form, array $fieldsToKeep)
    {
        foreach($form->all() as $fieldName => $field){
            if(!isset($fieldsToKeep[$fieldName])){
                $form->remove($fieldName);
                continue;
            }
            if(is_array($fieldsToKeep[$fieldName])){
                $this->filterFields($field, $fieldsToKeep[$fieldName]);
            }
        }
    }
}

