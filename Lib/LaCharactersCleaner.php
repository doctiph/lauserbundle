<?php
namespace La\UserBundle\Lib;


class LaCharactersCleaner
{

    const LA_CHARACTERS_VALID_RULE = ' a-zA-Z0-9._-';

    public function __construct($doctrine, $userClass)
    {
        $this->doctrine = $doctrine;
        $this->userClass = $userClass;
    }

    public function clean($str)
    {
        $dirtyStr = $str;

        $wrongEmailDns = array(
            'alice',
            'aol',
            'f', 'fr', 'fre', 'free',
            'ho', 'hot', 'hotm', 'hotma', 'hotmai', 'hotmail',
            'lapos', 'lapost', 'laposte',
            'neuf',
            'or', 'ora', 'oran', 'orang',
            'skynet',
            'te', 'tele', 'tele2',
            'voi',
            'wai',
            'w', 'wa', 'wan', 'wana', 'wanad', 'wanado', 'wanadoo',
            'y', 'ya', 'yah', 'yaho', 'yahoo',
        );

        if (preg_match('#(.+)@[a-z0-9\.-]+\.[a-z]+$#i', $str, $matches) && is_array($matches) && isset($matches[1])) {
            $str = $matches[1];
        } else {
            foreach ($wrongEmailDns as $dns) {
                if (preg_match('#(.+)@' . $dns . '(\.)?$#i', $str, $matches) && is_array($matches) && isset($matches[1])) {
                    $str = $matches[1];
                    break;
                }
            }
        }

        // Replacing accents a
        $str = preg_replace('/[\x{00c0}-\x{00c5}]/u', 'A', $str);
        $str = preg_replace('/[\x{00e0}-\x{00e5}]/u', 'a', $str);
        // Replacing accents e
        $str = preg_replace('/[\x{00c8}-\x{00cb}]/u', 'E', $str);
        $str = preg_replace('/[\x{00e8}-\x{00eb}]/u', 'e', $str);
        // Replacing accents i
        $str = preg_replace('/[\x{00cc}-\x{00cf}]/u', 'I', $str);
        $str = preg_replace('/[\x{00ec}-\x{00ef}]/u', 'i', $str);
        // Replacing accents o
        $str = preg_replace('/[\x{00d2}-\x{00d6}]/u', 'O', $str);
        $str = preg_replace('/[\x{00f2}-\x{00f6}]/u', 'o', $str);
        // Replacing accents u
        $str = preg_replace('/[\x{00d9}-\x{00dc}]/u', 'U', $str);
        $str = preg_replace('/[\x{00f9}-\x{00fc}]/u', 'u', $str);
        // Replacing accents y
        $str = preg_replace('/\x{00dd}/u', 'Y', $str);
        $str = preg_replace('/[\x{00fd}\x{00ff}]/u', 'y', $str);

        $str = preg_replace('/\x{00c6}/u', 'AE', $str);
        $str = preg_replace('/\x{00c7}/u', 'C', $str);
        $str = preg_replace('/\x{00d1}/u', 'N', $str);
        $str = preg_replace('/\x{00d8}/u', 'O', $str);
        $str = preg_replace('/\x{00e6}/u', 'ae', $str);
        $str = preg_replace('/\x{00e7}/u', 'c', $str);
        $str = preg_replace('/\x{00f1}/u', 'n', $str);
        $str = preg_replace('/\x{00f8}/u', 'o', $str);

        // Replacing forbidden character with space
        $str = preg_replace('/[^' . self::LA_CHARACTERS_VALID_RULE . ']/', ' ', $str);

        // Replacing multiple whitespace by only one
        $str = preg_replace('@\s+@', ' ', $str);
        $str = trim($str);

        if (!preg_match("@^[" . self::LA_CHARACTERS_VALID_RULE . "]+$@", $dirtyStr)) {
            return $str;
        }
        if ($str === $dirtyStr) {
            return $str;
        }

        $isUnique = $this->isUniqueLogin($str);
        if (!$isUnique) {
            $uniqueLogin = $this->findUniqueLogin($str);
            if (is_null($uniqueLogin)) {
                return false;
            }
            $str = $uniqueLogin;
        }

        // minimal length
        if (strlen($str) < 4) {
            $lengthtoadd = 4 - strlen($str);
            do {
                $str = $str . '_';
                $lengthtoadd--;
            } while ($lengthtoadd > 0);
        }

        $userClass = $this->userClass;
        if (in_array($str, $userClass::getForbiddenUsernames())) {
            $str = null;
        }

        return $str;
    }


    public function isUniqueLogin($str)
    {
        $repository = $this->doctrine->getRepository($this->userClass);
        $exists = $repository->findByUsername($str);
        if (count($exists) > 0) {
            return false;
        }
        return true;
    }

    public function findUniqueLogin($str)
    {

        for ($i = 1; $i <= 99; $i++) {
            if ($this->isUniqueLogin($tmp = $str . $i)) {
                return $tmp;
            }
        }

        return null;
    }

    public function isFirstCharAlphanumerical($str)
    {
        return ctype_alnum(substr($str, 0, 1));
    }

    public function isNumerical($str)
    {
        $str = str_replace('-', '', $str);
        $str = str_replace('.', '', $str);
        $str = str_replace('_', '', $str);

        return ctype_digit($str);
    }
}
