<?php

namespace La\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**

php app/console doctrine:generate:entities La/UserBundle/Entity/Token

php app/console doctrine:schema:update --force

 */

/**
 * @ORM\Table(name="lauser__token")
 * @ORM\Entity(repositoryClass="La\UserBundle\Entity\TokenRepository")
 */

class Token
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     */
    protected $token;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $expire;

    /****************************************************************************************************
     *                                        Delete after
     ****************************************************************************************************/


    /**
     * Set token
     *
     * @param string $token
     * @return Token
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Token
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expire
     *
     * @param \DateTime $expire
     * @return Token
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;
    
        return $this;
    }

    /**
     * Get expire
     *
     * @return \DateTime 
     */
    public function getExpire()
    {
        return $this->expire;
    }
}