<?php

namespace La\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use La\UserBundle\Traits\Entity as Property;

/**
 * @ORM\Entity
 * @ORM\Table(
 *      name="lauser__visit",
 *      indexes={
 *          @ORM\Index(name="iduser_idx", columns={"idUser"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="La\UserBundle\Entity\UserVisitRepository")
 */
class UserVisit
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastVisit;


    /**
     * @param mixed $lastVisit
     */
    public function setLastVisit($lastVisit)
    {
        $this->lastVisit = $lastVisit;
    }

    /**
     * @return mixed
     */
    public function getLastVisit()
    {
        return $this->lastVisit;
    }

    /**
     * @ORM\Column(type="integer")
     */
    protected $idUser;


    /**
     * @param mixed $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return mixed
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @ORM\Column(type="boolean")
     */
    protected $connected;


    /**
     * @param mixed $connected
     */
    public function setConnected($connected)
    {
        $this->connected = $connected;
    }

    /**
     * @return mixed
     */
    public function getConnected()
    {
        return $this->connected;
    }

    /**
     * IP based on headers
     *
     * @ORM\Column(type="string")
     */
    protected $ip = '';


    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * return $ip
     */
    public function setIp($headers)
    {
        if (is_array($headers)) {
            if (isset($headers['HTTP_TRUE_CLIENT_IP'])) {
                $this->ip = $headers['HTTP_TRUE_CLIENT_IP'];
            } elseif (isset($headers['HTTP_X_FORWARDED_FOR'])) {
                $this->ip = $headers['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($headers['REMOTE_ADDR'])) {
                $this->ip = $headers['REMOTE_ADDR'];
            }
        }
        return $this->ip;
    }

    /**
     * Token
     *
     * @ORM\Column(type="string")
     */
    protected $token = '';


    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}