<?php

namespace La\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\AST\Join;
use La\UserBundle\Entity\CrmInterface;

class CrmRepository extends EntityRepository
{

//       Schemas pour l'updateExistingCrm()
//
//       Pour lire les tableaux :
//       - OUI/NON = la valeur est différente (ou non) de la valeur par défaut
//       - X = on ne set pas la nouvelle valeur.
//       - ✓ = on set la nouvelle valeur.
//
//       Cas $overwrite = true
//                             nouvelle valeur
//                                |OUI|NON|
//        valeur existante    |OUI| X | X |
//                            |NON| ✓ | ✓ |
//
//       Cas $overwrite = false
//                             nouvelle valeur
//                                |OUI|NON|
//        valeur existante    |OUI| ✓ | X |
//                            |NON| ✓ | ✓ |
//

    /**
     * Ca sert à quoi : Ca sert à mettre à jour un crm sans relation avec un user (inscription NL)
     *
     * @param CrmInterface $existingCrm
     * @param CrmInterface $newCrm
     * @param bool $overwrite
     */
    public function updateExistingCrm(CrmInterface &$existingCrm, CrmInterface $newCrm, $overwrite = true)
    {

        $reflection = new \ReflectionClass(get_class($newCrm));
        $defaultProperties = $reflection->getDefaultProperties();

        // Default value est la valeur par défaut de la propriété
        foreach ($defaultProperties as $propertyName => $defaultValue) {
            try {
                $property = $reflection->getProperty($propertyName);
                if (is_null($property)) {
                    continue;
                }
                $property->setAccessible(true);

                // Si le crm existant a déjà une valeur (non-nulle) et que l'on n'écrase pas
                if ($property->getValue($existingCrm) !== $defaultValue && !is_null($property->getValue($existingCrm)) && !$overwrite) {
                    continue;
                }
                // Si le crm existant a déjà une valeur (non-nulle) et que le nouveau n'en a pas
                if ($property->getValue($existingCrm) !== $defaultValue && !is_null($property->getValue($existingCrm)) && $property->getValue($newCrm) === $defaultValue) {
                    continue;
                }

                // Tous les autres cas :
                $property->setValue($existingCrm, $property->getValue($newCrm));
            } catch (\Exception $e) {
                continue;
            }
        }
    }

    /**
     * @param \DateTime $date
     * @param bool $limit
     * @param int $offset
     * @return array
     */
    public function getActiveUserFromDate(\DateTime $date, $limit = false, $offset = 0)
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('e')
            ->leftJoin('e.user', 'u')
            ->where('(u.enabled = 1 OR u.enabled IS NULL)')
            ->andWhere('e.modified >= :modified')
            ->setParameter('modified', $date);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $result = $qb
            ->getQuery()
            ->getResult();
        return $result;
    }

    public function getCountActiveUserFromDate(\DateTime $date, $limit = false, $offset = 0)
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->select('count(e.id)')
            ->leftJoin('e.user', 'u')
            ->where('(u.enabled = 1 OR u.enabled IS NULL)')
            ->andWhere('e.modified >= :modified')
            ->setParameter('modified', $date);

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $result = $qb
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param \DateTime $date
     * @param bool $limit
     * @param int $offset
     * @param boolean $sql
     * @param bool $active
     * @return array
     */
    public function getIterateActiveUserFromDate(\DateTime $date, $limit = false, $offset = 0, $sql = false, $active = false)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('e')
            ->leftJoin('e.user', 'u')
            ->where('e.modified >= :modified')
            ->setParameter('modified', $date);

        if ($active) {
            $qb->andWhere('(u.enabled = 1 OR u.enabled IS NULL)');
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $q = $qb->getQuery();
        if ($sql) {
            return $qb->getQuery()->getSQL();
        }
        $iterableResult = $q->iterate();
        return $iterableResult;
    }

    /**
     * @return array
     */
    public function getMaxId()
    {
        return $this->createQueryBuilder('e')
            ->select('MAX(e.id)')
            ->getQuery()
            ->getResult();
    }
}
