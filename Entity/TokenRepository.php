<?php

namespace La\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use La\UserBundle\Api\Exception as ApiException;

/**
 * TokenRepository
 */
class TokenRepository extends EntityRepository
{

    const TOKEN_TTL = 10;

    /**
     * Remove old user tokens
     *
     * @return array
     */
    public function cleanUp()
    {
        $date = new \DateTime();

        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getEntityName(), 't')
            ->where("t.expire <= :expire")
            ->setParameter('expire', $date);

        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * Get expired tokens (expiration date can be specified)
     *
     * @param null $expirationDate
     * @return array
     */
    public function getExpired($expirationDate = null)
    {
        if (is_null($expirationDate)) {
            $expirationDate = new \DateTime();
        }

        $qb = $this->_em->createQueryBuilder();
        $qb->select('t')
            ->from($this->getEntityName(), 't')
            ->where("t.expire <= :expire")
            ->setParameter('expire', $expirationDate);
        $query = $qb->getQuery();
        $result = $query->getResult();

        return $result;
    }

    /**
     * Remove a user token
     *
     * @param $id
     * @return array
     */
    public function remove($id)
    {
        $date = new \DateTime();
        $qb = $this->_em->createQueryBuilder();
        $qb->delete($this->getEntityName(), 't')
            ->where("t.id = :id")
            ->setParameter('id', $id);

        $query = $qb->getQuery();
        $result = $query->getResult();
        return $result;
    }

    /**
     * Extend and store an existing token
     *
     * @param $token
     * @throws \La\UserBundle\Api\Exception\InvalidTokenException
     */
    public function extendAndStore($token)
    {
        if (is_array($token)) {
            $token = $token[0];
        }

        $expire = new \DateTime();
        $expire->modify('+' . static::TOKEN_TTL . 'day');

        try {
            $token->setExpire($expire);
        } catch (\Exception $e) {
            throw new ApiException\InvalidTokenException();
        }

        $this->store($token);
    }


    /**
     * Store token
     *
     * @param $token
     */
    public function store($token)
    {
        $this->_em->persist($token);
        $this->_em->flush();
    }
}
