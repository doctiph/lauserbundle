<?php

namespace La\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as AbstractUser;
use La\UserBundle\Traits\Entity as Property;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @ORM\MappedSuperclass
 */
abstract class User extends AbstractUser implements UserInterface
{
    const AUTO_INCREMENT_ID = 1;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        parent::__construct();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    use Property\User\Crm;
    use Property\User\CrmTmp;
    use Property\User\EmailRequested;
    use Property\Created;
    use Property\Modified;

    public function setSalt($salt)
    {
        $this->salt = $salt;
        return $this;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(
            new UniqueEntity(array(
                'fields' => 'usernameCanonical',
                'errorPath' => 'username',
                'message' => 'la_user.registration.username.already_used',
                'groups' => 'LaRegistration')));

        $metadata->addConstraint(
            new UniqueEntity(array(
                'fields' => 'emailCanonical',
                'errorPath' => 'email',
                'message' => 'la_user.registration.email.already_used',
                'groups' => 'LaRegistration')));
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getDisplayUsername($token = null)
    {
        return $this->username;
    }

    public static function getForbiddenUsernames()
    {
        return array(
            'admin',
            'administrateur',
            'administrator',
            'anonymous',
            'anonyme',
            'chansig',
            'chansigaud',
            'elle',
            'fakeuser',
            'fake',
            'moderateur',
            'modérateur',
            'moderator',
            'pierre-yves',
            'root',
            'toto',
            'test',
            'webmaster',
            'www',
        );
    }

}