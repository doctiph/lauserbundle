<?php

namespace La\UserBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use La\UserBundle\Entity\CrmInterface;
use La\UserBundle\Traits\Entity as Property;


/**
 * @ORM\MappedSuperclass
 *
 */
abstract class Crm implements CrmInterface
{

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->modified = new \DateTime();
        $this->setLastActivity();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    use Property\Id;
    use Property\Created;
    use Property\Crm\Modified;
    use Property\Crm\LastActivity;
    use Property\Crm\Email;

    use Property\Crm\User;
    use Property\Crm\FirstName;
    use Property\Crm\LastName;
    use Property\Crm\Origin;

    use Property\PropertiesGetter;

}