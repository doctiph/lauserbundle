<?php

namespace La\UserBundle\Entity;

interface UserInterface
{

    public function getDisplayUsername($token = null);

    public function setSalt($salt);

    public static function getForbiddenUsernames();

}