<?php

namespace La\UserBundle\Services;

use FOS\UserBundle\Doctrine\UserManager;

class CreateTestUser
{
    protected $serviceDoctrine;
    protected $userManager;
    protected $lauserConf;
    protected $clientCrmEntity;
    protected $userClass;
    protected $em;

    /**
     * @param $em
     * @param UserManager $usermanager
     * @param $conf
     * @param $crmClass
     * @param $userClass
     */
    public function __construct($em, UserManager $usermanager, $conf, $crmClass, $userClass)
    {
        $this->userManager = $usermanager;
        $this->clientCrmEntity = $crmClass;
        $this->lauserConf = $conf;
        $this->userClass = $userClass;
        $this->em = $em;
    }

    /**
     * @param $output
     * @param bool $superadmin
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function createTestUser($output, $superadmin = true)
    {
        $testUser = $this->userManager->createUser();

        $testUser->setUsername($this->lauserConf['test_user']);
        $testUser->setPlainPassword($this->lauserConf['test_user_password']);
        $testUser->setEmail($this->lauserConf['test_user_email']);
        $testUser->setEnabled(true);
        try {
            $crm = $this->createCrm();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            exit();
        }

        $testUser->setCrm($crm);
        if ($superadmin === true || $superadmin === "true") {
            $testUser->setSuperAdmin(true);
        }
        $this->userManager->updateUser($testUser);

        return $testUser;
    }

    /**
     * @return mixed
     */
    protected function createCrm()
    {

        $crm = new $this->clientCrmEntity();
        $crm->setFirstName('Prénom de test');
        $crm->setLastName('Nom de Famille de test');

        return $crm;
    }


    public function deleteTestUser()
    {
        $userRepository = $this->em->getRepository($this->userClass);

        $user = $userRepository->findOneByUsername($this->lauserConf['test_user']);

        $crm = $user->getCrm();
        $this->em->remove($user);
        $this->em->remove($crm);
        $this->em->flush();
    }

    /**
     * @return bool
     */
    public function checkTestUser()
    {
        $repository = $this->em->getRepository($this->userClass);

        if ($repository->findBy(array('username' => $this->lauserConf['test_user']))) {
            return true;
        } else {
            return false;
        }
    }
}