<?php

namespace La\UserBundle\Services;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use FOS\UserBundle\Mailer\Mailer;
use FOS\UserBundle\Model\UserInterface;
use La\MessagingBundle\Mailer\MailerSender;

class LaMailer extends Mailer
{
    /**
     * @var \La\MessagingBundle\Mailer\MailerSender
     */
    protected $mailer;

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $parameters;

    public function __construct(MailerSender $mailer, RouterInterface $router, EngineInterface $templating, Session $session, $libUser, array $parameters)
    {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->templating = $templating;
        $this->session = $session;
        $this->parameters = $parameters;
        $this->libUser = $libUser;

    }

    /**
     * {@inheritdoc}
     */
    public function sendConfirmationEmailMessage(UserInterface $user, $context = array())
    {
        $template = $this->parameters['registration_confirmation.template'];

        $to = $user->getEmail();

        $query = '';
        if ($this->session->has('la_user.redirect_urls')) {
            $query .= '?redirect_url=' . $this->session->get('la_user.redirect_urls');
        }

        $context = array_merge(
            array(
                'username' => $this->libUser->getDisplayUsername($user),
                'token' => $user->getConfirmationToken(),
                'query' => $query
            ), $context);

        $headers = array(
            'from' => $this->parameters['from_email']['registration_confirmation'],
            'sender' => array_keys($this->parameters['from_email']['registration_confirmation'])[0],
            'replyTo' => $this->parameters['from_email']['registration_confirmation'],
        );

        $this->mailer
            ->withTemplate($template)
            ->createAndSend(
                $to,
                $context,
                $headers
            );
    }

    /**
     * @param UserInterface $user
     * @param array $context
     * @return mixed
     */
    public function sendReminderEmailMessage(UserInterface $user, $context = array())
    {
        $template = $this->parameters['registration_reminder.template'];

        $to = $user->getEmail();

        $query = '';
        if ($this->session->has('la_user.redirect_urls')) {
            $query .= '?redirect_url=' . $this->session->get('la_user.redirect_urls');
        }

        $context = array_merge(
            array(
                'username' => $this->libUser->getDisplayUsername($user),
                'token' => $user->getConfirmationToken(),
                'query' => $query
            ), $context);

        $headers = array(
            'from' => $this->parameters['from_email']['registration_confirmation'],
            'sender' => array_keys($this->parameters['from_email']['registration_confirmation'])[0],
            'replyTo' => $this->parameters['from_email']['registration_confirmation'],
        );

        $this->mailer
            ->withTemplate($template)
            ->createAndSend(
                $to,
                $context,
                $headers
            );
    }

    /**
     * Send the resetting password email to the user
     *
     * @param UserInterface $user
     * @param array
     * @return mixed
     */
    public function sendResettingEmailMessage(UserInterface $user, $context = array())
    {
        $template = $this->parameters['resetting_password.template'];

        $to = $user->getEmail();

        $query = '';
        if ($this->session->has('la_user.redirect_urls')) {
            $query .= '?redirect_url=' . $this->session->get('la_user.redirect_urls');
        }

        $context = array_merge(array(
            'username' => $this->libUser->getDisplayUsername($user),
            'token' => $user->getConfirmationToken(),
            'query' => $query
        ), $context);

        $headers = array(
            'from' => $this->parameters['from_email']['resetting_password'],
            'sender' => array_keys($this->parameters['from_email']['resetting_password'])[0],
            'replyTo' => $this->parameters['from_email']['resetting_password'],
        );


        $this->mailer
            ->withTemplate($template)
            ->createAndSend(
                $to,
                $context,
                $headers
            );
    }


    public function sendWelcomeEmailMessage(UserInterface $user, $context = array())
    {
        $template = $this->parameters['registration_welcome.template'];

        $context = array_merge(array(
            'username' => $this->libUser->getDisplayUsername($user),
        ), $context);

        $headers = array(
            'from' => $this->parameters['from_email']['registration_confirmation'],
            'sender' => array_keys($this->parameters['from_email']['registration_confirmation'])[0],
            'replyTo' => $this->parameters['from_email']['registration_confirmation'],
        );

        $this->mailer
            ->withTemplate($template)
            ->createAndSend(
                $user->getEmail(),
                $context,
                $headers
            );
    }

    public function sendChangeEmailMessage(UserInterface $user, $context = array())
    {
        $template = $this->parameters['change_email.template'];

        $to = $user->getEmail();

        $query = '';
        if ($this->session->has('la_user.redirect_urls')) {
            $query .= '?redirect_url=' . $this->session->get('la_user.redirect_urls');
        }

        $context = array_merge(array(
            'username' => $this->libUser->getDisplayUsername($user),
            'token' => $user->getEmailRequestedToken(),
            'query' => $query
        ), $context);

        $headers = array(
            'from' => $this->parameters['from_email']['change_email'],
            'sender' => array_keys($this->parameters['from_email']['change_email'])[0],
            'replyTo' => $this->parameters['from_email']['change_email'],
        );

        $this->mailer
            ->withTemplate($template)
            ->createAndSend(
                $to,
                $context,
                $headers
            );
    }

}