<?php

namespace La\UserBundle\Services;

use Symfony\Component\Console\Output\OutputInterface;

class SetAutoIncrement
{

    public function __construct($doctrineService, $userClass)
    {
        $this->doctrine = $doctrineService;
        $this->userClass = $userClass;
    }

    public function setAutoIncrement($value, OutputInterface $output)
    {

        if(is_null($value))
        {
            $user = $this->userClass;
            $value = $user::AUTO_INCREMENT_ID;
        }
        $tables = array(
          'lauser__crm',
          'lauser__users'
        );

        try {

            foreach($tables as $table)
            {
                $res = $this->doctrine
                            ->getManager()
                            ->getConnection()
                            ->executeUpdate(sprintf('ALTER TABLE  %s AUTO_INCREMENT = %d',$table,$value));
                $output->writeln(sprintf('%d affected rows.',$res));
            }

            return $value;

        }catch(\Exception $e)
        {
            $output->writeln($e->getMessage());
        }
    }

}

