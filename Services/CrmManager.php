<?php

namespace La\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use La\UserBundle\Entity\CrmInterface;

/**
 * Class CrmManager
 * @package La\UserBundle\Services
 */
class CrmManager
{
    protected $em;
    protected $clientCrmEntity;

    /**
     * @param EntityManager $em
     * @param $crmClass
     */
    public function __construct(EntityManager $em, $crmClass)
    {
        $this->em = $em;
        $this->clientCrmEntity = $crmClass;
        $this->crmRepository = $em->getRepository($this->clientCrmEntity);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        return new $this->clientCrmEntity();
    }

    /**
     * @param $crm
     * @return mixed
     */
    public function update(CrmInterface $crm)
    {
        $this->em->persist($crm);
        $this->em->flush();
        return $crm;
    }

    /**
     *  get em
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getByEmail($email)
    {
        //Check if crm exists and returns it if it does.
        $crm = $this->crmRepository->findOneByEmail($email);
        if ($crm instanceof CrmInterface) {
            return $crm;
        } else {
            return null;
        }
    }

    /**
     * @param $id
     * @return null|object
     */
    public function getById($id)
    {
        //Check if crm exists and returns it if it does.
        $crm = $this->crmRepository->findOneById($id);
        if ($crm instanceof CrmInterface) {
            return $crm;
        } else {
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function getMaxId()
    {
        return $this->getRepository()->getMaxId();
    }


    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository()
    {
        return $this->crmRepository;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->clientCrmEntity;
    }

    /**
     * Update old crm with data from new crm. Return old crm.
     * Update only empty values if not authenticated, update even filled values if authenticated.
     *
     * @param \La\UserBundle\Entity\CrmInterface $old
     * @param \La\UserBundle\Entity\CrmInterface $new
     * @param bool $authenticated
     * @return \La\UserBundle\Entity\CrmInterface $old
     */
    public function updateCrm(CrmInterface $old, CrmInterface $new, $authenticated = false)
    {
        // If different emails, cannot assert the update. Return.
        if ($old->getEmail() != $new->getEmail()) {
            return $old;
        }

        $reflection = new \ReflectionClass($old);
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            if (
                (is_null($property->getValue($old)) || $property->getValue($old) === '' || $authenticated)
                &&
                (!is_null($property->getValue($new)) && $property->getValue($old) != '')
            ) {
                $property->setValue($old, $property->getValue($new));
            }
        }

        return $old;
    }


}