<?php

namespace La\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use La\UserBundle\Entity\UserVisit;

/**
 * Class UserVisitManager
 * @package La\UserBundle\Services
 */
class UserVisitManager
{

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->userVisitRepository = $em->getRepository('La\UserBundle\Entity\UserVisit');
    }

    /**
     * @param $idUser
     * @param $token
     * @param $headers
     * @param bool $connected
     */
    public function track($idUser, $token, $headers, $connected = true)
    {
        $visit = $this->userVisitRepository->findOneByIdUser($idUser);

        if (is_null($visit)) {
            $visit = new UserVisit();
        }

        $now = new \DateTime();
        $visit->setIdUser($idUser);
        $visit->setConnected($connected);
        $visit->setToken($token);
        $visit->setLastVisit($now);
        $visit->setIp($headers);
        if (!$this->em->isOpen()) {
            $this->em = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }
        $this->em->persist($visit);
        $this->em->flush();
    }
}