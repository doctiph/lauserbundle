<?php
namespace La\UserBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressHelper;
use FOS\UserBundle\Model\UserManager;
use La\UserBundle\Lib\LaUserUtil;

class RegistrationReminder
{
    const DEBUG = true;

    /** @var  EntityManager */
    protected $em;
    /** @var LaMailer */
    protected $mailer;
    /** @var UserManager */
    protected $userManager;
    /** @var LaUserUtil */
    protected $userUtils;

    /**
     * @param $em
     * @param LaMailer $mailer
     * @param UserManager $userManager
     * @param LaUserUtil $userUtils
     */
    public function __construct($em, LaMailer $mailer, UserManager $userManager, LaUserUtil $userUtils)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->userManager = $userManager;
        $this->userUtils = $userUtils;
    }

    public function remind(OutputInterface $output, ProgressHelper $progress, $force = null)
    {
        $affectedEmails = [];
        $offset = 24;
        if ($force) {
            $offset = 0; // remind now
        }
        $users = $this->getUnconfirmedUsers($offset, 48);
        $output->writeln(sprintf("%d users found. Sending mails...", count($users)));

        if (self::DEBUG) {
            $progress->start($output, count($users));
        }
        foreach ($users as $email) {
            $user = $this->userManager->findUserBy(array('email' => $email));
            $this->mailer->sendReminderEmailMessage($user);

            $affectedEmails[] = $user->getEmail();
            if (self::DEBUG) {
                $progress->advance();
            }
        }
        if (self::DEBUG) {
            $progress->finish();
        }
        if (count($users) > 0) {
            $output->writeln('Affected users : ' . implode(',', $affectedEmails));
        }
        $output->writeln(sprintf("%d users were reminded to activate their account.", count($users)));
    }

    public function clean(OutputInterface $output, ProgressHelper $progress)
    {
        $affectedEmails = [];
        $errorsEmails = [];

        $users = $this->getUnconfirmedUsers(48, null);
        $output->writeln(sprintf("%d users found. Deleting...", count($users)));
        if (self::DEBUG) {
            $progress->start($output, count($users));
        }
        foreach ($users as $u) {
            $email = $u['email'];

            if($this->userUtils->deleteUser($email)){
                $affectedEmails[] = $email;
            }else{
                $errorsEmails[] = $email;
            }

            if (self::DEBUG) {
                $progress->advance();
            }
        }
        if (self::DEBUG) {
            $progress->finish();
        }
        if (count($users) > 0) {
            $output->writeln('Affected users : ' . implode(',', $affectedEmails));
            $output->writeln('Failed users   : ' . implode(',', $errorsEmails));
        }
        $output->writeln(sprintf("%d users were removed from the database.", count($affectedEmails) - count($errorsEmails)));
    }

    /**
     * @param $offset
     * @param null $limit
     * @return array
     */
    protected function getUnconfirmedUsers($offset, $limit)
    {
        $limitDate = false;
        if (!is_null($limit)) {
            if ($limit < $offset) {
                exit($limit . " is smaller than " . $offset . " and shouldn't be." . PHP_EOL);
            }
            $limitDate = new \DateTime();
            $limitDate->modify(sprintf("-%d hours", $limit));
            $limitDate = "AND created >= '" . $limitDate->format("Y-m-d H:i:s") . "'";
        }

        $offsetDate = new \DateTime();
        $offsetDate->modify(sprintf("-%d hours", $offset));
        $offsetDate = "AND created <= '" . $offsetDate->format("Y-m-d H:i:s") . "'";

        $query = sprintf("SELECT email
                          FROM lauser__users
                          WHERE crmTmp IS NOT NULL
                          AND confirmation_token IS NOT NULL
                          AND enabled = 0 %s %s", $offsetDate, $limitDate ? $limitDate : "");

        return $this->em->getConnection()->fetchAll($query);
    }

}