<?php

namespace La\UserBundle\Services;

use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\Bundle\DoctrineBundle\Registry;

class GraphicalStatsGenerator
{
    const LINE_NUMBER = 30;

    protected $doctrine;

    public function __construct(Registry $doctrineService)
    {
        $this->doctrine = $doctrineService;
    }

    public function getDailyRegistrations($limit = 7, OutputInterface $output)
    {
        $oldestDate = $this->getLimitDateTime($limit);

        $graphData = array();

        $query = "SELECT DAY(C.created) as 'day', MONTH(C.created) as 'month', YEAR(C.created) as 'year', count(C.id) as 'nb'
                  FROM lauser__users U INNER JOIN lauser__crm C ON U.crm_id = C.id
                  WHERE C.created >= '" . $oldestDate->format('Y-m-d 00:00:00') .
               "' GROUP BY day, month ORDER BY month, day";

        $dailyRegistrations = $this->doctrine->getManager()->getConnection()->fetchAll($query);

        foreach ($dailyRegistrations as $dailyRegistration) {

            $weekday = new \DateTime($dailyRegistration['year'] . "-" . $dailyRegistration['month'] . "-" . $dailyRegistration['day']);

            $day = $weekday->format("l");
            $we = $day == "Saturday" || $day == "Sunday";

            $graphData[] = array(
                "date" => $weekday,
                "label_format" => $we ? "weekend" : null,
                "graph_format" => null,
                "labels" => array(
                    $day,
                    $dailyRegistration['day'],
                    $dailyRegistration['month']),
                "registrations" => $dailyRegistration['nb'],
                "parameters" => array(
                    "left_spaces" => 5,
                    "spaces_between_bars" => 1,
                    "spaces_in_bars" => 0
                )
            );
        }

        $this->drawGraph($graphData, $output);
    }

    public function getMonthlyRegistrations($limit = 12, OutputInterface $output)
    {
        $oldestDate = $this->getLimitDateTime($limit * 30.5);
        $graphData = array();

        $query = "SELECT MONTH(C.created) as 'month', YEAR(C.created) as 'year', count(C.id) as 'nb'
                      FROM lauser__users U INNER JOIN lauser__crm C ON U.crm_id = C.id
                      WHERE C.created >= '" . $oldestDate->format('Y-m-01 00:00:00') .
            "' GROUP BY YEAR(C.created),MONTH(C.created) ORDER BY YEAR(C.created), MONTH(C.created)";

        $dailyRegistrations = $this->doctrine->getManager()->getConnection()->fetchAll($query);

        foreach ($dailyRegistrations as $dailyRegistration) {

            $month = new \DateTime($dailyRegistration['year'] . "-" . $dailyRegistration['month'] . "-01");

            $graphData[] = array(
                "date" => $month,
                "label_format" => null,
                "graph_format" => null,
                "labels" => array(
                    $month->format("F"),
                    $dailyRegistration['year'],
                ),
                "registrations" => $dailyRegistration['nb'],
                "parameters" => array(
                    "left_spaces" => 7,
                    "spaces_between_bars" => 3,
                    "spaces_in_bars" => 2
                )
            );
        }
        $this->drawGraph($graphData, $output);
    }

    public function getDailySubscriptions($limit = 7, OutputInterface $output)
    {
        $oldestDate = $this->getLimitDateTime($limit);

        $graphData = array();

        $query = "SELECT DAY(C.created) as 'day', MONTH(C.created) as 'month', YEAR(C.created) as 'year', count(C.id) as 'nb'
                  FROM lauser__crm C
                  WHERE C.created >= '" . $oldestDate->format('Y-m-d 00:00:00') .
            "' GROUP BY day, month ORDER BY month, day";

        $dailyRegistrations = $this->doctrine->getManager()->getConnection()->fetchAll($query);

        foreach ($dailyRegistrations as $dailyRegistration) {

            $weekday = new \DateTime($dailyRegistration['year'] . "-" . $dailyRegistration['month'] . "-" . $dailyRegistration['day']);

            $day = $weekday->format("D");
            $we = $day == "Sat" || $day == "Sun";

            $graphData[] = array(
                "date" => $weekday,
                "label_format" => $we ? "weekend" : null,
                "graph_format" => null,
                "labels" => array(
                    $day,
                    $dailyRegistration['day'],
                    $dailyRegistration['month']),
                "registrations" => $dailyRegistration['nb'],
                "parameters" => array(
                    "left_spaces" => 5,
                    "spaces_between_bars" => 1,
                    "spaces_in_bars" => 0
                )
            );
        }

        $this->drawGraph($graphData, $output);
    }

    public function getMonthlySubscriptions($limit = 12, OutputInterface $output)
    {
        $oldestDate = $this->getLimitDateTime($limit * 30.5);
        $graphData = array();

        $query = "SELECT MONTH(C.created) as 'month', YEAR(C.created) as 'year', count(C.id) as 'nb'
                      FROM lauser__crm C
                      WHERE C.created >= '" . $oldestDate->format('Y-m-01 00:00:00') .
            "' GROUP BY YEAR(C.created),MONTH(C.created) ORDER BY YEAR(C.created), MONTH(C.created)";

        $dailyRegistrations = $this->doctrine->getManager()->getConnection()->fetchAll($query);

        foreach ($dailyRegistrations as $dailyRegistration) {

            $month = new \DateTime($dailyRegistration['year'] . "-" . $dailyRegistration['month'] . "-01");

            $graphData[] = array(
                "date" => $month,
                "label_format" => null,
                "graph_format" => null,
                "labels" => array(
                    $month->format("F"),
                    $dailyRegistration['year'],
                ),
                "registrations" => $dailyRegistration['nb'],
                "parameters" => array(
                    "left_spaces" => 7,
                    "spaces_between_bars" => 3,
                    "spaces_in_bars" => 2
                )
            );
        }
        $this->drawGraph($graphData, $output);
    }

    protected function drawGraph(array $data, OutputInterface $output)
    {
        // PARAMETERS
        $left_spaces = $data[0]['parameters']['left_spaces'];
        $spaces_between_bars = $data[0]['parameters']['spaces_between_bars'];
        $spaces_in_bars = $data[0]['parameters']['spaces_in_bars'];

        $datatype = $this->getDataType($data);

        $total = 0;
        foreach ($data as $entry) {
            $total += $entry['registrations'];
        }

        $y_max = self::LINE_NUMBER;
        foreach ($data as $entry) {
            if ($entry['registrations'] > $y_max) {
                $y_max = $entry['registrations'];
            }
        }
        $y_step = round($y_max / self::LINE_NUMBER);
        //

        // TITLE

        $output->writeln(sprintf(
            PHP_EOL . "<emphasis> Stats for the last %d %ss (%s to %s)" . PHP_EOL . "=> Total %d - Average per %s : %d</emphasis>" . PHP_EOL,
            count($data),
            $datatype,
            $data[0]['date']->format('Y-m-d'),
            $data[count($data) - 1]['date']->format('Y-m-d'),
            $total,
            $datatype,
            $total / count($data)
        ));

        // DATA

        for ($i = $y_max; $i > 0; $i = $i - $y_step) {

            $y_data_line1 = sprintf("% " . $left_spaces . "s |", $i);

            foreach ($data as $entry) {

                $format = is_null($entry['graph_format']) ? "graph_bars" : $entry["graph_format"];

                if ($entry["registrations"] >= $i + $y_step) {
                    $y_data_line1 .= "<" . $format . ">┃" . str_repeat(" ", $spaces_in_bars) . "┃</" . $format . ">" . str_repeat('.', $spaces_between_bars);
                } else if ($entry["registrations"] >= $i) {
                    $y_data_line1 .= "<" . $format . ">┏" . str_repeat("━", $spaces_in_bars) . "┓</" . $format . ">" . str_repeat('.', $spaces_between_bars);
                } else {
                    $y_data_line1 .= "." . str_repeat(".", $spaces_in_bars) . "." . str_repeat('.', $spaces_between_bars);
                }

            }
            $output->writeln($y_data_line1);
        }

        // LABELS

        $y_labels = [];

        foreach ($data[0]['labels'] as $label) {
            $y_labels[] = str_repeat(' ', $left_spaces + 2);
        }

        foreach ($data as $entry) {
            foreach ($entry['labels'] as $i => $label) {
                if (is_null($entry['label_format'])) {
                    $y_labels[$i] .= $this->computeLabel($label, $spaces_in_bars, $spaces_between_bars);
                } else {
                    $y_labels[$i] .=
                        "<" . $entry['label_format'] . ">" . $this->computeLabel($label, $spaces_in_bars, $spaces_between_bars) . "</" . $entry['label_format'] . ">";
                }
            }
        }
        foreach ($y_labels as $y_label) {
            $output->writeln($y_label);
        }

        $output->writeln("");
    }

    protected function getLimitDateTime($limit)
    {
        $datetime = new \DateTime();
        $datetime->modify("-" . $limit . " days");
        return $datetime;
    }

    protected function getDataType($data)
    {
        if(count($data) <3)
        {
            exit("Error : Not enough data to compute. Please try a larger scope.".PHP_EOL);
        }
        $month0 = $data[0]['date']->format('m');
        $month1 = $data[1]['date']->format('m');
        $month2 = $data[2]['date']->format('m');

        // If 3 consecutive dates have 3 different months, then it's monthly data
        if ($month0 != $month1) {
            if ($month1 != $month2) {
                return "month";
            }
        }
        return "day";
    }

    protected function computeLabel($label, $spaces_in_bars, $spaces_between_bars)
    {
        // Just in case if the label is numeric
        $label = sprintf("%02s", $label);

        $available_space = $spaces_in_bars + 2 + $spaces_between_bars  - 1; //  - 1 for visibility
        $label_size = strlen($label);

        if($available_space <= $label_size)
        {
            $truncate_size = $available_space;
            $label = substr($label, 0, $truncate_size)." ";
        }else{
            $remaining_space = $available_space + 1 - $label_size;
            $label =  $label . str_repeat(" ", $remaining_space);
        }

        return $label;
    }


}
