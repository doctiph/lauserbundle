<?php

namespace La\UserBundle\Services;

use La\StatsBundle\Model\AbstractStatsManager;

/**
 * Class StatsManager
 * @package La\UserBundle\Services
 */
class StatsManager extends AbstractStatsManager
{
    /**
     * @return string
     */
    public function getType()
    {
        return "user";
    }


    /**
     * @return array
     */
    public function listAvailableStats()
    {
        return array(
            "totalUsers" => $this->getTotalUsers(),
            "totalCrm" => $this->getTotalCrm(),
            "monthlyUsers" => $this->getMonthlyUsersByDay(),
            "monthlyCrm" => $this->getMonthlyCrmByDay(),
            "yearlyUsers" => $this->getYearlyUsersByMonth(),
            "yearlyCrm" => $this->getYearlyCrmByMonth(),
        );
    }

    /**
     * @return int
     */
    public function getTotalUsers()
    {
        $query = "SELECT count(U.id) as 'nb'
                  FROM lauser__users U
                  WHERE U.enabled = 1
                  AND U.locked = 0
                  AND U.expired = 0";
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    /**
     * @return int
     */
    public function getTotalCrm()
    {
        $query = "SELECT count(C.id) as 'nb'
                  FROM lauser__crm C";
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        return $stmt->fetchColumn();
    }

    /**
     * @return string
     */
    public function getMonthlyUsersByDay()
    {
        $query = "SELECT DATE(C.created) as 'date', count(U.id) as 'nb'
                  FROM lauser__users U
                  LEFT JOIN lauser__crm C ON U.crm_id = C.id
                  WHERE C.created >= '".$this->oneMonthAgo."'
                  AND U.enabled = 1
                  AND U.locked = 0
                  AND U.expired = 0
                  GROUP BY DAY(C.created), MONTH(C.created), YEAR(C.created) ORDER BY YEAR(C.created), MONTH(C.created), DAY(C.created)";
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());
        return serialize($stats);
    }

    /**
     * @return string
     */
    public function getYearlyUsersByMonth()
    {
        $query = "SELECT  DATE_FORMAT(C.created, '%Y-%m') as 'date', count(U.id) as 'nb'
                  FROM lauser__users U
                  LEFT JOIN lauser__crm C ON U.crm_id = C.id
                  WHERE C.created >= '".$this->oneYearAgo."'
                  AND U.enabled = 1
                  AND U.locked = 0
                  AND U.expired = 0
                  GROUP BY MONTH(C.created), YEAR(C.created) ORDER BY YEAR(C.created), MONTH(C.created)";
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());
        return serialize($stats);
    }

    /**
     * @return string
     */
    public function getMonthlyCrmByDay()
    {
        $query = "SELECT DATE(C.created) as 'date', count(C.id) as 'nb'
                  FROM lauser__crm C
                  WHERE C.created >= '".$this->oneMonthAgo."'
                  GROUP BY DAY(C.created), MONTH(C.created), YEAR(C.created) ORDER BY YEAR(C.created), MONTH(C.created), DAY(C.created)";

        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateMonthlyData($stmt->fetchAll());
        return serialize($stats);
    }

    /**
     * @return string
     */
    public function getYearlyCrmByMonth()
    {
        $query = "SELECT DATE_FORMAT(C.created, '%Y-%m') as 'date', count(C.id) as 'nb'
                  FROM lauser__crm C
                  WHERE C.created >= '".$this->oneYearAgo."'
                  GROUP BY MONTH(C.created), YEAR(C.created) ORDER BY YEAR(C.created), MONTH(C.created)";
        $stmt = $this->em->getConnection()->prepare($query);
        $stmt->execute();
        $stats = $this->populateYearlyData($stmt->fetchAll());
        return serialize($stats);
    }
}