<?php
namespace La\UserBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\EventDispatcher\EventDispatcher;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Doctrine\UserManager;

class ManualUserActivation
{

    /** @var  EventDispatcher */
    protected $dispatcher;
    /** @var  UserManager */
    protected $userManager;


    /**
     * @param $dispatcher
     * @param $userManager
     */
    public function __construct($dispatcher, $userManager)
    {
        $this->dispatcher = $dispatcher;
        $this->userManager = $userManager;
    }

    /**
     * @param $usernameOrIdToActivate
     * @param Output $output
     * @return array|bool
     */
    public function activate($usernameOrIdToActivate, Output $output)
    {
        foreach ($usernameOrIdToActivate as $usernameOrId) {

            // If argument is not numeric, then it cannot be an id : It's likely an username
            $found = $this->userManager->findUserByUsername($usernameOrId);
            if (is_null($found)) {
                $found = $this->userManager->findUserBy(array('id' => $usernameOrId));
                if (is_null($found)) {
                    $output->writeln(sprintf("<error>The argument '%s' you entered isnt valid or couldn't be found as username or id.</error>", $usernameOrId));
                    return false;
                }
            }
            $this->activateUser($found, $usernameOrId, $output);
        }
        return true;

    }

    public function activateUser($user, $needle, Output $output = null)
    {

        if ($user->isEnabled() && !is_null($user->getCrm()) && !is_null($output)) {
            $output->writeln(sprintf(
                "<error>User found for argument '%s' (id: %d -- username: %s) hasn't been activated : It already is.</error>", $needle, $user->getId(), $user->getUsername()));
        } else {
            $this->dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, new GetResponseUserEvent($user, new Request()));
            $user->setCrmTmp(null);
            $user->setEnabled(true);
            $this->userManager->updateUser($user);
            if (!is_null($output)) {
                $output->writeln(sprintf("<icy>User found for argument '%s' (id: %d -- username: %s)  has been activated !</icy>", $needle, $user->getId(), $user->getUsername()));
            }
        }

    }

}