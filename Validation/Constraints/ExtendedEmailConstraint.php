<?php
namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class ExtendedEmailConstraint extends Constraint
{
    public $message = array(
        'disposable_host' => 'la_user.exended_email.disposable_host',
        'invalid_email' => 'la_user.exended_email.invalid_email'
    );

    public function validatedBy()
    {
        return 'security.validator.extended_email';
    }

}