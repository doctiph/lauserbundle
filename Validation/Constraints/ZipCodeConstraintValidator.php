<?php
namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ZipCodeConstraintValidator extends ConstraintValidator
{

    public function validate($object, Constraint $constraint)
    {
        $country = $object->getCountry();
        $zipCode = $object->getZipCode();

        $errorPath = $constraint->errorPath;

        // validate only if french zip code ( or zip code entered with no country )
        if ('FR' === $country || (is_null($country) && $zipCode > 0)) {
            if (is_null($country)) {
                $this->context->addViolationAt($errorPath, $constraint->message['blank_country']);
            } else if (strlen($zipCode) > 0) {
                if (!preg_match('/^[0-9]{5}$/', $zipCode, $matches)) {
                    $this->context->addViolationAt($errorPath, $constraint->message['length']);
                } else if (!preg_match('/[A-Za-z0-9\-_\.]+$/', $zipCode, $matches)) {
                    $this->context->addViolationAt($errorPath, $constraint->message['invalid_chars']);
                }
            }
        }

        // TODO les autres...
    }
}