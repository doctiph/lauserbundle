<?php

namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class LaCurrentPassword extends Constraint
{
    public $message = 'la_user.profile.current_password.invalid';
    public $service = 'security.validator.la_current_password';

    public function validatedBy()
    {
        return $this->service;
    }
}
