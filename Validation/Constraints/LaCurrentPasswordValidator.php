<?php

namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;

class LaCurrentPasswordValidator extends ConstraintValidator
{
    private $securityContext;
    private $encoderFactory;

    public function __construct(SecurityContextInterface $securityContext, EncoderFactoryInterface $encoderFactory)
    {
        $this->securityContext = $securityContext;
        $this->encoderFactory = $encoderFactory;
    }

    public function validate($password, Constraint $constraint)
    {
        $user = $this->securityContext->getToken()->getUser();
        if (!$user instanceof UserInterface) {
            throw new ConstraintDefinitionException('The User object must implement the UserInterface interface.');
        }

        $security = $this->securityContext;
        if (!$security->isGranted('ROLE_SUPER_ADMIN') && !$security->isGranted('ROLE_PREVIOUS_ADMIN')) {
            $encoder = $this->encoderFactory->getEncoder($user);

            if (!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
                $this->context->addViolation($constraint->message);
            }

        }

    }
}
