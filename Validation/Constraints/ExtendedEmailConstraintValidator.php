<?php
namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExtendedEmailConstraintValidator extends ConstraintValidator
{


    public function validate($value, Constraint $constraint)
    {
        /** @var ExtendedEmailConstraint $constraint */

        if ($this->validateEmail($value)) {
            $host = explode("@", $value)[1];
            $this->validateHost($host, $constraint);
        } else {
            $this->context->addViolation($constraint->message['invalid_email']);
        }
    }

    protected function validateHost($host, $constraint)
    {
        $class = "EmailChecker\\EmailChecker";

        if (class_exists($class)) {
            $checker = new $class();
            if ($checker->isValid($host)) {
                $this->context->addViolation($constraint->message['disposable_host']);
            }
        }

    }

    protected function validateEmail($value)
    {
        return
            filter_var($value, FILTER_VALIDATE_EMAIL)
            &&
            filter_var($value, FILTER_SANITIZE_EMAIL);
    }

}