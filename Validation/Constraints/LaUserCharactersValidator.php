<?php
namespace La\UserBundle\Validation\Constraints;

use La\UserBundle\Entity\User;
use La\UserBundle\Lib\LaCharactersCleaner;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class LaUserCharactersValidator extends ConstraintValidator
{
    private $laCharactersUtil;

    public function __construct(LaCharactersCleaner $laCharactersUtil)
    {
        $this->laCharactersUtil = $laCharactersUtil;
    }

    public function validate($value, Constraint $constraint)
    {
        if (strlen($value) > 0) {
            if (in_array(strtolower($value), User::getForbiddenUsernames())) {
                $this->context->addViolation($constraint->message['forbidden_username']);
            } else if (!$this->laCharactersUtil->isFirstCharAlphanumerical($value)) {
                $this->context->addViolation($constraint->message['alphanumeric_first_char']);
            }else if ($this->laCharactersUtil->isNumerical($value)) {
                $this->context->addViolation($constraint->message['invalid_chars']);
            } else {
//                preg_match('/[A-Za-z0-9\-_\.]+$/', $value, $matches);
//                if (!count($matches)) {
//                    $this->context->addViolation($constraint->message['invalid_chars']);
//                } else if (isset($matches[0]) && $value != $matches[0]) {
//                    $this->context->addViolation($constraint->message['invalid_chars']);
//                }
                $cleanedValue = $this->laCharactersUtil->clean($value);
                if($cleanedValue != $value)
                {
                    $this->context->addViolation($constraint->message['invalid_chars']);
                }

            }
        }
    }
}