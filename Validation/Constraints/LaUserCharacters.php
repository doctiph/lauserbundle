<?php
namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class LaUserCharacters extends Constraint
{
    public $message = array(
        'alphanumeric_first_char' => 'la_user.registration.username.fchar',
        'invalid_chars' => 'la_user.registration.username.invalid',
        'forbidden_username' => 'la_user.registration.username.forbidden'
    );

    public function validatedBy()
    {
        return 'security.validator.la_characters';
    }
}