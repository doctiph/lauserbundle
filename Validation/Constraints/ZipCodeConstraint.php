<?php

namespace La\UserBundle\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

class ZipCodeConstraint extends Constraint
{

    public $errorPath = 'zipcode';

    public $message = array(
        'length' => 'la_user.registration.zipcode.length',
        'invalid_chars' => 'la_user.registration.zipcode.invalid',
        'blank_country' => 'la_user.registration.zipcode.blankcountry',
    );

    // the target is the class and not the property since we need to check another property (country)
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}