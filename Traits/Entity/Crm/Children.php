<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait Children
{
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $children;

    /**
     * Set children
     *
     * @param array $children
     * @return $this
     */
    public function setChildren(array $children)
    {
        $childrenToInsert = array();
        foreach ($children as $child) {
            $childrenToInsert[] = $this->CleanChild($child);
        }
        $this->children = serialize($childrenToInsert);
        return $this;
    }

    /**
     * Get children
     *
     * @return array
     */
    public function getChildren()
    {
        $arrayChildren = @unserialize($this->children);
        return is_array($arrayChildren) ? $arrayChildren : array();
    }

    public function CleanChild(array $child)
    {
        $childToInsert = array();
        // sex
        if (isset($child['sex'])) {
            $childToInsert['sex'] = $child['sex'] == 'M' ? 1 : $child['sex'] === 'F' ? 2 : 0;
        }
        // first name
        if (isset($child['first_name'])) {
            $childToInsert['first_name'] = $child['first_name'] ? : '';
        }
        // birthdate
        if (isset($child['birthdate'])) {
            $birthdatetime = new \DateTime;
            if (is_numeric($child['birthdate'])) {
                $childToInsert['birthdate'] = $birthdatetime->setTimestamp($child['birthdate']);
            } else {
                $childToInsert['birthdate'] = null;
            }
        }
        //picture (disabled for elle ?)
//        $childToInsert['picture'] = $child['picture'] ?: '';/
        return $childToInsert;
    }
}