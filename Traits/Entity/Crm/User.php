<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait User
{
    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="crm")
     */
    protected $user;


    /**
     * Set user
     *
     * @param User $user
     * @return $this
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return null|User
     */
    public function getUser()
    {
        return $this->user;
    }

}