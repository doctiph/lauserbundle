<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait LastActivity
{

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastActivity;

    /**
     * Set last activity
     *
     * @param \DateTime $lastActivity
     * @return $this
     */
    public function setLastActivity(\DateTime $lastActivity = null)
    {
        if ($lastActivity instanceof \DateTime) {
            $this->lastActivity = $lastActivity;
        } else{
            $this->lastActivity = $this->modified > $this->created ? $this->modified : $this->created;
        }
        return $this;
    }

    /**
     * Get last activity
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }
}