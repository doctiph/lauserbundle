<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait Birthdate
{

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $birthdate;

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return $this
     */
    public function setBirthdate(\DateTime $birthdate = null)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }
}