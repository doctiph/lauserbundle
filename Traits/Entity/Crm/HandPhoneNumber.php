<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait HandPhoneNumber
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $handPhoneNumber;

    /**
     * Set handPhoneNumber
     *
     * @param string $handPhoneNumber
     * @return $this
     */
    public function setHandPhoneNumber($handPhoneNumber)
    {
        $this->handPhoneNumber = $handPhoneNumber;

        return $this;
    }

    /**
     * Get handPhoneNumber
     * @return string
     */
    public function getHandPhoneNumber()
    {
        return $this->handPhoneNumber;
    }

}