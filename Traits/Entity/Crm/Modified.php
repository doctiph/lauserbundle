<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;

trait Modified
{

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $modified = null;

    /**
     * Set modified
     * @param \DateTime $modified
     * @return $this
     */
    public function setModified(\DateTime $modified = null)
    {
        $this->modified = $modified;
        $this->setLastActivity($modified);

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }
}