<?php
namespace La\UserBundle\Traits\Entity\Crm;

use Doctrine\ORM\Mapping as ORM;
use La\UserBundle\Validation\Constraints\ZipCodeConstraint;
use Symfony\Component\Validator\Mapping\ClassMetadata;

trait ZipCode
{
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $zipcode;

    /**
     * Set zipcode
     *
     * @param string $zipcode
     * @return $this
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    public static function loadValidatorMetadata(ClassMetadata $metadata)
    {
        $metadata->addConstraint(new ZipCodeConstraint(
            array('groups' => array('LaRegistration', 'LaEdit'))
        ));
    }

}