<?php
namespace La\UserBundle\Traits\Entity;

trait PropertiesGetter
{

    public function properties()
    {
        $res = [];
        $reflection = new \ReflectionClass(get_class($this));
        $reflectionProperties = $reflection->getProperties();

        foreach ($reflectionProperties as $reflectionProperty) {
            $reflectionProperty->setAccessible(true);
            $res[$reflectionProperty->getName()] = $reflectionProperty->getValue($this);
        }
        return $res;
    }
}