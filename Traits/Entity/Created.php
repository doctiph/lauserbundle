<?php
namespace La\UserBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Created
{

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $created = null;

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created = null)
    {
        if ($created instanceof \DateTime) {
            $this->created = $created;
        }
        return $this;
    }

    /**
     * Get created
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }
}