<?php
namespace La\UserBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Approved
{

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $approved = null;

    /**
     * Set approved
     * @param \Datetime $approved
     * @return $this
     */
    public function setApproved(\DateTime $approved = null)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return null|\Datetime
     */
    public function getApproved()
    {
        return $this->approved;
    }
}