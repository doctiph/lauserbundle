<?php
namespace La\UserBundle\Traits\Entity\User;

use Doctrine\ORM\Mapping as ORM;

trait Avatar
{

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $avatar = 0;

    /**
     * Set avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * Get avatar
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

}