<?php
namespace La\UserBundle\Traits\Entity\User;

use Doctrine\ORM\Mapping as ORM;

trait RemoteId
{

    /**
     * @ORM\Column(type="integer")
     */
    protected $remoteId = 0;

    /**
     * Set remoteId
     *
     * @param int $remoteId
     * @return $this
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;
        return $this;
    }

    /**
     * Get remoteId
     *
     * @return int
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

}