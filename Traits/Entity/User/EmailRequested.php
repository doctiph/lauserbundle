<?php
namespace La\UserBundle\Traits\Entity\User;

use Doctrine\ORM\Mapping as ORM;

trait EmailRequested
{

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $emailRequested = null;
    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $emailRequestedAt = null;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $emailRequestedToken = null;

    /**
     * @param mixed $emailRequested
     */
    public function setEmailRequested($emailRequested)
    {
        $this->emailRequested = $emailRequested;
    }

    /**
     * @return mixed
     */
    public function getEmailRequested()
    {
        return $this->emailRequested;
    }

    /**
     * @param mixed $emailRequestedAt
     */
    public function setEmailRequestedAt($emailRequestedAt)
    {
        $this->emailRequestedAt = $emailRequestedAt;
    }

    /**
     * @return mixed
     */
    public function getEmailRequestedAt()
    {
        return $this->emailRequestedAt;
    }

    /**
     * @param mixed $emailRequestedToken
     */
    public function setEmailRequestedToken($emailRequestedToken)
    {
        $this->emailRequestedToken = $emailRequestedToken;
    }

    /**
     * @return mixed
     */
    public function getEmailRequestedToken()
    {
        return $this->emailRequestedToken;
    }



}