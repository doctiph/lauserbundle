<?php
namespace La\UserBundle\Traits\Entity\User;

use La\UserBundle\Entity\CrmInterface;
use Doctrine\ORM\Mapping as ORM;

trait CrmTmp
{

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $crmTmp = null;

    /**
     * Set crmTmp
     *
     * @param CrmInterface $crm
     * @return $this
     */
    public function setCrmTmp(CrmInterface $crm = null)
    {
        if (is_null($crm)) {
            $this->crmTmp = null;
        } else {
            $this->crmTmp = serialize($crm);
        }
        return $this;
    }

    /**
     * Get crmTmp
     *
     * @return null|CrmInterface
     */
    public function getCrmTmp()
    {
        return $this->crmTmp;
    }


}