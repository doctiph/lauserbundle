<?php
namespace La\UserBundle\Traits\Entity\User;

use La\UserBundle\Entity\CrmInterface;
use Doctrine\ORM\Mapping as ORM;

trait Crm
{

    /**
     * @ORM\OneToOne(targetEntity="Crm", cascade={"persist", "merge", "detach", "remove"}, inversedBy="user")
     * @ORM\JoinColumn(name="crm_id", referencedColumnName="id")
     *
     */
    protected $crm = null;

    /**
     * Set crm
     *
     * @param CrmInterface $crm
     * @return $this
     */
    public function setCrm(CrmInterface $crm = null)
    {
        $this->crm = $crm;
        if ($this->crm !== null) {
            $this->crm->setEmail($this->getEmail());
        }
        return $this;
    }

    /**
     * Get Crm
     *
     * @return null|CrmInterface
     */
    public function getCrm()
    {
        return $this->crm;
    }
}