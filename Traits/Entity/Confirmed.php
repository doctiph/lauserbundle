<?php
namespace La\UserBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Confirmed
{

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $confirmed = null;

    /**
     * Set confirmed
     *
     * @param \DateTime $confirmed
     * @return $this
     */
    public function setConfirmed(\DateTime $confirmed = null)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return \DateTime
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }
}