<?php
namespace La\UserBundle\Traits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Removed
{

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $removed = null;

    /**
     * Set removed
     *
     * @param \Datetime $removed
     * @return $this
     */
    public function setRemoved(\Datetime $removed = null)
    {
        $this->removed = $removed;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getRemoved()
    {
        return $this->removed;
    }
}