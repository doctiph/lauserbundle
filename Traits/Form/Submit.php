<?php
namespace La\UserBundle\Traits\Form;

trait Submit
{
    /**
     * @param array $options
     */
    public static function add($builder, array $options = array())
    {
        $builder->remove('submit');

        $default = array(
            'label' => 'la_user.form.submit.default',
        );
        $builder->add('submit', 'submit', array_merge($default, $options));
    }

}