<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Form\FormBuilderInterface;

trait PhoneNumber
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.phonenumber',
        );

        $builder
            ->add('phonenumber', 'text', array_merge($default, $options));
    }

}