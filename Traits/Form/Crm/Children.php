<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Form\FormBuilderInterface;

trait Children
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.children.label',
            'expanded' => true,
            'choices' => array(
                'no' => 'la_user.form.crm.children.no',
                'yes' => 'la_user.form.crm.children.yes'),
            'data' => 'no'
        );

        $builder->add('children', 'choice', array_merge($default, $options));
    }

}