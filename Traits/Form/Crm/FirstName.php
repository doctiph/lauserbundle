<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\NotBlank;

trait FirstName
{
    /**
     * @param array $options
     */
    public static function add($builder, array $options = array())
    {

        $default = array(
            'label' => 'la_user.form.crm.firstname',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'la_user.registration.firstName.blank',
                    'groups' => array('LaRegistration', 'LaEdit')
                )),
            ));

        $builder->add('firstName', 'text', array_merge($default, $options));
    }

}