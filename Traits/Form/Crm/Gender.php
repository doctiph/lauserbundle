<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;

trait Gender
{
    /**
     * @param array $options
     */
    public static function add($builder, $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.gender.label',
            'choices' => array(
                '1' => 'la_user.form.crm.gender.female',
                '0' => 'la_user.form.crm.gender.male'),
            'empty_value' => 'la_user.form.crm.gender.empty',
            'constraints' => array(
                new Choice(array(
                    "message" => "la_user.registration.gender.invalid",
                    "choices" => array('0', '1'),
                    "groups" => array( "LaRegistration")
                )),
                new NotBlank(array(
                        "message" => "la_user.registration.gender.blank",
                        "groups" => array("LaRegistration")
                    )
                ),
            ),
        );
        $builder->add('gender', 'choice', array_merge($default, $options));
    }

}