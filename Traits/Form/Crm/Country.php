<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\Country as CountryConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;

trait Country
{
    public static function add($builder, array $options = array())
    {

        $default = array(
            'label' => 'la_user.form.crm.country',
            'preferred_choices' => array('FR'),
            'empty_value' => false,
            'required' => true,
            'constraints' => array(
                new CountryConstraint(array(
                    'message' => 'la_user.registration.country.invalid',
                    'groups' => array('LaRegistration', 'LaEdit'),
                )),
            ),
        );

        $builder->add('country', 'country', array_merge($default, $options));

    }

}