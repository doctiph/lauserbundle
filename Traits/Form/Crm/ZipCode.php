<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\NotBlank;

trait ZipCode
{
    /**
     * @param $builder
     * @param array $options
     */
    public static function add($builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.zipcode',
            'required' => true
        );

        $builder
            ->add('zipcode', 'text', array_merge($default, $options));
    }

}