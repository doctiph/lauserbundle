<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\NotBlank;

trait LastName
{
    /**
     * @param array $options
     */
    public static function add($builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.name',
            'constraints' => array(
                new NotBlank(array(
                    'message' => 'la_user.registration.lastName.blank',
                    'groups' => array('LaRegistration', 'LaEdit')
                )),
            ));

        $builder->add('lastName', 'text', array_merge($default, $options));
    }

}