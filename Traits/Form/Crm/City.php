<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Form\FormBuilderInterface;

trait City
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.city',
        );

        $builder->add('city', 'text', array_merge($default, $options));
    }

}