<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Validator\Constraints\NotBlank;

trait Birthdate
{
    /**
     * @param $builder
     * @param array $options
     */
    public static function add($builder, array $options = array())
    {
        // get the past 100 years
        $years = array();
        for ($i = date('Y'); $i >= (date('Y') - 101); $i--) {
            $years[] = $i;
        }

        $default = array(
            'label' => 'la_user.form.crm.birthdate',
            'widget' => 'choice',
            'years' => $years,
            'empty_value' => '',
        );

        $builder->add('birthdate', 'birthday', array_merge($default, $options));
    }

}