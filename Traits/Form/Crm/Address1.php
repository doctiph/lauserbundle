<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Form\FormBuilderInterface;

trait Address1
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.address1',
        );

        $builder->add('address1', 'text', array_merge($default, $options));
    }

}