<?php
namespace La\UserBundle\Traits\Form\Crm;

use Symfony\Component\Form\FormBuilderInterface;

trait HandPhoneNumber
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.handphonenumber',
        );

        $builder
            ->add('handphonenumber', 'text', array_merge($default, $options));
    }

}