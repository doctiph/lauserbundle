<?php
namespace La\UserBundle\Traits\Form\Registration;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\True;

trait Gcu
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'label' => 'la_user.form.crm.gcu',
            'mapped' => false,
            'required' => true,
            'constraints' => new True(array(
                'message' => 'la_user.registration.gcu.disagree',
                'groups' => array('LaRegistration')))
        );
        $builder->add('gcu', 'checkbox', array_merge($default, $options));
    }
}