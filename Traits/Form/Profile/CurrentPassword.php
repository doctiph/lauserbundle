<?php
namespace La\UserBundle\Traits\Form\Profile;

use Symfony\Component\Form\FormBuilderInterface;
use La\UserBundle\Validation\Constraints\LaCurrentPassword;

trait CurrentPassword
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {

        $default = array(
            'label' => 'la_user.profile.current_password',
            'mapped' => false,
            'constraints' => new LaCurrentPassword(array(
                'groups' => array('LaEdit'))),
        );

        $builder->add('current_password', 'password', array_merge($default, $options));
    }

}