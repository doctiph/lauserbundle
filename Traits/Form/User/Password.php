<?php
namespace La\UserBundle\Traits\Form\User;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

trait Password
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {
        $default = array(
            'type' => 'password',
            'first_options' => array(
                'label' => 'la_user.form.password',
                'constraints' => array(
                    new Length(array(
                        'min' => 4,
                        'max' => 255,
                        'minMessage' => 'la_user.registration.password.short',
                        'maxMessage' => 'la_user.registration.password.long',
                        'groups' => array('LaRegistration', 'LaEdit')
                    )),
                    new NotBlank(array(
                        'message' => 'la_user.registration.password.blank',
                        'groups' => array('LaRegistration', 'LaEdit')
                    )),
                ),
            ),
            'second_options' => array('label' => 'la_user.form.password_confirmation'),
            'invalid_message' => 'la_user.registration.password.mismatch',
        );

        $builder->add('plainPassword', 'repeated', array_merge($default, $options));
    }

}