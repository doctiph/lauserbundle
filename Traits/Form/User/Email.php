<?php
namespace La\UserBundle\Traits\Form\User;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\NotBlank;
use La\UserBundle\Validation\Constraints\ExtendedEmailConstraint;

trait Email
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {

        $default = array(
            'label' => 'la_user.form.email',
            'constraints' => array(
                new EmailConstraint(array(
                    'message' => 'la_user.registration.email.invalid',
                    'checkMX' => true,
                    'groups' => array('LaRegistration', 'LaEdit'),
                )),
                new NotBlank(array(
                    'message' => 'la_user.registration.email.blank',
                    'groups' => array('LaRegistration', 'LaEdit'),
                )),
                new ExtendedEmailConstraint(array(
                    'groups' => array('LaRegistration', 'LaEdit')
                ))
            ));


        $builder->add('email', 'email', array_merge($default,$options));
    }

}