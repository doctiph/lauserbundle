<?php
namespace La\UserBundle\Traits\Form\User;

use La\UserBundle\Validation\Constraints\LaUserCharacters;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

trait Username
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public static function add(FormBuilderInterface $builder, array $options = array())
    {

        $default = array(
            'label' => 'la_user.form.username',
            'constraints' => array(
                new LaUserCharacters(array("groups" => array('LaRegistration', 'LaEdit'))),
                new Length(array(
                    "min" => 4,
                    "max" => 20,
                    "minMessage" => "la_user.registration.username.short",
                    "maxMessage" => "la_user.registration.username.long",
                    'groups' => array('LaRegistration', 'LaEdit')
                )),
                new NotBlank(array(
                    "message" => "la_user.registration.username.blank",
                    'groups' => array('LaRegistration', 'LaEdit')
                )),
            ),
        );

        $builder->add('username', null, array_merge($default, $options));
    }

}