<?php
namespace La\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 *
 * php app/console user:delete <email>

 */
class DeleteUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:delete')
            ->setDescription('Delete a given user from our database')
            ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'Email of the user that will be deleted'
            );

    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $email = $input->getArgument('email');
        $userUtils = $this->getContainer()->get('la_user.lib.lauserutils');

        if ($output->getVerbosity() === $output::VERBOSITY_VERBOSE) {
            $output->writeln(PHP_EOL);
            $output->writeln('<question>  __       _      _      _         __  </question>');
            $output->writeln('<question> / /      | |    | |    | |        \ \ </question>');
            $output->writeln('<question> | |    __| | ___| | ___| |_ ___   | | </question>');
            $output->writeln('<question> | |   / _` |/ _ \ |/ _ \ __/ _ \  | | </question>');
            $output->writeln('<question> | |  | (_| |  __/ |  __/ ||  __/  | | </question>');
            $output->writeln('<question> | |   \____|\___|_|\___|\__\___|  | | </question>');
            $output->writeln('<question> | |        With great power,      | | </question>');
            $output->writeln('<question> \_\  comes great responsibility.  /_/ </question>');
            $output->writeln("<question>                                       </question>".PHP_EOL);
        }

        try {

            $dialog = $this->getHelperSet()->get('dialog');

            if ($dialog->askConfirmation(
                $output,
                sprintf('<comment>Deleting %s. Are you sure ? ("yes" or enter to delete, "no" to abort.):</comment> ', $email),
                true)
            ) {
                if ($userUtils->deleteUser($email)) {
                    $output->writeln("<info>Success.</info>");
                    exit(PHP_EOL);
                }else{
                    $output->writeln("<error>Error.</error>");
                }
            }
            $output->writeln("<comment>Aborted.</comment>");
            exit(PHP_EOL);
        } catch (\Exception $e) {
            exit($e->getMessage());
        }

    }

}
