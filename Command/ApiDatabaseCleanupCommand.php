<?php
namespace La\UserBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**

php app/console api:db:cleanup { [--age|-a] [-v] }

 */
class ApiDatabaseCleanupCommand extends ContainerAwareCommand
{
    const DEFAULT_OBSOLESCENCE_AGE = 2;

    protected function configure()
    {
        $this
            ->setName('api:db:cleanup')
            ->setDescription('Clean unconfirmed users after x days')
            ->addOption(
                'age',
                'a',
                InputOption::VALUE_OPTIONAL,
                'Optional : Delay after which a token is obsolescent and needs to be deleted',
                self::DEFAULT_OBSOLESCENCE_AGE
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $style = new OutputFormatterStyle('black', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('debug', $style);
        $dialog = $this->getHelperSet()->get('dialog');
        $progress = $this->getHelperSet()->get('progress');

        // Options
        $age = $input->getOption('age');

        try {

            $output->writeln("Cleaning obsolescent tokens.");
            $manager = $this->getContainer()->get('la_user.api.db.manager');
            $manager->cleanObsolescentTokens($output, $progress, $dialog, $age);

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }


}
