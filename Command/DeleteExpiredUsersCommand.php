<?php
namespace La\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Delete expired users since more than 24 hours ago
 *
 * php app/console user:delete:expired
 */
class DeleteExpiredUsersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:delete:expired')
            ->setDescription('Delete expired users from our database');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $userUtils = $this->getContainer()->get('la_user.lib.lauserutils');
        $emails = $userUtils->getExpiredUsers();

        $output->writeln(sprintf("%d users found.", count($emails)));
        try {
            $processed = 0;
            foreach ($emails as $email) {
                if ($userUtils->deleteExpiredUser($email)) {
                    $output->writeln(sprintf("<info>Deleted: %s</info>", $email));
                    $processed++;
                } else {
                    $output->writeln(sprintf("<error>Error while deleting: %s</error>", $email));
                }
            }
            $output->writeln(sprintf("%d users deleted. %d failed.", $processed, count($emails) - $processed));
        } catch (\Exception $e) {
            exit($e->getMessage());
        }

    }

}
