<?php
namespace La\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 *
 * php app/console user:activate {username or id}

 */
class RegistrationReminderCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:reminder')
            ->setDescription('Reminder that will resend unconfirmed registration mail >24h and delete unconfirmed users >48h')
            ->addOption(
                'force',
                null,
                InputOption::VALUE_NONE,
                'Force reminder'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        $force = $input->getOption('force');
        $registrationReminder = $this->getContainer()->get('la_user.registration_reminder.command');
        $progress = $progress = $this->getHelperSet()->get('progress');
        try {
            // >24h, <48h remind
            $registrationReminder->remind($output, $progress, $force);
            // >48h, delete accounts
            $registrationReminder->clean($output, $progress);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }

}
