<?php
namespace La\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**
 *
 * php app/console user:activate {username or id}

 */
class ManualUserActivationCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:activate')
            ->setDescription('Manual override of automatic user activation on registration success')
            ->addArgument(
                'username_or_id',
                InputArgument::IS_ARRAY,
                'Please specify the username(s) or id(s) you wish to activate.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $userActivator = $this->getContainer()->get('la_user.activate_user.command');

        $usernamesToActivate = $input->getArgument('username_or_id');

        $style = new OutputFormatterStyle('white', 'cyan', array('bold'));
        $output->getFormatter()->setStyle('icy', $style);
        $dialog = $this->getHelperSet()->get('dialog');

        try {

            if(empty($usernamesToActivate)){
                $answer = $dialog->ask(
                    $output,
                    sprintf('<icy>Please enter the id or username you wish to activate.'.PHP_EOL.'(Multiples id/username separated by spaces, empty entry to exit) : </icy>'),
                    null
                );
                if (is_null($answer)) {
                    $output->writeln("<icy>Goodbye.</icy>");
                    exit();
                }
                $usernamesToActivate = explode(' ', $answer);
            }

            $userActivator->activate($usernamesToActivate, $output);

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }

}
