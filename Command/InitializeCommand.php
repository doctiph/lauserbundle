<?php
namespace La\UserBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**

php app/console user:initialize

 */
class InitializeCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:initialize')
            ->setDescription('Initialize the user environment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $style = new OutputFormatterStyle('black', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('ask', $style);

        $dialog = $this->getHelperSet()->get('dialog');
        $db = $this->getContainer()->getParameter('database_host').'.'.$this->getContainer()->getParameter('database_name');

        $commandsToRun = array(
            "doctrine:schema:update   " => array(
                'desc' => 'The database '.$db.' will be updated.',
                'params' => array('--force' => true)
            ),
            "user:lib:setautoincrement" => array(
                'desc' => 'The auto increment will be initialized.',
                'params' => array('--value' => '10000000')
            ),
//            "importuser:ez:newsletters" => array(
//                'desc' => 'The newsletters will be imported from eZ.',
//                'params' => array()
//            ),
            "user:createtestuser      " => array(
                'desc' => 'The test user will be generated.',
                'params' => array('--superadmin')
            ),
        );

        // Deprecated ////////////////////////////////////////////////////////////////////////////////////////////////////////
        $style = new OutputFormatterStyle('red', null, array('bold', 'blink'));
        $output->getFormatter()->setStyle('deprecated', $style);
        $style = new OutputFormatterStyle('green', null, array('bold', 'blink'));
        $output->getFormatter()->setStyle('commands', $style);
        $output->writeln("<deprecated>Cette commande est obsolète. Veuillez lancer les commandes qu'elle regroupait manuellement : </deprecated> ");
        foreach($commandsToRun as $command => $informations)
        {
            $output->writeln("      <commands>".$command."</commands> : ".$informations['desc']);
        }
        exit(PHP_EOL);


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



        foreach ($commandsToRun as $command => $informations) {


            $output->writeln('');
            $output->writeln(sprintf('<ask>%s</ask>', $informations['desc']));
            if (!$dialog->askConfirmation(
                $output,
                sprintf('<ask>Please confirm (y/n)</ask> : '),
                true)
            ) {
                continue;
            }

            $app = $this->getApplication()->find($command);
            $input = new ArrayInput(
                array_merge(
                    array('command' => $command),
                    $informations['params']
                ));

            // Possible upgrade : check return code and act according to it
            $returnCode = $app->run($input, $output);
        }

        $output->writeln('');
        $output->writeln('The initialization has been successfuly completed !');


    }

}
