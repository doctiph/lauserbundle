<?php
namespace La\UserBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**

php app/console user:createtestuser --superadmin

 */
class CreateTestUserCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('user:createtestuser')
            ->setDescription('Create Test User for debug purpose')
            ->addOption(
                'superadmin',
                null,
                InputOption::VALUE_NONE,
                'Test user will have Super-Administrator privileges');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        try {

            if ($this->createTestUser($input, $output)) {
                $output->writeln("Test user has been successfuly generated. (Please check configuration files for its credentials)");
            } else {
                $output->writeln("An error occured during the generation. Please check the logs.");
            }

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return mixed
     */
    protected function createTestUser(InputInterface $input, OutputInterface $output)
    {

        $style = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);
        $dialog = $this->getHelperSet()->get('dialog');

        $userGenerator = $this->getContainer()->get('la_user.create_test_user.command');

        $superadmin = $input->getOption('superadmin');

        $output->writeln(sprintf("Creating%stest user...", $superadmin === true ? " superadmin " : " "));

        if ($userGenerator->checkTestUser()) {

            $output->writeln("<fire>Test User was already found in database.</fire>");
            if (!$dialog->askConfirmation($output, "<fire>Do you want to recreate it ? ('y' to continue) : </fire> ")) {
                $output->writeln("Aborting. Test user was not regenerated.");
                exit;
            }
            $userGenerator->deleteTestUser();
            $output->writeln("Old Test User has been successfuly deleted. Generating new Test User.");
        }

        return $userGenerator->createTestUser($output, $superadmin);

    }

}
