<?php
namespace La\UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**

php app/console user:lib:setautoincrement [--value=10000001]

 */
class SetAutoIncrementCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:lib:setautoincrement')
            ->setDescription('Set AutoIncrement to a given Value for la_user and la_crm')
            ->addOption(
                'value',
                null,
                InputOption::VALUE_OPTIONAL,
                'value ? integer'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $style = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('fire', $style);

        $setter = $this->getContainer()->get('la_user.setautoincrement.command');

        $value = $input->getOption('value');

        try {
            $value = $setter->setAutoIncrement($value, $output);

            $output->writeln(sprintf("AutoIncrement has been set to '%d'", $value));

        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
        }
    }

}