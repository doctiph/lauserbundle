<?php
namespace La\UserBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


/**

php app/console user:stats:draw

 */
class GraphicalStatsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:stats:draw')
            ->setDescription('Get graphical stats from past week registrations');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);

        $style = new OutputFormatterStyle('black', 'yellow', array('bold', 'blink'));
        $output->getFormatter()->setStyle('emphasis', $style);

        $style = new OutputFormatterStyle('green', null, array('bold'));
        $output->getFormatter()->setStyle('graph_bars', $style);

        $style = new OutputFormatterStyle("green", null, array('bold'));
        $output->getFormatter()->setStyle('weekend', $style);

        $dialog = $this->getHelperSet()->get('dialog');

        $statsGenerator = $this->getContainer()->get('la_user.graphical_stats_generator.command');

        $availableStats = array(
            "<weekend>Exit</weekend>" => null,
            "New Users since Last Week" => array("method" => "getDailyRegistrations", "limit" => 7),
            "New Users since Last Month" => array("method" => "getDailyRegistrations", "limit" => 31),
            "New Users since Last Year" => array("method" => "getMonthlyRegistrations", "limit" => 12),
            "New Crm since Last Week" => array("method" => "getDailySubscriptions", "limit" => 7),
            "New Crm since Last Month" => array("method" => "getDailySubscriptions", "limit" => 31),
            "New Crm since Last Year" => array("method" => "getMonthlySubscriptions", "limit" => 12),
        );


        do {

            $chosenStat = $dialog->select(
                $output,
                PHP_EOL."<emphasis> Which stat do you want to display ? </emphasis>".PHP_EOL,
                array_keys($availableStats)
            );
            $output->writeln("");
            $method = $availableStats[array_keys($availableStats)[$chosenStat]];
            if (!is_null($method)) {
                $statsGenerator->$method['method']($method['limit'], $output);
            } else {
                $output->writeln("<emphasis> Exiting. </emphasis>" . PHP_EOL);
            }

        } while (!is_null($method));



    }

}
