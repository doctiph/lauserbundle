<?php

namespace La\UserBundle\EventListener;


use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use La\UserBundle\LaUserEvents;
use La\UserBundle\Event\CrmEvent;
use La\UserBundle\Entity\UserInterface;
use La\UserBundle\Lib\LaUserUtil;
use La\UserBundle\Lib\RedirectUtil;

class LoginListener implements EventSubscriberInterface
{
    protected $serviceLogin;

    public function __construct(LaUserUtil $serviceLogin, RedirectUtil $redirectUtil)
    {
        $this->serviceLaUserUtil = $serviceLogin;
        $this->serviceRedirectUtil = $redirectUtil;
    }

    public static function getSubscribedEvents()
    {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
            FOSUserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
        );
    }

    public function onImplicitLogin(UserEvent $event)
    {
        $event->getDispatcher()->dispatch(LaUserEvents::LAST_ACTIVITY_UPDATE, new CrmEvent($event->getUser()->getCrm()));
        $this->serviceLaUserUtil->createCookiesFromEvent($event);
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        if ($user instanceof UserInterface) {
            $event->getDispatcher()->dispatch(FOSUserEvents::SECURITY_IMPLICIT_LOGIN, new UserEvent($user, $event->getRequest()));
        }
        $event->getDispatcher()->addListener(KernelEvents::RESPONSE, array($this, 'redirectToUrl'));
    }

    public function redirectToUrl(FilterResponseEvent $event)
    {
        if ($callback = $this->serviceRedirectUtil->getCallback()) {
            $response = $this->serviceRedirectUtil->responseCallback($callback);
            $event->setResponse($response);
        } elseif ($redirectUrl = $this->serviceRedirectUtil->getRedirectUrl()) {
            $response = $this->serviceRedirectUtil->responseRedirectTo($redirectUrl);
            $event->setResponse($response);
        }
    }

}
