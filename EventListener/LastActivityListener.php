<?php

namespace La\UserBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\UserBundle\LaUserEvents;
use La\UserBundle\Services\CrmManager;
use La\UserBundle\Event\CrmEvent;

class LastActivityListener implements EventSubscriberInterface
{
    /** @var CrmManager */
    protected $manager;

    public function construct(CrmManager $manager)
    {
        $this->manager = $manager;
    }

    public static function getSubscribedEvents()
    {
        return array(
            LaUserEvents::LAST_ACTIVITY_UPDATE => 'onLastActivityUpdate',
        );
    }

    public function onLastActivityUpdate(CrmEvent $event)
    {
        $crm = $event->getCrm();
        $crm->setLastActivity(new \DateTime());
//        $this->manager->update($crm);
    }

}
