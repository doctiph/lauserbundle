<?php

namespace La\UserBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use La\AdminBundle\Event\AdminNavEvent;
use La\UserBundle\Controller\AdminController;

class AdminListener implements EventSubscriberInterface
{
//    const ADMIN_NAV_EVENT_PRIORITY = 1;

    protected $router;

    public function __construct($router)
    {
        $this->router = $router;
    }

    public static function getSubscribedEvents()
    {
        return array(
            'admin.nav.links' => array('onAdminNavLinksRequest')
        );
    }

    public function onAdminNavLinksRequest(AdminNavEvent $event)
    {
        $event->addLinks(
            array(
                'name' => 'la_user_admin.nav.stats.default',
                'icon' => 'bar-chart-o',
                'links' => array(
                    array(
                        'name' => 'la_user_admin.nav.stats.users',
                        'url' => $this->router->generate('la_user_admin_stats'),
                        // role minimal
                        'role' => 'ROLE_LA_ADMIN',
                    )
                )
            )
        );

        $event->addLinks(
            array(
                'name' => 'la_user_admin.nav.users.default',
                'icon' => 'user',
                'links' => array(
                    array(
                        'name' => 'la_user_admin.nav.users.crm',
                        'url' => $this->router->generate('la_user_admin_crm'),
                        'role' => 'ROLE_LA_ADMIN_LA_USER',
                    ),
                    array(
                        'name' => 'la_user_admin.nav.users.user',
                        'url' => $this->router->generate('la_user_admin_user'),
                        'role' => 'ROLE_LA_ADMIN_LA_USER',
                    ),
                    array(
                        'name' => 'la_user_admin.nav.users.unconfirmed',
                        'url' => $this->router->generate('la_user_admin_user_unconfirmed'),
                        'role' => 'ROLE_LA_ADMIN_LA_USER_ACTIVATE',
                    ),
                    array(
                        'name' => 'la_user_admin.nav.users.export',
                        'url' => $this->router->generate('la_user_admin_user_export'),
                        'role' => 'ROLE_LA_ADMIN_LA_USER',
                    )
                )
            )
        );
    }


}