<?php

namespace La\UserBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Doctrine\ORM\EntityManager;
use La\UserBundle\LaUserEvents;

class ApiListener implements EventSubscriberInterface
{

    public function __construct(EntityManager $em, $laConf, $apiManager)
    {
        $this->em = $em;
        $this->laConf = $laConf;
        $this->apiManager = $apiManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
//            LaUserEvents::API_BEFORE_REGISTRATION => array('onApiRegistration', 50) @todo Réfléchir à déplacer la logique de register ici
            LaUserEvents::API_FORMAT_USER_REQUEST => array('onUserFormat', 100)
        );
    }

    public function onUserFormat(GenericEvent $event)
    {
        $event['data'] = $this->apiManager->formatEventUser($event);
    }

}