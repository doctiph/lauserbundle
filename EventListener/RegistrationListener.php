<?php

namespace La\UserBundle\EventListener;


use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use La\UserBundle\Lib\LaUserUtil;
use La\UserBundle\Services\LaMailer;

class RegistrationListener implements EventSubscriberInterface
{

    public function __construct(EntityManager $em, LaUserUtil $serviceLaUserUtil, UserManagerInterface $userManager, LaMailer $mailer)
    {
        $this->em = $em;
        $this->serviceLaUserUtil = $serviceLaUserUtil;
        $this->userManager = $userManager;
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
            FOSUserEvents::REGISTRATION_CONFIRM => 'onRegistrationConfirm',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationConfirmed',
        );
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();
        $this->serviceLaUserUtil->setCrmTmpData($user, $this->serviceLaUserUtil->siteOriginFactory());
        $this->userManager->updateUser($user);
    }

    // Before confirm for test
    public function onRegistrationConfirm(GetResponseUserEvent $event)
    {
        $user = $event->getUser();
        $this->serviceLaUserUtil->activateUserCrm($user);
    }

    public function onRegistrationConfirmed(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();
        $user->setCrmTmp(null);
        $this->em->flush();

        // send welcome mail
        $this->mailer->sendWelcomeEmailMessage($user);
    }
}