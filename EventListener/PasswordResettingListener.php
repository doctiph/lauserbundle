<?php

namespace La\UserBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserManagerInterface;
use La\UserBundle\Lib\LaUserUtil;
use La\UserBundle\Lib\RedirectUtil;
use La\UserBundle\Entity\UserInterface;

class PasswordResettingListener implements EventSubscriberInterface
{
    protected $router;
    protected $userManager;
    protected $laUserUtil;
    protected $redirectUtil;
    protected $dispatcher;

    public function __construct(UrlGeneratorInterface $router, $dispatcher, UserManagerInterface $userManager, LaUserUtil $laUserUtil, RedirectUtil $redirectUtil)
    {
        $this->router = $router;
        $this->userManager = $userManager;
        $this->serviceLaUserUtil = $laUserUtil;
        $this->serviceRedirectUtil = $redirectUtil;
        $this->dispatcher = $dispatcher;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::RESETTING_RESET_INITIALIZE => 'onPasswordResettingInitialize',
            FOSUserEvents::RESETTING_RESET_COMPLETED => 'onPasswordResettingSuccess',
        ];
    }

    public function onPasswordResettingInitialize(GetResponseUserEvent $event)
    {
        /** @var UserInterface $user */
        $user = $event->getUser();
        $user->setSalt($this->userManager->createUser()->getSalt()); // On password reset, regenerate salt, using FOS user construct
    }

    public function onPasswordResettingSuccess(FilterUserResponseEvent $event)
    {
        $this->dispatcher->addListener(KernelEvents::RESPONSE, array($this, 'redirectToUrl'));
    }

    public function redirectToUrl(FilterResponseEvent $event)
    {
        if ($callback = $this->serviceRedirectUtil->getCallback()) {
            $response = $this->serviceRedirectUtil->responseCallback($callback);
            $event->setResponse($response);
        } elseif ($redirectUrl = $this->serviceRedirectUtil->getRedirectUrl()) {
            $response = $this->serviceRedirectUtil->responseRedirectTo($redirectUrl);
            $event->setResponse($response);
        }
    }
}

?>