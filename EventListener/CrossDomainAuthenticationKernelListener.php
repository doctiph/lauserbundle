<?php

/**
 * Copyright Pierre "Le grand" Tranchard
 */
namespace La\UserBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\Security\Core\SecurityContextInterface;
use FOS\UserBundle\Security\LoginManagerInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use La\UserBundle\Lib\LaUserUtil;

/**
 * Class CrossDomainAuthenticationKernelListener
 */
class CrossDomainAuthenticationKernelListener
{

    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $securityContext;

    /**
     * @var \La\UserBundle\Lib\LaUserUtil
     */
    private $laUserUtil;

    /**
     * @var \FOS\UserBundle\Security\LoginManagerInterface
     */
    private $loginManager;

    /**
     * @var
     */
    private $firewallName;


    /**
     * @param SecurityContextInterface $securityContext
     * @param LaUserUtil $laUserUtil
     * @param LoginManagerInterface $loginManager
     * @param $firewallName
     */
    public function __construct(
        SecurityContextInterface $securityContext,
        LaUserUtil $laUserUtil,
        LoginManagerInterface $loginManager, $firewallName
    )
    {
        $this->securityContext = $securityContext;
        $this->laUserUtil = $laUserUtil;
        $this->loginManager = $loginManager;
        $this->firewallName = $firewallName;
    }

    /**
     * On Kernel Request
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     * @return void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }
        $request = $event->getRequest();
        if (!$this->securityContext->isGranted('IS_AUTHENTICATED_FULLY')) {

            $cookieKey = LaUserUtil::COOKIE_LA_TOKEN;
            if ($request->cookies->has($cookieKey)) {

                $token = $request->cookies->get($cookieKey);
                $encodedToken = $this->laUserUtil->encodeToken($token);
                try {
                    $user = $this->laUserUtil->getUserFromToken($encodedToken);
                } catch (\Exception $ex) {
                    $user = null;
                }

                if ($user) {
                    if (!$user->isEnabled()) {
                        return;
                    }
                    try {
                        $this->loginManager->loginUser($this->firewallName, $user, $event->getResponse());
                        $event->getDispatcher()->dispatch(FOSUserEvents::SECURITY_IMPLICIT_LOGIN, new UserEvent($user, $event->getRequest()));
                    } catch (AccountStatusException $ex) {
                        // We simply do not authenticate users which do not pass the user
                        // checker (not enabled, expired, etc.).
                    }

                }
            }

        }

    }

}
