<?php

namespace La\UserBundle\EventListener;

use La\ApiBundle\EventListener\ApiDocListener as BaseApiDocListener;


class ApiDocListener extends BaseApiDocListener
{
    const API_DOC_ROOT_EVENT_PRIORITY = 50;


    public function onApiDocRootRequest($event)
    {
        $event->addDocRootDir(__DIR__ . '/../Resources/doc/api/resources');
    }
}