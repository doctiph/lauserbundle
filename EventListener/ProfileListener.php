<?php

namespace La\UserBundle\EventListener;


use Doctrine\ORM\EntityManager;
use La\UserBundle\LaUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use La\UserBundle\Lib\LaUserUtil;
use La\UserBundle\Lib\RedirectUtil;

class ProfileListener implements EventSubscriberInterface
{

    public function __construct(EntityManager $em, LaUserUtil $serviceLaUserUtil, RedirectUtil $redirectUtil, Session $session)
    {
        $this->em = $em;
        $this->serviceLaUserUtil = $serviceLaUserUtil;
        $this->serviceRedirectUtil = $redirectUtil;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::PROFILE_EDIT_SUCCESS => 'onProfileEditSuccess',
            FOSUserEvents::PROFILE_EDIT_COMPLETED => 'onProfileEditCompleted',
        );
    }

    public function onProfileEditSuccess(FormEvent $event)
    {
        $user = $event->getForm()->getData();
        $event->getDispatcher()->dispatch(FOSUserEvents::SECURITY_IMPLICIT_LOGIN, new UserEvent($user, $event->getRequest()));
    }

    public function onProfileEditCompleted(FilterUserResponseEvent $event)
    {
        // On ajoute un flashbag message de succes
        $flashbag = $this->session->getFlashBag();
        $flashbag->clear();
        $flashbag->add('la_user_profile_edit', 'la_user.profile.edit_success');

        $event->getDispatcher()->addListener(KernelEvents::RESPONSE, array($this, 'redirectToUrl'));
    }

    public function redirectToUrl(FilterResponseEvent $event)
    {
        if ($redirectUrl = $this->serviceRedirectUtil->getRedirectUrl()) {
            $response = $this->serviceRedirectUtil->responseRedirectTo($redirectUrl);
            $event->setResponse($response);
        }
    }

}
