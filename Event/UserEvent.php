<?php

namespace La\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use La\UserBundle\Entity\User;

class UserEvent extends Event
{
    protected $user;
    protected $displayUsername;

    public function __construct(User $user)
    {
        $this->setUser($user);
        $this->setDisplayUsername('');
    }

    /**
     * @param User $user
     */
    protected function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $displayUsername
     */
    public function setDisplayUsername($displayUsername)
    {
        $this->displayUsername = $displayUsername;
    }

    /**
     * @return mixed
     */
    public function getDisplayUsername()
    {
        return $this->displayUsername;
    }


}