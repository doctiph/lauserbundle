<?php

namespace La\UserBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use La\UserBundle\Entity\CrmInterface;

class CrmEvent extends Event
{
    protected $crm;
    protected $crmToMerge = null;

    public function __construct(CrmInterface $crm)
    {
        $this->setCrm($crm);
    }

    /**
     * @param CrmInterface $crm
     */
    protected function setCrm(CrmInterface $crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return CrmInterface
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param null $crmToMerge
     */
    public function setCrmToMerge($crmToMerge)
    {
        $this->crmToMerge = $crmToMerge;
    }

    /**
     * @return null
     */
    public function getCrmToMerge()
    {
        return $this->crmToMerge;
    }



}