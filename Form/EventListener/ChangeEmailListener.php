<?php
namespace La\UserBundle\Form\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Translation\Translator;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManager;
use La\UserBundle\Services\CrmManager;

/**
 * Class ChangeEmailListener
 * @package La\UserBundle\Form\EventListener
 */
class ChangeEmailListener implements EventSubscriberInterface
{
    /** @var \FOS\UserBundle\Model\UserManager */
    protected $userManager;
    /** @var \La\UserBundle\Services\CrmManager */
    protected $crmManager;
    /** @var  Translator */
    protected $translator;

    /**
     * @param UserManager $userManager
     * @param CrmManager $crmManager
     * @param Translator $translator
     */
    public function __construct(UserManager $userManager, CrmManager $crmManager, Translator $translator)
    {
        $this->userManager = $userManager;
        $this->crmManager = $crmManager;
        $this->translator = $translator;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SUBMIT => 'onPostSubmit',
        );
    }

    /**
     * @param $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $form = $event->getForm();

        /** @var UserInterface $user */
        $user = $event->getData();
        $crm = $user->getCrm();

        $existingUser = $this->userManager->findUserByEmail($user->getEmail());
        if ($existingUser && $existingUser->getId() === $user->getId()) {
            $form->get('email')->addError(new FormError($this->translator->trans('la_user.profile.change_email.identical', array(), 'validators')));
        } else if ($existingUser) {
            $form->get('email')->addError(new FormError($this->translator->trans('la_user.profile.change_email.already_used', array(), 'validators')));
        } else {
            $existingCrm = $this->crmManager->getByEmail($user->getEmail());
            if ($existingCrm && $existingCrm->getId() === $crm->getId()) {
                $form->get('email')->addError(new FormError($this->translator->trans('la_user.profile.change_email.identical', array(), 'validators')));
            } else if ($existingUser) {
                $form->get('email')->addError(new FormError($this->translator->trans('la_user.profile.change_email.already_used', array(), 'validators')));
            }
        }

    }


}