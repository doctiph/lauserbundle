<?php
namespace La\UserBundle\Form\Type;

use La\UserBundle\Form\Type\CrmType as BaseCrmType;

/**
 * Used in profile/edit
 *
 * Class ProfileCrmType
 * @package La\ElleUserBundle\Form\Type
 */
class ProfileCrmType extends BaseCrmType
{

    public function getName()
    {
        return 'la_profile_crm';
    }

}