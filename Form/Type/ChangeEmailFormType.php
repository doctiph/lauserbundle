<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace La\UserBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use FOS\UserBundle\Doctrine\UserManager;
use La\UserBundle\Services\CrmManager;
use La\UserBundle\Form\EventListener\ChangeEmailListener;
use La\UserBundle\Traits\Form\User\Email;
use La\UserBundle\Traits\Form\Submit;

class ChangeEmailFormType extends AbstractType
{
    protected $class;
    /** @var \FOS\UserBundle\Model\UserManager  */
    protected $userManager;
    /** @var \La\UserBundle\Services\CrmManager  */
    protected $crmManager;

    /**
     * @param UserManager $userManager
     * @param CrmManager $crmManager
     * @param $translator
     * @param $class
     */
    public function __construct(UserManager $userManager, CrmManager $crmManager, $translator, $class)
    {
        $this->class = $class;
        $this->userManager = $userManager;
        $this->crmManager = $crmManager;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Email::add($builder, array(
            'label' => 'la_user.form.change_email.label'
        ));
        Submit::add($builder, array(
            'label' => 'la_user.form.submit.change_email'
        ));

        $builder->addEventSubscriber(new ChangeEmailListener($this->userManager, $this->crmManager, $this->translator));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'translation_domain' => 'forms',
            'intention' => 'change_email',
            'csrf_protection'   => false
        ));
    }

    public function getName()
    {
        return 'la_user_change_email';
    }
}
