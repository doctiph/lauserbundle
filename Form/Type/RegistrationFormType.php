<?php

namespace La\UserBundle\Form\Type;


use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use La\UserBundle\Traits\Form\User;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

abstract class RegistrationFormType extends BaseRegistrationFormType
{
    public function __construct($class, $crmType)
    {
        parent::__construct($class);
        $this->crmType = $crmType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        User\Username::add($builder,array('required' => true));
        User\Email::add($builder);
        User\Password::add($builder);

        $builder->add('crmTmp', $this->crmType, array(
            'label' => null,
//            'required' => false,
        ));
    }

    public function getName()
    {
        return 'la_user_registration';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array(
            'translation_domain' => 'forms',
            'cascade_validation' => true, // cascade validation for Crm form
        ));
    }

}
