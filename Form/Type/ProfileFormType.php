<?php

namespace La\UserBundle\Form\Type;

use FOS\UserBundle\Form\Type\ProfileFormType as baseProfileFormType;
use La\UserBundle\Traits\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProfileFormType extends baseProfileFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Form\Profile\CurrentPassword::add($builder);
        Form\Submit::add($builder, array('label' => 'la_user.form.submit.profile'));
    }

    public function getName()
    {
        return 'la_user_profile';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'validation_groups' => array('LaEdit'),
            'translation_domain' => 'forms',
            'cascade_validation' => true
        ));
    }

}
