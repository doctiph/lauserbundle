<?php
namespace La\UserBundle\Form\Type;


use La\UserBundle\Form\Type\CrmType as BaseCrmType;

abstract class RegistrationCrmType extends BaseCrmType
{

    public function getName()
    {
        return 'la_registration_crm';
    }

}