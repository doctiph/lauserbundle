<?php

namespace La\UserBundle\Form\Type;

use FOS\UserBundle\Form\Type\ResettingFormType as ParentResettingFormType;
use La\UserBundle\Traits\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints;

class ResettingFormType extends ParentResettingFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Form\User\Password::add($builder,array(
            'first_options' => array(
                'label' => 'la_user.form.reset_password.new_password',
                'constraints' => array(
                    new Constraints\Length(array(
                        'min' => 4,
                        'max' => 255,
                        'minMessage' => 'la_user.registration.password.short',
                        'maxMessage' => 'la_user.registration.password.long',
                        'groups' => 'LaEdit')),
                    new Constraints\NotBlank(array(
                        'message' => 'la_user.registration.password.blank',
                        'groups' => array('LaEdit')
                    )),
                ),
            ),
            'second_options' => array('label' => 'la_user.form.reset_password.new_repeat'),
        ));
        Form\Submit::add($builder,array(
            'label' => 'la_user.form.submit.reset_password',
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'validation_groups' => array('LaEdit'),
            'translation_domain' => 'forms',
        ));
    }

    public function getName()
    {
        return 'la_user_resetting';
    }
}
