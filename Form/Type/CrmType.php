<?php
namespace La\UserBundle\Form\Type;

use La\UserBundle\Traits\Form\Crm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

abstract class CrmType extends AbstractType
{

    public function __construct($em, $translator)
    {
        $this->em = $em;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Crm\FirstName::add($builder, array('required' => true));
        Crm\LastName::add($builder, array('required' => true));
    }

    public function getName()
    {
        return 'la_crm';
    }

}