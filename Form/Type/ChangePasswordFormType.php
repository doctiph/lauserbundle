<?php

namespace La\UserBundle\Form\Type;

use FOS\UserBundle\Form\Type\ChangePasswordFormType as ParentChangePasswordFormType;
use La\UserBundle\Traits\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ChangePasswordFormType extends ParentChangePasswordFormType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Form\Profile\CurrentPassword::add($builder, array('label' => 'la_user.form.change_password.current'));
        Form\User\Password::add($builder);
        Form\Submit::add($builder, array('label' => 'la_user.form.submit.change_password'));

    }

    public function getName()
    {
        return 'la_user_change_password';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);

        $resolver->setDefaults(array(
            'validation_groups' => array('LaEdit'),
            'translation_domain' => 'forms',
        ));
    }
}
