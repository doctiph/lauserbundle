<?php
namespace La\UserBundle\Form\Type;

use La\UserBundle\Traits\Form\Crm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ChildrenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        Crm\Gender::add($builder);
        Crm\FirstName::add($builder);
        Crm\Birthdate::add($builder);
    }

    public function getName()
    {
        return 'la_crm_children';
    }

}